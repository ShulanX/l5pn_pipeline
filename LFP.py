# -*- coding: utf-8 -*-
"""
Created on Sat Sep 14 16:18:23 2019

@author: xiao208

The model is based on LFPy, to visite the file location, use "inspect.getsourcefile(LFPy.Cell)"

segmenting method: lambda = 100, can be changed by specify 'f' in the cell parameter

The custome method is adapted from
        Fromherz, Peter. “Extracellular recording with transistors and the distribution of ionic conductances in a cell membrane.” 
        European Biophysics Journal 28 (1999): 254-258.
"""
#%%
import LFPy
import allensdk.model.biophysical.utils as utils
import allensdk.model.biophysical.runner as runner
from neuron import h
import numpy as np
import os
import sys
import neuron
import statistics
import math
import random
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.animation as animation
import matplotlib
from scipy.integrate import odeint
from scipy import signal
import scipy
import warnings
import pylab as pl
import json
# from neurom import viewer

def create_LFPclass(wd, verbool = False):
    return LFPclass(wd, verbool)

def get_longest_branch(cell):
    seg_d = []

    soma_seg_idx = cell.get_idx("soma[0]")

    for sec in h.allsec():
        for seg, seg_idx in zip(sec, cell.get_idx(sec.name())):
            try:
                d = cell.get_intersegment_distance(soma_seg_idx, seg_idx)
            except:
                pass
            seg_d.append(d)

    max_d = np.max(seg_d)
    return max_d


def distribute_channels_linear(cell, sec_name, mec, value_name, value, verbool = False):
    """
    set gradient in of a value in the mechanism in sections, based on the distance between the center of the section and soma
    The value is a np vector of the param_value, the sections will be assigned based distance to soma and linear interpolation

    Parameters
    ----------
    sec_name : str or list
        'soma', 'dend', 'apic' or 'axon'
    mec : str
        name of the mechaniem
    value_name : str
        name of the value to be set
    value : np array, 
    g = value[0] + d * value[1]
    Returns
    -------
    None.

    """
    # load and sort the sections
    sections = []
    sec_d = []

    for sec in h.allsec():
        if sec.name()[0:4] in sec_name:
            sections.append(sec)

    # sections.remove(sections[0])

    # load the soma section and its position
    soma_seg_idx = cell.get_idx("soma[0]")

    # calculate the distance and set gradient
    for sec in sections:
        if ("shead" not in sec.name()) and ("sneck" not in sec.name()) and ("pre" not in sec.name()):
            for seg, seg_idx in zip(sec, cell.get_idx(sec.name())):
                d = cell.get_intersegment_distance(soma_seg_idx, seg_idx)
                if h.ismembrane(str(mec), sec=sec) != 1:
                    sec.insert(str(mec))
                if verbool:
                    print('Setting %s to %.6g in %s'
                          % (value_name, value[0] + d * value[1], sec.name()))
                seg_mec = getattr(seg, str(mec))
                setattr(sec_mec, value_name, value[0] + d * value[1])


def distribute_channels_step(cell, sec_name, mec, value_name, value, verbool = False):
    """
    set the value of mechanism parameter with step function, usually to set conducntance of Ca channels on apical dendrite
    value: np.array(start_dis, stop_dis, value_in, value_out, scale factor)

    Parameters
    ----------
    sec_name : str or list
        'soma', 'dend', 'apic' or 'axon'
    mec : str
        name of the mechaniem
    value_name : str
        name of the value to be set
    value : np array, 
        a vector of the values. np.array(start_dis, stop_dis, value_in, value_out)

    Returns
    -------
    None.

    """
    # load and sort the sections
    # load and sort the sections
    sections = []

    for sec in h.allsec():
        if sec.name()[0:4] in sec_name:
            sections.append(sec)

    # sections.remove(sections[0])
    # load the soma section and its position
    soma_seg_idx = cell.get_idx("soma[0]")

    # calculate the distance and set parameter value
    for sec in h.allsec():
        if ("shead" not in sec.name()) and ("sneck" not in sec.name()) and ("pre" not in sec.name()):
            for seg, seg_idx in zip(sec, cell.get_idx(sec.name())):
                d = cell.get_intersegment_distance(soma_seg_idx, seg_idx)
                if d >= value[0] and d <= value[1]:
                    if h.ismembrane(str(mec), sec=sec) != 1:
                        sec.insert(str(mec))
                    if verbool:
                        print('Setting %s to %.6g in %s'
                              % (value_name, value[2]*value[4], sec.name()))
                    seg_mec = getattr(seg, str(mec))
                    setattr(seg_mec, value_name, value[2]*value[4])
                else:
                    if h.ismembrane(str(mec), sec=sec) != 1:
                        sec.insert(str(mec))
                    if verbool:
                        print('Setting %s to %.6g in %s'
                              % (value_name, value[3]*value[4], sec.name()))
                    seg_mec = getattr(seg, str(mec))
                    setattr(seg_mec, value_name, value[3] * value[4])


def distribute_channels_exponential(cell, sec_name, mec, value_name, value, verbool = False):
    """
    set the value of mechanism parameter with exponential function, usually to set conducntance of Ih on apical dendrite
    g_sec = value[4]* (value[0] + value[3]*exp(value[1]*(d - value[2])))

    Parameters
    ----------
    sec_name : str or list
        'soma', 'dend', 'apic' or 'axon'
    mec : str
        name of the mechaniem
    value_name : str
        name of the value to be set
    value : np array, 
        a vector of the values. g_sec = value[4]* (value[0] + value[3]*exp(value[1]*(d - value[2])))

    Returns
    -------
    None.

    """
    # load and sort the sections
    sections = []

    for sec in h.allsec():
        if sec.name()[0:4] in sec_name:
            sections.append(sec)
    # sections.remove(sections[0])

    soma_seg_idx = cell.get_idx("soma[0]")

    max_d = get_longest_branch(cell)

    # calculate the distance and set the prameter value
    for sec in sections:
        if ("shead" not in sec.name()) and ("sneck" not in sec.name()) and ("pre" not in sec.name()):
            for seg, seg_idx in zip(sec, cell.get_idx(sec.name())):
                d = cell.get_intersegment_distance(soma_seg_idx, seg_idx)
                value_insert = value[4]*(value[0] + value[3] * math.exp(value[1] * (d/max_d - value[2])))
                if value_insert <= 0:
                    value_insert = 0
                if h.ismembrane(str(mec), sec=sec) != 1:
                    sec.insert(str(mec))
                if verbool:
                    print('Setting %s to %.6g in %s'
                          % (value_name, value_insert, sec.name()))
                seg_mec = getattr(seg, str(mec))
                setattr(seg_mec, value_name, value_insert)

def distribute_channels_sigmoid(cell, sec_name, mec, value_name, value, verbool = False):
    """
    set the value of mechanism parameter with exponential function, usually to set conducntance of Ih on apical dendrite
    g_sec = value[0] + value[1]*(1 + exp((d - value[2])/value[3]))

    Parameters
    ----------
    sec_name : str or list
        'soma', 'dend', 'apic' or 'axon'
    mec : str
        name of the mechaniem
    value_name : str
        name of the value to be set
    value : np array,
        a vector of the values. g_sec = value[0] + value[1]*(1 + exp((d - value[2])/value[3]))

    Returns
    -------
    None.

    """
    # load and sort the sections
    sections = []

    for sec in h.allsec():
        if sec.name()[0:4] in sec_name:
            sections.append(sec)

    # sections.remove(sections[0])

    soma_seg_idx = cell.get_idx("soma[0]")

    # calculate the distance and set the prameter value
    for sec in sections:
        for seg, seg_idx in zip(sec, cell.get_idx(sec.name())):
            d = cell.get_intersegment_distance(soma_seg_idx, seg_idx)
            value_insert = value[0] + value[1]*(1 + math.exp((d - value[2])/value[3]))
            if value_insert <= 0:
                value_insert = 0
            if h.ismembrane(str(mec), sec=sec) != 1:
                sec.insert(str(mec))
            if verbool:
                print('Setting %s to %.6g in %s'
                      % (value_name, value_insert, sec.name()))
            seg_mec = getattr(seg, str(mec))
            setattr(seg_mec, value_name, value_insert)

def locate_center(section):
    """
    locate the center of the section, return the x, y, z position

    Parameters
    ----------
    section : nrn.Section
        the target section

    Returns
    -------
    np.array([x, y, z])

    """
    n3d = section.n3d()
    x3d = []
    z3d = []
    y3d = []
    for i in range(n3d):
        x3d.append(section.x3d(i))
        y3d.append(section.y3d(i))
        z3d.append(section.z3d(i))

    mid_x = statistics.median(x3d)
    mid_y = statistics.median(y3d)
    mid_z = statistics.median(z3d)
    return np.array([mid_x, mid_y, mid_z])


def distance_3d(coor1, coor2):
    """
    calculated the 3d distance from coor1 to coor2

    Parameters
    ----------
    coor1 : np.array
        [x, y, z] of point 1
    coor2 : np.array
        [x, y, z] o flist 2

    Returns
    -------
    d: distance

    """
    d = np.linalg.norm(coor1 - coor2)
    return d


def search_sec_index(sections, name):
    """
    search the indec of a section given its name

    Parameters
    ----------
    sections: list
        List of all sections.

    name : str
        Name of a section.

    Returns
    -------
    index: the index of the given name

    """
    return [i for i in range(len(sections)) if sections[i].name() == name]


def local_junctional_V_calc(cell, section, position=[0, 0, 0], beta_bar=0, g_j=0.3, c_EL=1, if_plot=False):
    """
        calcultate the junctional voltage based on cable theory, adapted from: 
        
        Fromherz, Peter. “Extracellular recording with transistors and the distribution of ionic conductances in a cell membrane.” 
        European Biophysics Journal 28 (1999): 254-258.
        
        only Vm from the closest section (need to be specified in the function) is considered

    Parameters
    ----------
    cell: LFPy.cell
    section: str
        name of the section as the source of transmembrane current
    position: list, [x,y,z]
        position of the electrode
    beta_bar: defult is 0
        the fraction of junctional membrane area compared to entire membrane area, exponentially decay with the distance
    g_j: defult is 0.3 (S/m)
        extracellular conductivity (seal conductance), the unit is uS/um (S/m)
        in cortical gray matter the value is around 0.3 to 0.4 S/m
    c_EL: default is 1 (10^-12 f)
        electrode capacitancem the unit is pf 
    if_plot: default is False
        plot the Vm, im trace and the V_j trace

    Returns
    -------
    V_j: np.array 
        junction voltage calculated based on the given g_j and c_EL, unit is mV

    """
    C_EL = c_EL * 1e-12
    dt = cell.dt * 1e-3
    idx = cell.get_idx(section)
    EL_coor = np.asarray(position)
    b = np.asarray([1])
    if idx.size == 0:
        raise ValueError('the section %s does not exist' % section)
        return -1
    else:
        # d = []
        # area_d = []
        # for i in idx:
        #     seg_coor = np.asarray([(cell.xstart[i] - cell.xend[i]) / 2, (cell.ystart[i] - cell.yend[i]) / 2,
        #                            (cell.zstart[i] - cell.zend[i]) / 2])
        #     d_ = 1 / distance_3d(seg_coor, EL_coor) * 1e6  # 1/d ,unit in m^-1
        #     beta = beta_bar * math.exp(-(1 / d_ * 1e6) / 100)
        #     area_d_ = beta * cell.area[i] * 1e-8 * d_  # bate*area/d, unit in cm^2 m^-1
        #     d.append(d_)
        #     area_d.append(area_d_)
        # d = np.asarray(d)
        # area_d = np.asarray(area_d)
        # i_m = np.dot(np.transpose(cell.imem[idx, :]), d) * 1e-9
        # v_m = np.mean(cell.vmem[idx, :], axis=0) * 1e-3
        # i_cap = np.dot(np.transpose(cell.icap[idx, :]), d) * 1e-9
        # i_pas = np.dot(np.transpose(cell.ipas[idx, :]), d) * 1e-9
        # i_ion = np.dot(np.transpose(
        #     cell.rec_variables['ik'][idx, :] + cell.rec_variables['ina'][idx, :] + cell.rec_variables['ica'][idx, :] +
        #     cell.rec_variables['ihcn'][idx, :]), area_d) * 1e-3
        #
        # i_syn = np.zeros(i_m.shape)
        # for s in cell.synapses:
        #     if s.kwargs['section'] == section:
        #         syn_coor = np.asarray([s.x, s.y, s.z])
        #         d_ = 1 / distance_3d(syn_coor, EL_coor) * 1e6  # 1/d ,unit in m^-1
        #         i_syn = i_syn + s.i * d_ * 1e-9
        #         d = np.append(d, [d_], axis=0)
        #
        # a = np.asarray([g_j + np.sum(d) * C_EL / dt, -np.sum(d) * C_EL / dt])
        d = []
        area_d = []
        for i in idx:
            seg_coor = np.asarray([(cell.xstart[i] - cell.xend[i]) / 2, (cell.ystart[i] - cell.yend[i]) / 2,
                                   (cell.zstart[i] - cell.zend[i]) / 2])
            d_ = distance_3d(seg_coor, EL_coor) * 1e6  # d ,unit in m
            beta = beta_bar * math.exp(-(d_ / 1e6) / 100)
            area_d_ = beta * cell.area[i] * 1e-8  # bate*area, unit in cm^2
            d.append(d_)
            area_d.append(area_d_)
        d = np.asarray(d)
        area_d = np.asarray(area_d)
        i_m = np.mean(np.transpose(cell.imem[idx, :])) * 1e-9
        v_m = np.mean(cell.vmem[idx, :], axis=0) * 1e-3
        i_cap = np.mean(np.transpose(cell.icap[idx, :])) * 1e-9
        i_pas = np.mean(np.transpose(cell.ipas[idx, :])) * 1e-9
        i_ion = np.dot(np.transpose(
            cell.rec_variables['ik'][idx, :] + cell.rec_variables['ina'][idx, :] + cell.rec_variables['ica'][idx, :] +
            cell.rec_variables['ihcn'][idx, :]), area_d) * 1e-3

        i_syn = np.zeros(i_m.shape)
        for s in cell.synapses:
            if s.kwargs['section'] == section:
                syn_coor = np.asarray([s.x, s.y, s.z])
                d_ = distance_3d(syn_coor, EL_coor) * 1e6  # d ,unit in m
                i_syn = i_syn + s.i * 1e-9
                d = np.append(d, [d_], axis=0)

        a = np.asarray([np.sum(d) * g_j +  C_EL / dt, -C_EL / dt])

        z = signal.lfilter_zi(b, a)
        i_j = i_ion + i_cap + i_pas + i_syn
        # i_j = i_m
        v_j, _ = signal.lfilter(b, a, i_j, zi=z * i_j[0])

    if if_plot:
        time_range = [118, 123]
        plt.figure()
        plt.subplot(3, 1, 1)
        plt.plot(cell.tvec, i_cap * 1e9)  # in nA
        plt.xlim(time_range[0], time_range[1])
        plt.ylabel('i_ion (nA)')
        plt.subplot(3, 1, 2)
        plt.plot(cell.tvec, v_m * 1e3)  # in mV
        plt.xlim(time_range[0], time_range[1])
        plt.ylabel('V_m (mV)')
        plt.subplot(3, 1, 3)
        plt.plot(cell.tvec, v_j * 1e3)
        plt.xlim(time_range[0], time_range[1])
        plt.ylabel('V_j (mV)')
        plt.xlabel('t (ms)')

    return v_j * 1e3


def junctional_V_calc(cell, position, beta_bar=0.5, g_j=0.3, c_EL=1, if_plot=False):
    """
        calcultate the junctional voltage based on cable theory, adapted from: 
        
        Fromherz, Peter. “Extracellular recording with transistors and the distribution of ionic conductances in a cell membrane.” 
        European Biophysics Journal 28 (1999): 254-258.
        
        Im from all the sections will contribute to the junctional V
        d calculated as the center of electrode to the center of the section
        beta calculated as with exponential decay with d, tau = 10um

    Parameters
    ----------
    cell: LFPy.cell
    beta_bar: default is 0.01
        the maximum area fraction, 
    position: list, [x,y,z]
        3d position of the electrode, the unit is um
    g_j: defult is 1 (S/m)
        seal conductance (axial), the unit is uS/um (S/m)
    c_EL: default is 1 (10^-12 f)
        electrode capacitancem the unit is 10^-12 f 
    if_plot: default is False
        plot the Vm, im trace and the V_j trace

    Returns
    -------
    V_j: np.array 
        junction voltage calculated based on the given g_j and c_EL, unit is mV

    """
    v_j = []
    coor_EL = np.asarray(position)
    for sec in cell.allseclist:
        # coor_sec = locate_center(sec)
        # d = distance_3d(coor_EL, coor_sec)
        # beta = beta_bar * math.exp(-d/100)
        v_j_local = local_junctional_V_calc(cell, sec.name(), position=position, beta_bar=beta_bar, g_j=g_j, c_EL=c_EL,
                                            if_plot=False)
        v_j.append(v_j_local)

    v_j = np.sum(np.asarray(v_j), axis=0)
    if if_plot:
        time_range = [118, 123]
        plt.plot(cell.tvec, v_j)
        plt.ylabel('V_j (mV)')
        plt.xlabel('t (ms)')
        plt.xlim(time_range[0], time_range[1])
    return v_j

def local_Vj_calc_sanity_check(cell, sec='soma[0]', r=100, time_range=[85, 90]):
    """
    quick sanity check for the local Vj calculation, plot the V_j traces recorded in
    x-z plane with radius of r

    :param sec: str, name of the target section
    :param r: distance of the most remote electrode in the grid to the section, unit in um
    :param time_range: time range, unit in ms
    :return: NONE
    """
    idx = cell.get_idx(sec)
    i = int(np.median(idx))
    x = np.linspace(cell.xmid[i] - r, cell.xmid[i] + r, 10)
    y = np.linspace(cell.ymid[i] - r, cell.ymid[i] + r, 10)
    v = []
    for xx in x:
        for yy in y:
            v_j_local = local_junctional_V_calc(self.cell, sec, [xx, yy, cell.zmid[i]],
                                                beta_bar=0.5, g_j=0.3, c_EL=1, if_plot=False)
            v.append(v_j_local)
    v = np.asarray(v)

    maxv = []
    for i in range(100):
        maxv.append(max(abs(v[i, :])))
    maxima = np.nanmax(maxv)
    minima = np.nanmin(maxv)
    norm_color = matplotlib.colors.Normalize(vmin=minima, vmax=maxima, clip=True)
    mapper = cm.ScalarMappable(norm=norm_color, cmap=cm.viridis)

    fig, axs = plt.subplot(10, 10)
    for i in range(100):
        ax = axs[i%10, 9 - int(i/10)]
        ax.plot(cell.tvec, v[i, :], color=mapper.to_rgba(max(abs(v[i, :]))))
        ax.axis('off')
        ax.set_xlim(time_range[0], time_range[1])
    plt.colorbar(mapper)

def calc_csd(electrode, sigma = 0.3, LFP = None):
    """
    kernal method is used to estimate csd, only apply for uniform electrodes array or grid
    Gaussian window is used
    kCSD method adapted from
    Potworowski J, Jakuczun W, ?eski S, Wójcik D. Kernel current source density method. Neural Comput. 2012;24(2):541-575.
    doi:10.1162/NECO_a_00236

    :param electrode: LFP.ExtRecElectrode object
    :param sigma:
    :param LFP: if the potential other than electrode.LFP is used,
    should be in the same shape as electrode.LFP

    :return: csd (A/(cm)^2)
    """
    x = electrode.x
    y = electrode.y
    z = electrode.z

    if LFP is None:
        lfp = electrode.LFP
    else:
        lfp = LFP

    X = np.sort(np.unique(x))
    Y = np.sort(np.unique(y))
    Z = np.sort(np.unique(z))

    deltaX = np.unique(0.01*np.floor(np.diff(X)/0.01))
    deltaY = np.unique(0.01*np.floor(np.diff(Y)/0.01))
    deltaZ = np.unique(0.01*np.floor(np.diff(Z)/0.01))

    csd = []
    if deltaX.size == 0 and deltaZ.size == 0: # 1D electrode array
        l = 3 # length of kernal
        gausswinl = 21
        N = lfp.shape[0] # number of electrodes
        K = N # number of kernel is the same as number of observations
        b_wave = np.zeros([N,K])
        b = np.zeros([N,K])
        for i in range(N):
            b_wave[i, i] = 1
            a = signal.gaussian(gausswinl,
                                np.floor(gausswinl/2))
            a = a/np.sum(a)
            b_temp = np.convolve(a, b_wave[:, i])
            # c = b_temp.size
            b_wave[:, i] = b_temp[int(np.floor(gausswinl/2)):int(- np.floor(gausswinl/2))]

        A = np.zeros([N, N])
        for i in range(N):
            for j in range(N):
                A[i, j] = 1/(2*sigma)*(math.sqrt((y[i]-y[j]) ** 2 + (l/2*deltaY) ** 2) - math.sqrt((y[i] - y[j]) ** 2))
            b[i,:] = np.dot(A[i,:], b_wave)

        kernel = np.zeros([N, N])
        kernel_wave = np.zeros([N, N])
        for i in range(N):
            for j in range(N):
                kernel[i, j] = np.dot(b[i, :], np.transpose(b[j, :]))
                kernel_wave[i, j] = np.dot(b[i, :], np.transpose(b_wave[j, :]))
    elif deltaX.size == 0 or deltaZ.size == 0:
        l = np.array([5, 5])
        if deltaX.size == 0:
            Nh = Z.size
            H = z
        else:
            Nh = X.size
            H = x
        Nv = Y.size
        N = lfp.shape[0]
        K = N

        b_wave = np.zeros([N, K])
        b = np.zeros([N, K])

        for i in range(Nv):
            for j in range(Nh):
                b0 = np.zeros([Nh, Nv])
                b0[i, j] = 1
                b1 = scipy.ndimage.gaussian_filter(b0, sigma = [math.sqrt(l[0]), math.sqrt(l[1])])
                b_wave[:, i*Nh + j] = b1.flatten()

        h = 1
        A = np.zeros([N, N])
        for n in range(N):
            for i in range(Nv):
                for j in range(Nh):
                    a = np.sqrt((H[n] - H[j + Nh*i]) ** 2 + (y[n] - y[j + Nh*i]) ** 2)
                    if deltaZ.size == 0:
                        if a == 0:
                            A[n, j + Nh * i] = 0
                        else:
                            A[n, j+Nh*i] = 1/(2*np.pi*sigma)*np.arcsinh(2*h/a)
                    else:
                        A[n, j + Nh * i] = 1 / (2 * np.pi * sigma) * np.arcsinh(2 * h / math.sqrt(a**2 + h**2))
            b[n,:] = np.dot(A[n,:], b_wave)

        kernel = np.zeros([N, N])
        kernel_wave = np.zeros([N, N])
        for i in range(N):
            for j in range(N):
                kernel[i, j] = np.dot(b[i, :], np.transpose(b[j, :]))
                kernel_wave[i, j] = np.dot(b[i, :], np.transpose(b_wave[j, :]))
    else:
        raise ValueError("3D electrode not supported yet")

    csd = np.dot(np.divide(np.transpose(kernel_wave), kernel), lfp)
    return 1e5*csd



def remove_channel(cell, mechanism = None):
    """
    remove channels in a bath application manner
    :param cell: LFPy.Cell
    :param mechanism: str, name of the mechanism
    :return:
    """
    mt = h.MechanismType(0)
    mt.select(str(mechanism))
    for sec in h.allsec(cell):
        mt.remove(sec = sec)


def remove_channel_section(cell, sections = ['soma[0]'], mechanism = None):
    """
    remove channels in one or a list of sections
    :param cell: LFPy.Cell
    :param sections: str or list of str, name of the target section
    :param mechanism: str, name of the mechanism
    :return:
    """
    mt = h.MechanismType(0)
    mt.select(str(mechanism))
    for sec in h.allsec(cell):
        if sec.name() in sections:
            mt.remove(sec = sec)

def gen_poisson_spikes(FR, t_win, dt = 0.025, tstop = 250):
    """
    generate poisson spike given firing rate trace

    :param FR: ndarray (spike/s), firing rate at each time window
    :param t_win: (ms) length of time window
    :param dt: (ms), default 0.025
    :param tstop: (ms), default 250
    :return:
        spike_train: ndarray (ms), time point of spiking
    """
    t_squ = np.arange(0, tstop+t_win, t_win)
    spike_train = []
    if FR.size < t_squ.size - 1:
        warnings.warn('Not enough firing rates, will attach zeros to the end')
        np.append(FR, np.zeros(t_squ.size - 1 - FR.size))
    for i, fr in zip(range(t_squ.size - 1), FR):
        t_spike = t_squ[i]
        if fr < 0:
            break
        while t_spike < t_squ[i + 1]:
            t_spike = t_spike + random.expovariate(fr/1000)
            if t_spike < t_squ[i + 1]:
                spike_train.append(dt*np.floor(t_spike/dt))
    # for i, fr in zip(range(t_squ.size - 1), FR):
    #     a = np.random.uniform(0, 1, int(np.round(t_win/dt))) <= fr*dt/1000.0
    #     b = np.where(a is True)
    #     spike_train.append(t_squ[i]+dt*b[0])
    spike_train = np.asarray(spike_train)
    return spike_train

def create_full_synapse_map_json(cell, if_add_spines = False, if_cluster = False):
    """
    create the json file (only synapses and spines) for a full synapse map, which include:
    0.8 excitatory synapses (spines)/um, all randomly distributed
    0.2 inhibitory synapses / um

    All the synapses are randomly and uniformly distributed on dendrites (normalized to area)
    :param cell: LFPy.Cell object
    :return:
    """
    tot_area = 0
    for sec in cell.allseclist:
        if sec.name()[0:4] == 'dend' or sec.name()[0:4] == 'apic':
            for seg in sec:
                tot_area = tot_area + seg.area()

    num_spine = []
    num_e = []
    num_i = []
    sections = [] #name of corresponding section
    # if if_add_spines:
    #     for sec in cell.allseclist:
    #         if sec.name()[0:4] == 'dend' or sec.name()[0:4] == 'apic':
    #             sec_area = np.sum(cell.area[cell.get_idx(sec.name())])
    #             num_spine.append(np.round(600 / tot_area * sec_area))
    #             num_e.append(np.round(200 / tot_area * sec_area))
    #             num_i.append(np.round(200 / tot_area * sec_area))
    #             sections.append(sec.name())
    # else:
    #     for sec in cell.allseclist:
    #         if sec.name()[0:4] == 'dend' or sec.name()[0:4] == 'apic':
    #             sec_area = np.sum(cell.area[cell.get_idx(sec.name())])
    #             num_e.append(np.round(800 / tot_area * sec_area))
    #             num_i.append(np.round(200 / tot_area * sec_area))
    #             sections.append(sec.name())

    if if_add_spines:
        for sec in cell.allseclist:
            if sec.name()[0:4] == 'soma':
                area = 0
                for seg in sec:
                    area = area + seg.area()
                num_e.append(0)
                num_i.append(np.round(10*area/100))
                num_spine.append(0)
                sections.append(sec.name())
            elif sec.name()[0:4] == 'dend' or sec.name()[0:4] == 'apic':
                num_spine.append(np.round(sec.L/10))
                area = 0
                for seg in sec:
                    area = area + seg.area()
                num_e.append(np.round(55 * area / 100))  # since both AMPA and NMDA is inserted, number is decreased to 25 from 55-65
                num_i.append(np.round(10 * area / 100))
                sections.append(sec.name())
    else:
        # for sec in cell.allseclist:  # Ujfalussy Makara 2020 Nature communication
        #     if sec.name()[0:4] == 'soma':
        #         area = 0
        #         for seg in sec:
        #             area = area + seg.area()
        #         num_e.append(np.round(0.01*area))
        #         num_i.append(0)
        #         sections.append(sec.name())
        #     elif sec.name()[0:4] == 'dend' or sec.name()[0:4] == 'apic':
        #         num_e.append(np.round(0.8 * sec.L))
        #         num_i.append(np.round(0.2 * sec.L))
        #         sections.append(sec.name())
        for sec in cell.allseclist:  # Destexhe Sejnowski 2001
            if sec.name()[0:4] == 'soma':
                area = 0
                for seg in sec:
                    area = area + seg.area()
                num_e.append(0)
                num_i.append(np.round(10*area/100))  # 10-20/100um^2
                sections.append(sec.name())
            elif sec.name()[0:4] == 'dend' or sec.name()[0:4] == 'apic':
                area = 0
                for seg in sec:
                    area = area + seg.area()
                num_e.append(np.round(55*area/100)) # since both AMPA and NMDA is inserted, number is decreased to 25 from 55-65
                num_i.append(np.round(10*area/100))
                sections.append(sec.name())
            elif sec.name()[0:4] == 'axon':
                    area = 0
                    for seg in sec:
                        area = area + seg.area()
                    num_e.append(0)
                    num_i.append(np.round(60 * area / 100))  # 40-80/100um^2
                    sections.append(sec.name())

    import json
    data = {}
    data["synapse"] = []
    for e, i, section in zip(num_e, num_i, sections):
        if e > 0:
            data["synapse"].append(
                {
                    "section": [section],
                    "num": int(e),
                    "name": "AMPA"
                }
            )
            data["synapse"].append(
                {
                    "section": [section],
                    "num": int(e/10),
                    "name": "NMDA"
                }
            ) #NMDA channels are also inserted for each non-spine synapses
        if i > 0:
            data["synapse"].append(
                {
                    "section": [section],
                    "num": int(i),
                    "name": "GABA_A"
                }
            )
    if if_add_spines:
        if if_cluster:
            cluster_num = 10   # if a section has over cluster_num spines, they will be clustered
            data["spines"] = []
            for spine, section in zip(num_spine, sections):
                if spine >= cluster_num:
                    n_clust = 1 #int(spine/cluster_num)
                    num_list = [n_clust, spine]#list(np.concatenate((np.array([n_clust]), np.repeat(cluster_num, n_clust))))
                    for i in range(len(num_list)):
                        num_list[i] = int(num_list[i])
                    data["spines"].append(
                    {
                        "section": [section],
                        "type": "cluster",
                        "num": num_list,
                        "head_len": 0.5,
                        "head_diam": 0.5,
                        "neck_len": 1.58,
                        "neck_diam": 0.077,
                        "head_g_pas": 5e-5,
                        "head_e_pas": -80,
                        "neck_g_pas": 5e-5,
                        "neck_e_pas": -80,
                        "head_cm": 1,
                        "head_Ra": 100,
                        "neck_cm": 1,
                        "neck_Ra": 100
                    }
                    )
                    # if spine % cluster_num > 0:
                    #     data["spines"].append(
                    #         {
                    #             "section": [section],
                    #             "type": "rand",
                    #             "num": int(spine % cluster_num),
                    #             "head_len": 0.5,
                    #             "head_diam": 0.5,
                    #             "neck_len": 1.58,
                    #             "neck_diam": 0.077,
                    #             "head_g_pas": 5e-5,
                    #             "head_e_pas": -80,
                    #             "neck_g_pas": 5e-5,
                    #             "neck_e_pas": -80,
                    #             "head_cm": 1,
                    #             "head_Ra": 100,
                    #             "neck_cm": 1,
                    #             "neck_Ra": 100
                    #         }
                    #     )
                elif spine > 0:
                    data["spines"].append(
                    {
                        "section": [section],
                        "type": "rand",
                        "num": int(spine),
                        "head_len": 0.5,
                        "head_diam": 0.5,
                        "neck_len": 1.58,
                        "neck_diam": 0.077,
                        "head_g_pas": 5e-5,
                        "head_e_pas": -80,
                        "neck_g_pas": 5e-5,
                        "neck_e_pas": -80,
                        "head_cm": 1,
                        "head_Ra": 100,
                        "neck_cm": 1,
                        "neck_Ra": 100
                    }
                    )
        else:
            data["spines"] = []
            for spine, section in zip(num_spine, sections):
                if spine > 0:
                    data["spines"].append(
                    {
                        "section": [section],
                        "type": "rand",
                        "num": int(spine),
                        "head_len": 0.5,
                        "head_diam": 0.5,
                        "neck_len": 1.58,
                        "neck_diam": 0.077,
                        "head_g_pas": 5e-5,
                        "head_e_pas": -80,
                        "neck_g_pas": 5e-5,
                        "neck_e_pas": -80,
                        "head_cm": 1,
                        "head_Ra": 100,
                        "neck_cm": 1,
                        "neck_Ra": 100
                    }
                    )

    with open('C:\\work\\Code\\neuron-l5pn-model\\full_map.txt', 'w') as outfile:
        json.dump(data, outfile, indent=4)


def create_full_synapse_map_json_nospine(cell, if_cluster = False):
    """
    create the json file (only synapses and spines) for a full synapse map, which include:
    0.8 excitatory synapses (spines)/um, all randomly distributed
    0.2 inhibitory synapses / um

    All the synapses are randomly and uniformly distributed on dendrites (normalized to area)
    :param cell: LFPy.Cell object
    :return:
    """
    tot_area = 0
    for sec in cell.allseclist:
        if sec.name()[0:4] == 'dend' or sec.name()[0:4] == 'apic':
            for seg in sec:
                tot_area = tot_area + seg.area()

    num_spine = []
    num_e = []
    num_i = []
    sections = [] #name of corresponding section
    # if if_add_spines:
    #     for sec in cell.allseclist:
    #         if sec.name()[0:4] == 'dend' or sec.name()[0:4] == 'apic':
    #             sec_area = np.sum(cell.area[cell.get_idx(sec.name())])
    #             num_spine.append(np.round(600 / tot_area * sec_area))
    #             num_e.append(np.round(200 / tot_area * sec_area))
    #             num_i.append(np.round(200 / tot_area * sec_area))
    #             sections.append(sec.name())
    # else:
    #     for sec in cell.allseclist:
    #         if sec.name()[0:4] == 'dend' or sec.name()[0:4] == 'apic':
    #             sec_area = np.sum(cell.area[cell.get_idx(sec.name())])
    #             num_e.append(np.round(800 / tot_area * sec_area))
    #             num_i.append(np.round(200 / tot_area * sec_area))
    #             sections.append(sec.name())

    for sec in cell.allseclist:
        if sec.name()[0:4] == 'soma':
            area = 0
            for seg in sec:
                area = area + seg.area()
            num_e.append(0)
            # num_i.append(np.round(10*area/100))
            num_i.append(0)
            num_spine.append(0)
            sections.append(sec.name())
        elif sec.name()[0:4] == 'dend':
            num_spine.append(np.round(sec.L/10))
            area = 0
            for seg in sec:
                area = area + seg.area()
            num_e.append(np.round(5 * area / 100)) #5 # since both AMPA and NMDA is inserted, number is decreased to 25 from 55-65
            num_i.append(np.round(0.3 * area / 100)) #5
            sections.append(sec.name())
        elif sec.name()[0:4] == 'apic':
            num_spine.append(np.round(sec.L/30))
            area = 0
            for seg in sec:
                area = area + seg.area()
            num_e.append(np.round(5 * area / 100))  # since both AMPA and NMDA is inserted, number is decreased to 25 from 55-65
            num_i.append(np.round(0.4 * area / 100))
            sections.append(sec.name())

    import json
    data = {}
    data["synapse"] = []
    for e, i, section in zip(num_e, num_i, sections):
        if e > 0:
            data["synapse"].append(
                {
                    "section": [section],
                    "num": int(e),
                    "name": "AMPA",
                    "if_clustered": False
                }
            )
            # data["synapse"].append(
            #     {
            #         "section": [section],
            #         "num": int(e/10),
            #         "name": "NMDA",
            #         "if_clustered": False
            #     }
            # ) #NMDA channels are also inserted for each non-spine synapses
        if i > 0:
            data["synapse"].append(
                {
                    "section": [section],
                    "num": int(i),
                    "name": "GABA_A",
                    "if_clustered": False
                }
            )

    if if_cluster:
        cluster_num = 8   # if a section has over cluster_num spines, they will be clustered
        for spine, section in zip(num_spine, sections):
            if spine >= cluster_num:
                n_clust = 1 #int(spine/cluster_num)
                num_list = [n_clust, spine]#list(np.concatenate((np.array([n_clust]), np.repeat(cluster_num, n_clust))))
                for i in range(len(num_list)):
                    num_list[i] = int(num_list[i])
                data["synapse"].append(
                {
                    "section": [section],
                    "num": int(spine),
                    "name": "AMPA",
                    "if_clustered": True
                }
                )
                data["synapse"].append(
                    {
                        "section": [section],
                        "num": int(spine),
                        "name": "NMDA_Mg",
                        "if_clustered": True
                    }
                )

            elif spine > 0:
                data["synapse"].append(
                {
                    "section": [section],
                    "num": int(spine),
                    "name": "AMPA",
                    "if_clustered": False
                }
                )
                data["synapse"].append(
                    {
                        "section": [section],
                        "num": int(spine),
                        "name": "NMDA_Mg",
                        "if_clustered": False
                    }
                )

    with open('C:\\work\\Code\\neuron-l5pn-model\\full_map.txt', 'w') as outfile:
        json.dump(data, outfile, indent=4)


class LFPclass(object):
    """
    electrode_position: np assay, x, y and z position

    """

    def __init__(self, wd, verbool = False):
        self.wd = wd
        self.verbool = verbool
        self.has_spines = False
        self.n_pre_bouton = 0
        # self.electrode_position = np.asarray([0, 0, 0])
        # self.create_cell()
        # self.synapse_parameter = self.create_synapse()
        # self.electrode_parameter, self.electrode = self.create_electrode()

    def create_cell(self, if_add_spines = False, tstop = 250, cell_type = 'l5pn', dt = 0.1):
        """

        :param if_add_spines:
        :param tstop:
        :param cell_type: 'l5pn', 'l23pn'
        :return:
        """
        wd = self.wd
        # neuron.load_mechanisms(wd)
        manifest_file_name = 'manifest_' + cell_type + '.json'
        # self.description = runner.load_description(os.path.join(wd, manifest_file_name))
        # self.morphology_file = self.description.manifest.get_path("MORPHOLOGY")
        f = open(os.path.join(wd, manifest_file_name))
        self.description = json.load(f)
        f = open(self.description["biophys"][0]["model_file"][1])
        self.data = json.load(f)
        self.morphology_file = os.path.join(wd, 'cell1.asc')
        h.celsius = self.data['conditions'][0]['celsius']
        self.cell_parameters = {
            'morphology': self.morphology_file,
            # 'cm': 1,
            # 'Ra': self.data['passive'][0]['ra'],
            'v_init': self.data['conditions'][0]["v_init"],
            'passive': True,
            'nsegs_method': 'lambda_f',
            'lambda_f': 100,
            'tstop': tstop,
            'dt': dt,
            'pt3d': True,
            'custom_fun': [self.set_active_channel],
            'custom_fun_args': [{"if_add_spines": if_add_spines}],
        }
        self.cell = LFPy.Cell(**self.cell_parameters)
        if self.verbool:
                print("finish setting up the cell")
        self.set_stochastic_channel()
        try:
            self.set_active_channel_gradient()
        except KeyError:
            pass
        if if_add_spines:
            self.connect_spines()
            self.has_spines = True
        if cell_type == 'l23pn': # for l2/3pn in Branco 2010
            for sec in self.cell.allseclist:
                if h.ismembrane('ca_ion', sec = sec):
                    h.ion_style('ca_ion', 0, 1, 0, 0, 0, sec = sec)

        sections = [h.Section()]
        sec_coor = []

        for sec in h.allsec():
            sections.append(sec)

        sections.remove(sections[0])

        for sec in sections:
            try:
                sec_coor.append(locate_center(sec))
            except:
                pass

        sec_coor = np.asarray(sec_coor)
        self.xrange = [min(sec_coor[:, 0]), max(sec_coor[:, 0])]
        self.xmid = np.median(sec_coor[:, :])
        self.yrange = [min(sec_coor[:, 1]), max(sec_coor[:, 1])]
        self.ymid = np.median(sec_coor[:, 1])
        self.zrange = [min(sec_coor[:, 2]), max(sec_coor[:, 2])]
        self.zmid = np.median(sec_coor[:, 2])

    def create_cell_Larkum2009(self, tstop = 250, dt = 0.01, cell_type = 'l5pn'):
        wd = self.wd
        # neuron.load_mechanisms(wd)
        manifest_file_name = 'manifest_' + cell_type + '.json'
        # self.description = runner.load_description(os.path.join(wd, manifest_file_name))
        f = open(os.path.join(wd, manifest_file_name))
        self.description = json.load(f)
        f = open(self.description["biophys"][0]["model_file"][1])
        self.data = json.load(f)
        self.morphology_file = wd + '\\\larkumEtAl2009_2\\apical_simulation.hoc' #self.description.manifest.get_path("MORPHOLOGY")
        h.celsius = self.data['conditions'][0]['celsius']
        self.cell_parameters = {
            'morphology': self.morphology_file,
            # 'cm': 1,
            # 'Ra': self.description.data['passive'][0]['ra'],
            'v_init': -75,#self.description.data['conditions'][0]["v_init"],
            'passive': True,
            'nsegs_method': 'lambda_f',
            'lambda_f': 100,
            'tstop': tstop,
            'dt': dt,
            'pt3d': True,
            # 'custom_fun': [self.set_active_channel],
            # 'custom_fun_args': [{"if_add_spines": if_add_spines}],
        }
        self.cell = LFPy.Cell(**self.cell_parameters)

    def create_cell_Mainen1996(self, tstop = 250, dt = 0.01, cell_type = 'l5pn'):
        wd = self.wd
        # neuron.load_mechanisms(wd)
        manifest_file_name = 'manifest_' + cell_type + '.json'
        f = open(os.path.join(wd, manifest_file_name))
        self.description = json.load(f)
        f = open(self.description["biophys"][0]["model_file"][1])
        self.data = json.load(f)
        self.morphology_file = wd + '\\XiEtal2013\\initiate_cell.hoc' #self.description.manifest.get_path("MORPHOLOGY")
        h.celsius = self.data['conditions'][0]['celsius']
        self.cell_parameters = {
            'morphology': self.morphology_file,
            # 'cm': 1,
            # 'Ra': self.description.data['passive'][0]['ra'],
            'v_init': -75,#self.description.data['conditions'][0]["v_init"],
            'passive': True,
            'nsegs_method': 'lambda_f',
            'lambda_f': 100,
            'tstop': tstop,
            'dt': dt,
            'pt3d': True,
            # 'custom_fun': [self.set_active_channel],
            # 'custom_fun_args': [{"if_add_spines": if_add_spines}],
        }
        self.cell = LFPy.Cell(**self.cell_parameters)

    def create_synapse(self, if_from_file = True, syn_dict = [], mode = 'rand'):
        """
        Insert synapse on targeted section in a random manner
        The synapse information is stopred in the distription file

        only clustered spines have NMDA channels inserted

        "section": list the sections where the synapse is inserted
        "num": number of inserted synpase
        "name": name of the synapse, can be "AMPA", "NMDA", "NMDA_Mg" and "GABA_A" for now

        The name and section of the synapse is stored under synapse.kwargs

        modified 05/11/2020: NMDA as in Branco et al 2010 is added, named as NMDA_Mg

        if_from_file: bool
            1 denotes to importing the synapse dendity and function from file, 0 denotes to importing from inputs
        syn_dict: dictionary or list of dictionalies
            if import synaptic setting from input, the dictionary should include "section", "num" and "name"
        Returns
        -------
        None.

        """
        # TODO: find proper parameter for the alpha synapses, add into the function that these parameter can be inputed
        try:
            getattr(self, "synapses")
        except AttributeError:
            self.synapses = []

        try:
            getattr(self, "ex_NMDA")
        except AttributeError:
            self.ex_NMDA = []

        try:
            getattr(self, "PNT_synapses") # for the point process synapses (alpha synapse & NMDA_Mg)
        except AttributeError:
            self.PNT_synapses = []
        self.PNT_synapses = []
        self.pre = []
        synapseParameters_AMPA = {
            'e': 0,
            'syntype': 'Exp2Syn',
            'tau1': 0.1,
            'tau2': 2.,
            # 'weight': 0.001,
            'weight': 0.0005,   # weight as g_max in uS
            'record_current': True,
        }

        synapseParameters_NMDA = {
            'e': 0,
            'syntype': 'Exp2Syn',
            'tau1': 2.,
            'tau2': 40.,
            'weight': 0.0008,   # default as NMDA:AMPA = 2:1
            'record_current': True,
        }

        synapseParameters_NMDA_Mg = {
            'gmax': 12000,  # Note: unit for NMDA_Mg g is pS, 5000 for active model, 8000 for passive
        }

        synapseParameters_GABA_A = {
            'e': -80,
            'syntype': 'Exp2Syn',
            'tau1': 1.,
            'tau2': 40.,
            # 'weight': 0.001,
            'weight': 0.0006,
            'record_current': True,
        }

        synapseParameters_AlphaE = {
            'e': 0,
            'syntype': 'AlphaSynapse',
            'tau': 10.,
            'gmax': 10,
        }

        synapseParameters_AlphaI = {
            'e': -80,
            'syntype': 'AlphaSynapse',
            'tau': 10.,
            'gmax': 1,
        }

        self.n_pre_bouton = 0  # number of presynaptic boutons
        self.pre = []  # list of bouton sections
        for sec in h.allsec():
            if 'pre' in sec.name():
                self.pre.append(sec)
        self.g_vec = [] # list of conductance traces recorded from NMDA synapses
        self.glu_vec = [] # list of claft glutamate concentration

        if if_from_file:
            synapse = self.data['synapse']

            for s in synapse:
                name = s["name"]
                if name == "NMDA":
                    self.insert_synapses(synapseParameters_NMDA, s, mode = mode)
                elif name == "NMDA_Mg":
                    self.insert_synapses_NMDA_Mg(synapseParameters_NMDA_Mg, s,mode = mode)
                elif name == "AMPA":
                    self.insert_synapses(synapseParameters_AMPA, s,mode = mode)
                elif name == "GABA_A":
                    self.insert_synapses(synapseParameters_GABA_A, s,mode = mode)
                elif name == "AlphaE":
                    self.insert_synapses(synapseParameters_AlphaE, s,mode = mode)
                elif name == "AlphaI":
                    self.insert_synapses(synapseParameters_AlphaI, s,mode = mode)
                else:
                    raise ValueError('The synaptic type is not defined')
        else:
            if isinstance(syn_dict, list):
                for dict in syn_dict:
                    name = dict["name"]
                    if name == "NMDA":
                        self.insert_synapses(synapseParameters_NMDA, dict,mode = mode)
                    elif name == "AMPA":
                        self.insert_synapses(synapseParameters_AMPA, dict,mode = mode)
                    elif name == "NMDA_Mg":
                        self.insert_synapses_NMDA_Mg(synapseParameters_NMDA_Mg, dict,mode = mode)
                    elif name == "GABA_A":
                        self.insert_synapses(synapseParameters_GABA_A, dict,mode = mode)
                    elif name == "AlphaE":
                        self.insert_synapses(synapseParameters_AlphaE, dict,mode = mode)
                    elif name == "AlphaI":
                        self.insert_synapses(synapseParameters_AlphaI, dict,mode = mode)
            else:
                name = syn_dict["name"]
                if name == "NMDA":
                    self.insert_synapses(synapseParameters_NMDA, syn_dict,mode = mode)
                elif name == "NMDA_Mg":
                    self.insert_synapses_NMDA_Mg(synapseParameters_NMDA_Mg, syn_dict,mode = mode)
                elif name == "AMPA":
                    self.insert_synapses(synapseParameters_AMPA, syn_dict,mode = mode)
                elif name == "GABA_A":
                    self.insert_synapses(synapseParameters_GABA_A, syn_dict,mode = mode)
                elif name == "AlphaE":
                    self.insert_synapses(synapseParameters_AlphaE, syn_dict,mode = mode)
                elif name == "AlphaI":
                    self.insert_synapses(synapseParameters_AlphaI, syn_dict,mode = mode)
        if self.has_spines:
            for clust in self.spines:
                if clust["if_clustered"]:
                    d = {
                        "parent": clust["parent"],
                        "parent_seg": clust["parent_seg"],
                        "if_clustered": True
                    }
                    sec = clust["name"]
                    sec_dic = {
                        "section": [sec],
                        "num": 1,
                        "name": 'AMPA'
                    }
                    self.insert_synapses(synapseParameters_AMPA, sec_dic, synapse_dict = d)
                    sec_dic = {
                        "section": [sec],
                        "num": 1,
                        "name": 'NMDA_Mg'
                    }
                    self.insert_synapses_NMDA_Mg(synapseParameters_NMDA_Mg, sec_dic, synapse_dict = d)
                else:
                    d = {
                        "parent": clust["parent"],
                        "parent_seg": clust["parent_seg"],
                        "if_clustered": False
                    }
                    sec = clust["name"]
                    sec_dic = {
                        "section": [sec],
                        "num": 1,
                        "name": 'AMPA'
                    }
                    self.insert_synapses(synapseParameters_AMPA, sec_dic, synapse_dict=d)
                    sec_dic = {
                        "section": [sec],
                        "num": 1,
                        "name": 'NMDA_Mg'
                    }
                    self.insert_synapses_NMDA_Mg(synapseParameters_NMDA_Mg, sec_dic, synapse_dict=d)
                    # self.insert_synapses(synapseParameters_NMDA, [sec], 1, 'NMDA', synapse_dict=d)

        # if len(self.PNT_synapses) != len(self.pre):
        #     self.pre = self.pre[0:len(self.PNT_synapses)]

    def create_ext_stim(self, x = 50, y = 0, z = 0, sigma = 0.3, stim_type = 'sin', stim_delay = 50, stim_amp = 20,
                        stim_dur = 200, stim_freq = 5, stim_wave = None):
        """
        create extracellular stimulation electrode at location defined by x,y, and z
        (as regrading to the center of cell)
        The potential at each segment is simulated as potential from a monopole in a
        uniform sphere conductor
        with LFPy.OneSphereVolumeConductor, R is defaulted as 1e5 (10mm) and sigma_out is 0.03

        i in nA, phy in mV

        stimulation parameter can be defined in kwargs

        :param x: scalar (um), lateral offset from the stimulation electrode to the center of soma
        :param y: scalar (um), posterior offset from the stimulation electrode to the center of soma
        :param z:
        :param sigma: scalar (S/m), conductivity of the brain, default 0.3
        :param kwargs: stimulation parameters, default value:
        {
            "stim_type": "sin" ("sin" (phase = 0), "step", "noise", "defined")
            "stim_delay": (ms) 50
            "stim_amp": (nA) 20
            "stim_dur": (ms) 100
            "stim_freq": (Hz) 5 (only used by sinusoidal stim)
        }

        :return:
        """
        # generate the iext_stim trace

        time = np.arange(self.cell.tstart, self.cell.tstop, self.cell.dt)
        self.iext_stim = np.zeros(time.size)
        if stim_type is "defined":
            self.iext_stim = stim_wave
            stim_amp = (np.max(stim_wave)-np.min(stim_wave))/2
        else:
            for i, t in zip(range(time.size), time):
                if t < stim_delay or t >= stim_dur + stim_delay:
                    self.iext_stim[i] = 0
                else:
                    if stim_type is 'sin':
                        self.iext_stim[i] = stim_amp*np.sin(2*np.pi*(t-stim_delay)*1e-3*stim_freq)
                    elif stim_type is "step":
                        self.iext_stim[i] = stim_amp
                    elif stim_type is "noise":
                        self.iext_stim[i] = np.random.normal(loc=0.0, scale=stim_amp)
                        # white gaussian noise for now
                    else:
                        raise ValueError("wrong stimulation type")

        # calculate potential induced by the field stimulation
        x_el = self.cell.somapos[0] + x
        y_el = self.cell.somapos[1] + y
        z_el = self.cell.somapos[2] + z

        X = np.asarray(self.cell.xmid)
        Y = np.asarray(self.cell.ymid)
        Z = np.asarray(self.cell.zmid)
        r = np.array([np.sqrt(X ** 2 + Y ** 2 + Z**2),
                      np.arctan2(Y, X) - np.arctan2(y_el, x_el),
                      np.arccos(Z/np.sqrt(X ** 2 + Y ** 2 + Z**2)) - np.arccos(z_el/np.sqrt(x_el ** 2 + y_el ** 2 + z_el ** 2))]) # the spherical coordinates of segments as if electrode is on x axis
        nan_idx = np.argwhere(np.isnan(r))
        for idx in nan_idx:
            r[idx[0], idx[1]] = 0

        sphere = LFPy.OneSphereVolumeConductor(r, R=10000.,
                                                    sigma_i=sigma, sigma_o=0.03)
        r_el = np.sqrt(x_el ** 2 + y_el ** 2 + z_el ** 2)
        phi_max = sphere.calc_potential(rs=r_el, I=stim_amp)

        phi = []
        for i in self.iext_stim:
            phi.append(i/stim_amp*phi_max)

        self.phi_seg = np.transpose(np.asarray(phi))

        # add extracellular mechanism
        self.vecs = []
        for sec in self.cell.allseclist:
            sec.insert('extracellular')
            for idx, seg in zip(self.cell.get_idx(sec.name()), sec):
                self.vecs.append(h.Vector(self.phi_seg[idx, :]))

        try:
            for sec in self.cell.allseclist:
                for idx, seg in zip(self.cell.get_idx(sec.name()), sec):
                    self.vecs[idx].play(seg.extracellular._ref_e, self.cell.dt)
        except AttributeError:
            print('no extra stim defined')

    def create_int_stim_dynamic(self, sec = ['soma[0]'], electrode_type = ['IClamp'], stim_wave = None):
        """
        dynamic current/voltage stimulation
        :param sec: list of sections
        :param electrode_type: 'IClamp' or 'VClamp'
        :param stim_wave: list of ndarray
        :return:
        """
        self.p_process = [] #list of point process ('IClamp')
        self.d_stim = [] # list of dynamic stim
        for s, e_type, stim in zip(sec, electrode_type, stim_wave):
            tvec = h.Vector(np.arange(0, self.cell.dt * len(stim), self.cell.dt))
            if e_type is 'IClamp':
                for a in h.allsec():
                    if s in a.name():
                        self.p_process.append(h.IClamp(a(0.5)))
                        self.d_stim.append(h.Vector(stim))
                        self.p_process[-1].dur = self.cell_parameters['tstop']
                        self.p_process[-1].delay = 0
                        self.d_stim[-1].play(self.p_process[-1]._ref_amp,self.cell.dt)
                        break
            elif e_type is 'VClamp':
                for a in h.allsec():
                    if s in a.name():
                        self.p_process.append(h.VClamp(a(0.5)))
                        self.d_stim.append(h.Vector(stim))
                        self.d_stim[-1].play(self.p_process[-1]._ref_v, self.cell.dt)
                        break
            else:
                raise ValueError('wrong stim type')



    def create_int_stim(self, sec = ['soma[0]'], electrode_type = ['IClamp'], stim_delay = [50], stim_amp = [0.8],
                        stim_dur = [100]):
        """
        add intracellular stimulation electorde

        :param sec: list of str, name of the target section
        :param electrode_type: list of str, 'IClamp', 'VClamp' or 'SEClamp'
        :param stim_delay: list
        :param stim_amp: list
        :param stim_dur: list
        :return:
        """
        self.int_electrode_parameters = [] # list of dict
        self.int_electrode = [] # list of LFPy.StimIntElectrode

        for s ,type, delay, amp, dur in zip(sec, electrode_type, stim_delay, stim_amp, stim_dur):
            self.int_electrode_parameters.append(
                {
                    'idx': int(np.median(self.cell.get_idx(s))),
                    'record_current': True,
                    'pptype': type,
                    'amp': amp,
                    'dur': dur,
                    'delay': delay,
                }
            )

        for param in self.int_electrode_parameters:
            self.int_electrode.append(LFPy.StimIntElectrode(self.cell, **param))

    def create_electrode(self, x = 0, y = 0, z = 0, sigma=0.3):
        """
        create 1d electrod at a set position, for default, the electrode locates at the median value of x and z,
        spread along y the entire range of the neuronal morphology

        Parameters
        ----------
        sigma : TYPE, optional
            DESCRIPTION. The default is 0.3.
        x : np.array
            offset of x. The default is 0.
        y: np.array
            offset of y. The default is 0.
        z: np.array
            offset of z. The default is 0.

        Returns
        -------
        None.

        """
        N = np.empty((32, 3))
        for i in range(N.shape[0]):
            N[i, :] = [1, 0, 0]

        self.electrodeParameters = {
            'sigma': sigma,  # Extracellular potential
            'x': x,  # x,y,z-coordinates of electrode contacts
            'y': y,
            'z': z,
            'n': 10,  # number of point on the electrode surface to average the potential
            'r': 5,  # radius of electrode surface
            'N': N,
            'method': 'pointsource',
        }
        self.electrode = LFPy.RecExtElectrode(self.cell, **self.electrodeParameters)

    def create_electrode_array(self, sigma=0.3, x_off=0, y_off=0, z_off=0, nChannel = 64):
        """
        create 1d electrod at a set position, for default, the electrode locates at the median value of x and z, 
        spread along y the entire range of the neuronal morphology

        Parameters
        ----------
        sigma : TYPE, optional
            DESCRIPTION. The default is 0.3.
        x_off : TYPE
            offset of x. The default is 0.
        y_off : TYPE
            offset of y. The default is 0.
        z_off : TYPE
            offset of z. The default is 0.
        nChannel : TYPE
            number of channels on the array. The default is 64.

        Returns
        -------
        None.

        """
        N = np.empty((nChannel, 3))
        x = np.zeros(nChannel) + round(self.xmid) + x_off
        y = np.linspace(self.yrange[0] - 5, self.yrange[1] + 5, nChannel) + y_off
        y = np.flip(y, axis = 0)
        z = np.zeros(64) + round(self.zmid) + z_off
        for i in range(N.shape[0]):
            N[i, :] = [1, 0, 0]

        self.electrodeParameters = {
            'sigma': sigma,  # Extracellular potential
            'x': x,  # x,y,z-coordinates of electrode contacts
            'y': y,
            'z': z,
            'n': 10,  # number of point on the electrode surface to average the potential
            'r': 5,  # radius of electrode surface
            'N': N,
            'method': 'pointsource',
        }
        self.electrode = LFPy.RecExtElectrode(self.cell, **self.electrodeParameters)

    def create_electrode_section(self, sigma = 0.3, sec = 'soma[0]', r = 100):
        # TODO: change the synaptic stimulation to generate BAC firing
        # TODO: also kill the somatic Na channel
        """
        create a grid around a certain section (on the x-z plane)
        :param sigma: conductivity of extracellular space, default is 0.3
        :param sec: name of the target section, default is 'soma[0]'
        :param r: distance of the most distant electrode to the section center, in um, default is 100
        :param timerange: time range if ploting, default is [89, 90]
        :return:
        """
        idx = self.cell.get_idx(sec)
        i = int(np.median(idx))
        x = np.linspace(self.cell.xmid[i] - r, self.cell.xmid[i] + r, 10)
        y = np.linspace(self.cell.ymid[i] - r, self.cell.ymid[i] + r, 10)
        y = np.flip(y, axis = 0)
        xx, yy = np.meshgrid(x, y)
        zz = np.ones(100)*self.cell.zmid[i] # - 10

        # x = np.linspace(-97.02317958 - r, -97.02317958 + r, 10)
        # z = np.linspace(45.88266056 - r, 45.88266056 + r, 10)
        # xx, zz = np.meshgrid(x, z)
        # yy = np.ones(100)*886.00947656 # - 10

        # x = np.linspace(-104.1329386 - r, -104.1329386 + r, 10)
        # z = np.linspace(self.cell.zmid[i] - r, self.cell.zmid[i] + r, 10)
        # xx, zz = np.meshgrid(x, z)
        # yy = np.ones(100)*1014.50966945 # - 10

        N = np.empty((100, 3))
        for i in range(N.shape[0]):
            N[i, :] = [1, 0, 0]
        self.electrodeParameters = {
            'sigma': sigma,  # Extracellular potential
            'x': xx,  # x,y,z-coordinates of electrode contacts
            'y': yy,
            'z': zz,
            'n': 10,  # number of point on the electrode surface to average the potential
            'r': 5,  # radius of electrode surface
            'N': N,
            'method': 'pointsource',
        }

        self.electrode = LFPy.RecExtElectrode(self.cell, **self.electrodeParameters)


    def set_simulation(self, spike_times = [], trg_syns = []):
        """
        synaptic input into certain sections, and initialize the stimulation

        :param spike_times: 1d np.array or list of np.array, presynaptic spike time in ms
        :param trg_secs: str or list of str, name of target section (can be vague such as 'apic') or target Alphasynapse ('Alpha')
        should have the same shape as spike_time
        :return:
        """

        for spike_time, trg_syn in zip(spike_times, trg_syns):
            syn = []
            for s in self.cell.synapses:
                if trg_syn in s.kwargs['section']:
                    syn.append([s])
            try:
                for s, pre in zip(self.PNT_synapses, self.pre):
                    if trg_syn in s.get_segment().sec.name():
                        syn.append([s, pre])
            except AttributeError:
                pass

            if (len(syn)>1) and (len(syn) == len(spike_time)):  # each element in spike_time corresponds to spike time for each synapse
                for s, t in zip(syn, spike_time):
                    if len(s)>1: # PNT synapse
                        s[1].del_rel = t
                    else:
                        for s0 in s:
                            if "GABA" not in s0.kwargs["name"]:
                                s0.kwargs['spike_time'] = np.asarray([t])
                                s0.set_spike_times(np.asarray([t]))
            elif (len(syn) == 1): # one synapse with a sequense of presynaptic spikes
                if len(syn[0]) > 1:  # PNT synapse
                    syn[0][1].del_rel = spike_time
                else:
                    for s0 in syn[0]:
                        if "GABA" not in s0.kwargs["name"]:
                            s0.kwargs['spike_time'] = spike_time
                            s0.set_spike_times(spike_time)
            elif (len(syn) > 1) and (len(spike_time)==1): # all synapses receive simultaneous input
                for s in syn:
                    if len(s)>1: # PNT synapse
                        s[1].del_rel = spike_time[0]
                    else:
                        for s0 in s:
                            if "GABA" not in s0.kwargs["name"]:
                                s0.kwargs['spike_time'] = spike_time
                                s0.set_spike_times(spike_time)
            else:
                print(len(syn))
                print(len(spike_time))
                raise ValueError("length of spike time doesn't align")

        try:
            self.simulationParameters = {
                'rec_imem': True,
                'rec_vmem': True,
                # 'rec_ipas': True,
                # 'rec_icap': True,
                # 'rec_variables': ['cai', 'ik', 'ina', 'ica', 'ihcn'],
                # 'rec_variables': ['ihcn_Ih', 'i_pas'],
                'electrode': self.electrode,
            }
        except AttributeError:
            self.simulationParameters = {
                'rec_imem': True,
                'rec_vmem': True,
                # 'rec_variables': ['gSKv3_1_SKv3_1', 'gSK_E2_SK_E2', 'gK_Tst_K_Tst_2F', 'gK_Pst_K_Pst_2F', 'gIm_Im', 'cai'],
            }

        self.cell.simulate(**self.simulationParameters)

    def set_full_connect_simulation(self, e_freq = 1, i_freq = 4, freqmod_i_apic = 15, freqmod_i_base = 0, freqmod_nonclust = 0, freqmod_clust = 0, phase = 0.0, phase_i = 0.0, t_win = 10):
        """
        This will only be applied to a fully connected cell model. The excitatory synaptic input will have a frequency of
        e_freq and inhibitory FR will be i_freq. The synaptic input will be generate with poisson process.

        updated 05052020: the clustered spines will be co-activated
        updated 02222022: The excitatory input is modulated (4 HZ, 10 times higher ave. frequency) for clustered spines

        e_freq/i_freq: list of two values, the first for apical and second for basal
        freq_nonclust: freqeuncy for modulating the non-clustered excitatory inputs, 0 means no modulation
        freq_clust: frequency for modulating the clustered excitatory inputs, 0 means no modulation
        phase: phase difference between clustered and non-clustered inputs, range 0 tp 2pi

        """
        # synaptic inputs not on spines
        self.idx_st = 0
        self.spike_trains = []
        t = np.arange(self.cell.tstart, self.cell.tstop+t_win, t_win)
        if len(i_freq) < 2:
            i_freq = [i_freq, i_freq]
        if len(e_freq) < 2:
            e_freq = [e_freq, e_freq]
        if freqmod_nonclust == 0.0:
            FR_e_nonclust = np.repeat(e_freq[0], t.size)
        else:
            amp = e_freq[0]*np.sqrt(2) / 2
            FR_e_nonclust = amp * np.sin(2 * np.pi * freqmod_nonclust * t / 1000+np.pi/2) + amp
        if freqmod_clust == 0.0:
            FR_e_clust = np.repeat(e_freq[0]/4, t.size)
        else:
            amp = e_freq[0]/4* np.sqrt(2) / 2
            FR_e_clust = amp * np.sin(2 * np.pi * freqmod_clust * t / 1000 + np.pi / 2 + phase) + amp  # out of phase with other imputs
        if freqmod_i_apic == 0.0:
            FR_i_apic = np.repeat(i_freq[0]/2, t.size)
        else:
            FR_i_apic = i_freq[0]*np.sqrt(2)/2*np.sin(2*np.pi*freqmod_i_apic*t/1000)+ i_freq[0]*np.sqrt(2)/2
        if freqmod_i_base == 0.0:
            FR_i_base = np.repeat(i_freq[1], t.size)
        else:
            FR_i_base = i_freq[1]*np.sqrt(2)/2*np.sin(2*np.pi*freqmod_i_base*t/1000 + phase_i)+i_freq[1]*np.sqrt(2)/2
        # FR_i_base = np.repeat(0, t.size)
        for s, idx in zip(self.cell.synapses, range(len(self.cell.synapses))):
            if ("shead" not in s.kwargs["section"]) and ("sneck" not in s.kwargs["section"]):
                if s.kwargs["name"] == "AMPA" or s.kwargs["name"] == "NMDA":
                    try:
                        if s.kwargs["if_clustered"]:
                            continue
                    except KeyError:
                        pass
                    if "dend" in s.kwargs["section"]:
                        spike_train = gen_poisson_spikes(FR_e_nonclust*float(e_freq[1]/e_freq[0]), t_win, self.cell.dt, self.cell.tstop)
                    else:
                        spike_train = gen_poisson_spikes(FR_e_nonclust, t_win,
                                                         self.cell.dt, self.cell.tstop)
                    s.set_spike_times(spike_train)
                    setattr(s, 'spike_time', spike_train)
                    self.spike_trains.append(spike_train)
                    self.idx_st = self.idx_st + 1
                elif s.kwargs["name"] == "GABA_A":
                    try:
                        if s.kwargs["if_clustered"]:
                            continue
                    except KeyError:
                        pass
                    if "apic" in s.kwargs["section"]:
                        spike_train = gen_poisson_spikes(FR_i_apic, t_win, self.cell.dt, self.cell.tstop)
                    else:
                        spike_train = gen_poisson_spikes(FR_i_base, t_win, self.cell.dt, self.cell.tstop)
                    s.set_spike_times(spike_train)
                    setattr(s, 'spike_time', spike_train)
                    self.spike_trains.append(spike_train)
                    self.idx_st = self.idx_st + 1
                else:
                    print("unknown synaptic type")
            else:
                break

        # point process synapses
        # if len(self.PNT_synapses) != len(self.pre):
        #     raise ValueError("mismatch in number fo synapses and presynaptic boutons")
        idx_NMDA = 0
        for s, pre, idx_NMDA in zip(self.PNT_synapses, self.pre, range(len(self.PNT_synapses))):
            # Note: For now it only support NMDG_Mg synapses, which require same number of presynaptic boutons and synapses
            if (("shead" not in s.get_segment().sec.name()) and ("sneck" not in s.get_segment().sec.name())):
                try:
                    if self.ex_NMDA[idx_NMDA]["if_clustered"]:
                        continue
                except KeyError:
                    pass
                if "dend" in s.get_segment().sec.name():
                    spike_train = gen_poisson_spikes(FR_e_nonclust*float(e_freq[1]/e_freq[0]), t_win, self.cell.dt, self.cell.tstop)
                else:
                    spike_train = gen_poisson_spikes(FR_e_nonclust, t_win, self.cell.dt, self.cell.tstop)
                # Note: for now just taking the first time point of a spike train, which will potentially cause a non-uniform
                # distribution of spiking probability
                if spike_train.size != 0:
                    self.set_spiketrain_to_bouton(spike_train,pre)
                    self.spike_trains.append(spike_train)
                    self.ex_NMDA[idx_NMDA].update({"spike_time": spike_train})
                    self.idx_st = self.idx_st + 1
                else:
                    self.spike_trains.append(np.asarray([]))
                    self.ex_NMDA[idx_NMDA].update({"spike_time": np.asarray([])})
                    self.idx_st = self.idx_st + 1
            else:
                break


        synapse_list = self.data['synapse']

        for s in synapse_list:
            sections = s["section"]
            num = s["num"]
            name = s["name"]
            try:
                if s["if_clustered"]:
                    if "dend" in sections:
                        spike_train = gen_poisson_spikes(FR_e_clust*float(e_freq[1]/e_freq[0]), t_win, self.cell.dt, self.cell.tstop)
                    else:
                        spike_train = gen_poisson_spikes(FR_e_clust, t_win, self.cell.dt, self.cell.tstop)
                    if "AMPA" in s["name"]:
                        for syn in self.cell.synapses:
                            if syn.kwargs["section"] == sections:
                                syn.set_spike_times(spike_train)
                                setattr(s, 'spike_time', spike_train)
                                self.spike_trains.append(spike_train)
                                self.idx_st = self.idx_st + 1
                    elif "NMDA_Mg" in s["name"]:
                        for syn, pre, idx_NMDA in zip(self.PNT_synapses, self.pre, range(len(self.PNT_synapses))):
                            if syn.get_segment().sec.name() == sections:
                                if spike_train.size != 0:
                                    self.set_spiketrain_to_bouton(spike_train, pre)
                                    self.ex_NMDA[idx_NMDA].update({"spike_time": spike_train})
                                    self.spike_trains.append(spike_train)
                                    self.idx_st = self.idx_st + 1
                                else:
                                    self.spike_trains.append(np.asarray([]))
                                    self.ex_NMDA[idx_NMDA].update({"spike_time": np.asarray([])})
                                    self.idx_st = self.idx_st + 1
                else:
                    continue
            except KeyError:
                pass

        if self.has_spines is True:
            # synaptic inputs on spines
            spine_synapses = self.cell.synapses[idx:]
            if (hasattr(self, 'pre') and self.pre != []):
                spine_pre = self.pre[idx_NMDA:]
                spine_NMDA = self.PNT_synapses[idx_NMDA:]
            idx = 0
            print(len(spine_NMDA))
            print(len(spine_synapses))
            while idx <= len(spine_synapses) -1:
                try:
                    if spine_synapses[idx].kwargs["section"] != spine_NMDA[idx].get_segment().sec.name():
                        raise ValueError("wrong alignment")
                except IndexError:
                    pass

                if spine_synapses[idx].kwargs["if_clustered"]:
                    parent = spine_synapses[idx].kwargs["parent"]
                    parent_seg = spine_synapses[idx].kwargs["parent_seg"]
                    idx_end = idx
                    while ((spine_synapses[idx_end].kwargs["parent"] == parent)
                            and (spine_synapses[idx_end].kwargs["parent_seg"] == parent_seg)
                            and (spine_synapses[idx_end].kwargs["if_clustered"])):
                        idx_end = idx_end + 1
                    if "dend" in spine_synapses[idx_end].kwargs["parent"]:
                        spike_train = gen_poisson_spikes(FR_e_clust*float(e_freq[1]/e_freq[0]), t_win, self.cell.dt, self.cell.tstop)
                    else:
                        spike_train = gen_poisson_spikes(FR_e_clust, t_win, self.cell.dt, self.cell.tstop)
                    # spike_train = np.asarray([spike_train])
                    for i in list(np.arange(idx, idx_end+1)):
                        if spike_train.size != 0:
                            spine_synapses[i].set_spike_times(spike_train)
                            self.set_spiketrain_to_bouton(spike_train,pre)
                            setattr(spine_synapses[i], 'spike_time', spike_train)
                            self.spike_trains.append(spike_train)
                            self.idx_st = self.idx_st + 1
                        else:
                            self.spike_trains.append(np.asarray([]))
                            self.idx_st = self.idx_st + 1
                    idx = idx_end
                else:
                    # spike_train = gen_poisson_spikes(FR_e, t_win, self.cell.dt, self.cell.tstop)
                    # spine_synapses[idx].set_spike_times(spike_train)
                    # spine_synapses[idx+1].set_spike_times(spike_train)
                    # setattr(spine_synapses[idx], 'spike_time', spike_train)
                    # setattr(spine_synapses[idx+1], 'spike_time', spike_train)
                    # idx = idx + 2
                    # FR_e = np.repeat(e_freq, t.size)
                    if "dend" in spine_synapses[idx_end].kwargs["parent"]:
                        spike_train = gen_poisson_spikes(FR_e_nonclust*float(e_freq[1]/e_freq[0]), t_win, self.cell.dt, self.cell.tstop)
                    else:
                        spike_train = gen_poisson_spikes(FR_e_nonclust, t_win, self.cell.dt, self.cell.tstop)
                    if spike_train.size != 0:
                        spine_synapses[idx].set_spike_times(spike_train)
                        self.set_spiketrain_to_bouton(spike_train,pre)
                        setattr(spine_synapses[idx], 'spike_time',spike_train)
                        self.spike_trains.append(spike_train)
                        self.idx_st = self.idx_st + 1
                    else:
                        self.spike_trains.append(np.asarray([]))
                        self.idx_st = self.idx_st + 1
                idx = idx + 1  #+ 2

        try:
            self.simulationParameters = {
                'rec_imem': True,
                'rec_vmem': True,
                # 'rec_variables': ['cai'],
                'electrode': self.electrode,
            }
        except AttributeError:
            self.simulationParameters = {
                'rec_imem': True,
                'rec_vmem': True,
                # 'rec_variables': ['cai'],
            }

        # self.cell.simulate(**self.simulationParameters)

    def set_full_connect_simulation_w_inputs(self):
        """
        repeat the fully connected model with the same defined input
        """
        # synaptic inputs not on spines
        idx_st_local = 0
        for s, idx in zip(self.cell.synapses, range(len(self.cell.synapses))):
            if ("shead" not in s.kwargs["section"]) and ("sneck" not in s.kwargs["section"]):
                try:
                    if s.kwargs["if_clustered"]:
                        continue
                except KeyError:
                    pass
                if s.kwargs["name"] == "AMPA" or s.kwargs["name"] == "NMDA":
                    spike_train = self.spike_trains[idx_st_local]
                    idx_st_local = idx_st_local + 1
                    s.set_spike_times(spike_train)
                    setattr(s, 'spike_time', spike_train)
                elif s.kwargs["name"] == "GABA_A":
                    spike_train = self.spike_trains[idx_st_local]
                    idx_st_local = idx_st_local + 1
                    s.set_spike_times(spike_train)
                    setattr(s, 'spike_time', spike_train)
                else:
                    print("unknown synaptic type")
            else:
                break

        # point process synapses
        # if len(self.PNT_synapses) != len(self.pre):
        #     raise ValueError("mismatch in number fo synapses and presynaptic boutons")

        for s, pre, idx_NMDA in zip(self.PNT_synapses, self.pre, range(len(self.PNT_synapses))):
            # Note: For now it only support NMDG_Mg synapses, which require same number of presynaptic boutons and synapses
            if (("shead" not in s.get_segment().sec.name()) and ("sneck" not in s.get_segment().sec.name())):
                try:
                    if self.ex_NMDA[idx_NMDA].kwargs["if_clustered"]:
                        continue
                except KeyError:
                    pass
                spike_train = self.spike_trains[idx_st_local]
                idx_st_local = idx_st_local + 1
                # Note: for now just taking the first time point of a spike train, which will potentially cause a non-uniform
                # distribution of spiking probability
                if spike_train.size != 0:
                    pre.del_rel = spike_train[0]
            else:
                break

        synapse_list = self.data['synapse']

        for s in synapse_list:
            sections = s["section"]
            num = s["num"]
            name = s["name"]
            try:
                if s["if_clustered"]:
                    if "AMPA" in s["name"]:
                        for syn in self.cell.synapses:
                            if syn.kwargs["section"] == sections:
                                spike_train = self.spike_trains[idx_st_local]
                                idx_st_local = idx_st_local + 1
                                syn.set_spike_times(spike_train)
                                setattr(s, 'spike_time', spike_train)
                    elif "NMDA_Mg" in s["name"]:
                        for syn, pre, idx_NMDA in zip(self.PNT_synapses, self.pre, range(len(self.PNT_synapses))):
                            if syn.get_segment().sec.name() == sections:
                                spike_train = self.spike_trains[idx_st_local]
                                idx_st_local = idx_st_local + 1
                                if spike_train.size != 0:
                                    self.set_spiketrain_to_bouton(spike_train, pre)
                                else:
                                    self.spike_trains.append(np.asarray([]))
                else:
                    continue
            except KeyError:
                pass
        if self.has_spines is True:
            # synaptic inputs on spines
            spine_synapses = self.cell.synapses[idx:]
            if (hasattr(self, 'pre') and self.pre != []):
                spine_pre = self.pre[idx_NMDA:]
                spine_NMDA = self.PNT_synapses[idx_NMDA:]
            idx = 0
            while idx <= len(spine_synapses) -1:
                try:
                    if spine_synapses[idx].kwargs["section"] != spine_NMDA[idx].get_segment().sec.name():
                        raise ValueError("wrong alignment")
                except IndexError:
                    pass

                if spine_synapses[idx].kwargs["if_clustered"]:
                    parent = spine_synapses[idx].kwargs["parent"]
                    parent_seg = spine_synapses[idx].kwargs["parent_seg"]
                    idx_end = idx
                    while ((spine_synapses[idx_end].kwargs["parent"] == parent)
                            and (spine_synapses[idx_end].kwargs["parent_seg"] == parent_seg)
                            and (spine_synapses[idx_end].kwargs["if_clustered"])):
                        idx_end = idx_end + 1
                    spike_train = self.spike_trains[idx_st_local]
                    for i in list(np.arange(idx, idx_end+1)):
                        if spike_train.size != 0:
                            spine_synapses[i].set_spike_times(spike_train)
                            spine_pre[i].del_rel = spike_train[0]
                            setattr(spine_synapses[i], 'spike_time', spike_train)
                            idx_st_local = idx_st_local + 1
                        else:
                            idx_st_local = idx_st_local + 1
                    idx = idx_end
                else:
                    # spike_train = gen_poisson_spikes(FR_e, t_win, self.cell.dt, self.cell.tstop)
                    # spine_synapses[idx].set_spike_times(spike_train)
                    # spine_synapses[idx+1].set_spike_times(spike_train)
                    # setattr(spine_synapses[idx], 'spike_time', spike_train)
                    # setattr(spine_synapses[idx+1], 'spike_time', spike_train)
                    # idx = idx + 2
                    spike_train = self.spike_trains[idx_st_local]
                    idx_st_local = idx_st_local + 1
                    if spike_train.size != 0:
                        spine_synapses[idx].set_spike_times(spike_train)
                        spine_pre[idx].del_rel = spike_train[0]
                        setattr(spine_synapses[idx], 'spike_time',spike_train)
                idx = idx + 1  #+ 2
        if idx_st_local != self.idx_st:
            warnings.warn("not correctly aligned")

        try:
            self.simulationParameters = {
                'rec_imem': True,
                'rec_vmem': True,
                'rec_variables': ['cai'],
                'electrode': self.electrode,
            }
        except AttributeError:
            self.simulationParameters = {
                'rec_imem': True,
                'rec_vmem': True,
                'rec_variables': ['cai'],
            }

        # self.cell.simulate(**self.simulationParameters)

    def get_potential_map(self, sigma = 0.3, if_plot = True, time_point = 80):
        """
        (post hoc) generate the potential distribution of the cell activity in a uniform sphere
        The function is not tested yet

        :param sigma: (S/m) extracellular conductivity
        :param if_plot: bool
        :param time_point: (ms) the time point for plotting the potential
        :return:
        """

        X, Z = np.mgrid[self.cell.somapos[0]-500:self.cell.somapos[0]+501:10., self.cell.somapos[2]-300:self.cell.somapos[2]+701:10.]
        Y = np.zeros(X.shape)
        r = np.array([np.sqrt(X ** 2 + Z ** 2).flatten(),
                      np.arccos(Z / np.sqrt(X ** 2 + Z ** 2)).flatten(),
                      np.arctan2(Y, X).flatten()])
        sphere = LFPy.OneSphereVolumeConductor(r=r, R=10000.,
                 sigma_i = sigma, sigma_o = 0.03)
        mapping = sphere.calc_mapping(self.cell, n_max=1000)
        self.mapped_phi = np.dot(mapping, self.cell.imem) # poential of each time points are column vectors
        if if_plot:
            Phi = self.mapped_phi[:,self.cell.tvec == time_point].reshape(X.shape)
            fig, ax = plt.subplots(1, 1)
            zips = []
            for x, z in self.cell.get_idx_polygons(projection=('x', 'z')):
                zips.append(zip(x, z))
            polycol = PolyCollection(zips,
                           edgecolors = 'none',
            facecolors = 'gray')
            vrange = 1E-3  # limits for color contour plot
            im = ax.contour(X, Z, Phi,
                      levels = np.linspace(-vrange, vrange, 41))
            circle = plt.Circle(xy=(0, 0), radius=sphere.R, fc='none', ec='k')
            ax.add_collection(polycol)
            ax.add_patch(circle)
            ax.axis(ax.axis('equal'))
            ax.set_xlim(X.min(), X.max())
            ax.set_ylim(Z.min(), Z.max())
            fig.colorbar(im, ax=ax)
            plt.show()

    def set_cell_loc(self, x = 2500, y = 1200, z = 4000):
        """
        set the x,y,z location of the cell as regarding to the center of the brain
        :param x: scalar (um), towards lateral
        :param y: scalar (um), towards dorsal
        :param z: scalar (um)
        :return:
        """
        self.cell.set_pos(x,y,z)

    def set_active_channel(self, if_add_spines = False):
        """
        adapted from allensdk.model.biophysical.utils.load_cell_parameters

        Returns
        -------
        None.

        """
        passive = self.data['passive'][0]
        genome = self.data['genome']
        conditions = self.data['conditions'][0]

        # Set fixed passive properties
        for sec in h.allsec():
            sec.Ra = passive['ra']
            sec.insert('pas')
            try:
                for seg in sec:
                    seg.pas.e = passive["e_pas"]
            except KeyError:
                pass
                # print("the model is all-active", end = '\n')
        if self.verbool:
            print("Setting Ra to be %.6g in all sections" % passive['ra'])
            print("Setting e_pas to be %.6g in all sections" % passive['e_pas'])
        # get the cm for each section
        try:
            for c in passive["cm"]:
                if 'soma' in c.keys():
                    cm_soma = c["cm"]
                elif 'axon' in c.keys():
                    cm_axon = c["cm"]
                elif 'apic' in c.keys():
                    cm_apic = c["cm"]
                elif 'dend' in c.keys():
                    cm_dend = c["cm"]
            for sec in h.allsec():
                if sec.name()[0:3] == "soma":
                    sec.cm = cm_soma
                elif sec.name()[0:3] == "apic":
                    sec.cm = cm_apic
                elif sec.name()[0:3] == "dend":
                    sec.cm = cm_dend
                elif sec.name()[0:3] == "axon":
                    sec.cm = cm_axon
                else:
                    print("Error in assigning cm in %s, set as 1" % sec.name())
                    sec.cm = 1
        except KeyError:
            pass
            # print("the model is all-active", end='\n')

        # Insert channels and set parameters
        for p in genome:
            section_array = p["section"]
            mechanism = p["mechanism"]
            param_name = p["name"]
            param_value = float(p["value"])
            if section_array == "glob":  # global parameter
                for sec in h.allsec():
                    param = getattr(sec, p["name"])
                    param = p["value"]
            else:
                if mechanism != "":
                    for sec in h.allsec():
                        if sec.name()[0:4] in section_array:
                            if h.ismembrane(str(mechanism),
                                            sec=sec) != 1:
                                sec.insert(str(mechanism))
                                if self.verbool:
                                    print('Adding mechanism %s to %s'
                                          % (mechanism, sec.name()))
                            setattr(sec, param_name + "_" + mechanism, param_value)
                else:
                    for sec in h.allsec():
                        if sec.name()[0:4] in section_array:
                            setattr(sec, param_name, param_value)
                if self.verbool:
                    print('Setting %s to %.6g in %s'
                          % (param_name, param_value, section_array))

        for erev in conditions['erev']:
            for sec in h.allsec():
                if sec.name()[0:3] in erev["section"]:
                    try:
                        sec.ek = float(erev["ek"])
                        sec.ena = float(erev["ena"])
                        sec.eca = float(erev["eca"])
                    except:
                        pass  # print('No Na or K channel in ' + erev["section"])

        if if_add_spines:
            self.insert_spines()
            # exec('h(\'create shead[' + str(self.nspine) + ']\')')
            # exec('h(\'create sneck[' + str(self.nspine) + ']\')')
            # exec('h(\'create pre[' + str(self.nspine + self.numNMDA_Mg) + ']\')')
        else:
            try:
                syns = self.data['synapse']
                self.numNMDA_Mg = 0
                for s in syns:
                    num = s["num"]
                    name = s["name"]
                    if name == "NMDA_Mg":
                        self.numNMDA_Mg = self.numNMDA_Mg + num
                if self.numNMDA_Mg > 0:
                    exec('h(\'create pre[' + str(self.numNMDA_Mg) + ']\')')
            except AttributeError:
                pass


    def set_stochastic_channel(self):
        passive = self.data['passive'][0]
        genome = self.data['genome']
        conditions = self.data['conditions'][0]
        # Insert channels and set parameters
        for p in genome:
            section_array = p["section"]
            mechanism = p["mechanism"]
            param_name = p["name"]
            param_value = float(p["value"])
            if section_array == "glob":  # global parameter
                for sec in h.allsec():
                    param = getattr(sec, p["name"])
                    param = p["value"]
            else:
                if '2F' in mechanism:
                    for sec in h.allsec():
                        if sec.name()[0:4] in section_array:
                            if '2F' in mechanism:
                                N_name = 'N' + mechanism[0:-3]
                                if mechanism in ['NaTs2_t_2F', 'NaTa_t_2F']:
                                    N = np.round(param_value*np.sum(self.cell.area[self.cell.get_idx(sec.name())])*100/5)
                                else:
                                    N = np.round(param_value * np.sum(self.cell.area[self.cell.get_idx(sec.name())]) * 100 / 40)
                                # assume that each channel has conductance of 10pS
                                setattr(sec, N_name + '_' + mechanism, N)
                            if self.verbool:
                                print('Changing num of channel in mechanism %s in %s to %d'
                                      % (mechanism, sec.name(), N))


    def set_active_channel_gradient(self):
        gradient = self.data['gradient']
        for g in gradient:
            section_array = g["section"]
            mechanism = g["mechanism"]
            value_name = g["name"]
            value = g["value"]
            function = g["function"]
            if mechanism != "":
                if self.verbool:
                    print('Adding mechanism %s to %s'
                          % (mechanism, section_array))
                if function == 'linear':
                    distribute_channels_linear(cell = self.cell, sec_name=section_array, mec=mechanism, value_name=value_name,
                                               value=value, verbool = self.verbool)
                elif function == 'sigmoid':
                    distribute_channels_sigmoid(cell = self.cell, sec_name = section_array, mec = mechanism, value_name = value_name,
                                                value = value, verbool = self.verbool)
                elif function == 'step':
                    distribute_channels_step(cell = self.cell, sec_name=section_array, mec=mechanism, value_name=value_name, value=value,
                                             verbool = self.verbool)
                elif function == 'exponential':
                    distribute_channels_exponential(cell = self.cell, sec_name=section_array, mec=mechanism, value_name=value_name,
                                                    value=value, verbool = self.verbool)
                else:
                    raise ValueError('the function doesn\'t exist')
            else:
                raise ValueError('the mechanism is empty')

    def set_deficite_channels(self, mec_name, prop_name,sec_name = 'all',  percentage = 0.3):
        """

        set conductance of certain channel to a certain percentage of original channel
        :param mec_name:
        :param prop_name:
        :param sec_name:
        :param percentage:
        :return:
        """
        if sec_name == 'all':
            for sec in self.cell.allseclist:
                for seg in sec:
                    try:
                        seg_mec = getattr(seg, str(mec_name))
                        current_value = getattr(seg_mec, prop_name)
                        setattr(seg_mec, prop_name, current_value*percentage)
                        if self.verbool:
                            print('change %s in %s from %.6g to %.6g' %(prop_name, sec.name(), current_value, current_value*percentage))
                    except AttributeError:
                        pass
        else:
            for sec in self.cell.allseclist:
                if sec.name()[0:4] in sec_name:
                    for seg in sec:
                        try:
                            seg_mec = getattr(seg, str(mec_name))
                            current_value = getattr(seg_mec, prop_name)
                            setattr(seg_mec, prop_name, current_value * percentage)
                            if self.verbool:
                                print('change %s in %s from %.6g to %.6g' % (
                                prop_name, sec.name(), current_value, current_value * percentage))
                        except AttributeError:
                            pass

    def insert_spines(self):
        """
        insert spines on the targeted section as specified in biophys file, the spines are passive sections consist of
        heads and necks

        :param spine_list: list of dictionaries
        {
            "section": name of the target section
            "type": 'cluster', 'rand'
            "num": number of spines (rand dist), or [number of clusters, number of spine in each cluster] (cluster)
            "shead_len"
            "shead_diam"
            "sneck_len"
            "sneck_diam"
            "head_g_pas"
            "head_e_pas"
            "neck_g_pas"
            "neck_e_pas"
            "head_cm"
            "head_Ra"
            "neck_cm"
            "neck_Ra"
        }

        :return:
        """

        spines = self.data['spines']

        self.nspine = 0
        for spine in spines:
            if isinstance(spine["num"], list):
                n = spine["num"][0]*spine["num"][1]
            else:
                n = spine["num"]
            n_sec = len(spine["section"])
            self.nspine = self.nspine + n*n_sec


        syns = self.data['synapse']
        #
        self.numNMDA_Mg = 0
        for s in syns:
            num = s["num"]
            name = s["name"]
            if name == "NMDA_Mg":
                self.numNMDA_Mg = self.numNMDA_Mg + num
        # if self.verbool:
        #     print(str(self.nspine))
        exec('h(\'create shead[' + str(self.nspine) + ']\')')
        exec('h(\'create sneck[' + str(self.nspine) + ']\')')
        exec('h(\'create pre[' + str(self.nspine+self.numNMDA_Mg) + ']\')')

    def set_active_basal_dendrites(self, if_add_spines = False):
        """
        add active channels to basal dendrites referenced to
        Gao PP, Graham JW, Zhou WL, Jang J, Angulo S, Dura-Bernal S, Hines M, Lytton WW, Antic SD. Local glutamate-mediated dendritic plateau potentials change the state of the cortical pyramidal neuron. J Neurophysiol. 2021 Jan 1;125(1):23-42. doi: 10.1152/jn.00734.2019. Epub 2020 Oct 21. PMID: 33085562; PMCID: PMC8087381.

        Returns
        -------
        None.

        """
        # Insert channels and set parameters
        verbool = self.verbool
        soma_seg_idx = self.cell.get_idx('soma[0]')
        for sec in h.allsec():
            if sec.name()[0:4] == 'dend':    # basal dendrites
                sec.insert('na')
                sec.insert('ca')   # HVA-Ca
                sec.insert('it')  # LVA-Ca
                sec.insert('kv')
                sec.insert('kBK')
                sec.gpeak_kBK = 2.68e-4 *100  # changing from S/cm^2 to mho /Cm^2
                sec.gbar_kv = 40
                if verbool:
                    print('Adding mechanism na to %s' %(sec.name()))
                    print('Adding mechanism ca to %s' % (sec.name()))
                    print('Adding mechanism CaT to %s' % (sec.name()))
                    print('Adding mechanism kv to %s' % (sec.name()))
                    print('Adding mechanism kBK to %s' % (sec.name()))
                    print('Setting gpeak_kBK to 0.0268 in %s' %(sec.name()))
                    print('Setting gbar_kv to 40 in %s' %(sec.name()))
                for seg, seg_idx in zip(sec, self.cell.get_idx(sec.name())):
                    d = self.cell.get_intersegment_distance(soma_seg_idx, seg_idx)
                    gbar_na = 150-d*0.5
                    if gbar_na <= 0:
                        gbar_na = 0
                    elif gbar_na >= 2000:
                        gbar_na = 2000
                    if verbool:
                        print('Setting gbar_na to %.6g in %s'
                              % (gbar_na, sec.name()))
                    seg_mec = getattr(seg, 'na')
                    setattr(seg_mec, 'gbar', gbar_na)
                    sec.insert('kad')
                    gbar_kad = (150 + 0.7 * d) * (1 / 300 * d) / 1e4  # change from pS/um2 to mho/cm^2
                    if gbar_kad <= 0:
                        gbar_kad = 0
                    elif gbar_kad >= 2000:
                        gbar_kad = 2000
                    seg_mec = getattr(seg, 'kad')
                    setattr(seg_mec, 'gkabar', gbar_kad)
                    sec.insert('kap')
                    gbar_kap = (150 + 0.7 * d) * (1 - 1 / 300 * d) / 1e4  # change from pS/um2 to mho/cm^2
                    if gbar_kap <= 0:
                        gbar_kap = 0
                    elif gbar_kap >= 2000:
                        gbar_kap = 2000
                    seg_mec = getattr(seg, 'kap')
                    setattr(seg_mec, 'gkabar', gbar_kap)
                    if verbool:
                        print('Adding mechanism kad to %s' % (sec.name()))
                        print('Setting gbar_kad to %.6g in %s' % (gbar_kad, sec.name()))
                        print('Adding mechanism kap to %s' % (sec.name()))
                        print('Setting gbar_kap to %.6g in %s' % (gbar_kap, sec.name()))
                    if d >= 30:  #distal
                        seg_mec = getattr(seg, 'ca')
                        setattr(seg_mec, 'gbar', 0.4)
                        seg_mec = getattr(seg, 'it')
                        setattr(seg_mec, 'gbar', 1.6/1e4)   # change from pS/um2 to mho/cm^2
                        if verbool:
                            print('Setting gbar_ca to 0.4 in %s' % (sec.name()))
                            print('Setting gbar_it to 1.6e-4 in %s' %(sec.name()))
                    else:
                        seg_mec = getattr(seg, 'ca')
                        setattr(seg_mec, 'gbar', 2)
                        seg_mec = getattr(seg, 'it')
                        setattr(seg_mec, 'gbar', 2/1e4)   # change from pS/um2 to mho/cm^2
                        if verbool:
                            print('Setting gbar_ca to 0.4 in %s' % (sec.name()))
                            print('Setting gbar_it to 2e-4 in %s' %(sec.name()))
                    if d >= 50:  # "spine factor", *2 for gpas and cm
                        seg.g_pas = 3e-5 * 2
                        seg.cm = 2
                    else:
                        seg.g_pas = 3e-5
                        seg.cm = 1

    def set_active_basal_dendrites_stochastic(self, if_add_spines = False):
        """
        add active channels to basal dendrites referenced to
        Gao PP, Graham JW, Zhou WL, Jang J, Angulo S, Dura-Bernal S, Hines M, Lytton WW, Antic SD. Local glutamate-mediated dendritic plateau potentials change the state of the cortical pyramidal neuron. J Neurophysiol. 2021 Jan 1;125(1):23-42. doi: 10.1152/jn.00734.2019. Epub 2020 Oct 21. PMID: 33085562; PMCID: PMC8087381.
        na is set as stochastic channels
        Returns
        -------
        None.

        """
        # Insert channels and set parameters
        verbool = self.verbool
        soma_seg_idx = self.cell.get_idx('soma[0]')
        for sec in h.allsec():
            if sec.name()[0:4] == 'dend':    # basal dendrites
                sec.insert('na_2F')
                sec.insert('ca')   # HVA-Ca
                sec.insert('it')  # LVA-Ca
                sec.insert('kv')
                sec.insert('kBK')
                sec.gpeak_kBK = 2.68e-4 *100  # changing from S/cm^2 to mho /Cm^2
                sec.gbar_kv = 40
                if verbool:
                    print('Adding mechanism na_2F to %s' %(sec.name()))
                    print('Adding mechanism ca to %s' % (sec.name()))
                    print('Adding mechanism CaT to %s' % (sec.name()))
                    print('Adding mechanism kv to %s' % (sec.name()))
                    print('Adding mechanism kBK to %s' % (sec.name()))
                    print('Setting gpeak_kBK to 0.0268 in %s' %(sec.name()))
                    print('Setting gbar_kv to 40 in %s' %(sec.name()))
                for seg, seg_idx in zip(sec, self.cell.get_idx(sec.name())):
                    d = self.cell.get_intersegment_distance(soma_seg_idx, seg_idx)
                    gbar_na = 150-d*0.5
                    if gbar_na <= 0:
                        gbar_na = 0
                    elif gbar_na >= 2000:
                        gbar_na = 2000
                    Nna =  np.round(gbar_na*np.sum(self.cell.area[self.cell.get_idx(sec.name())])/5)
                    setattr(sec, 'Nna_na_2F', Nna)
                    if self.verbool:
                        print('Changing num of channel in mechanism %s in %s to %d'
                              % ('na_2F', sec.name(), Nna))
                    if self.verbool:
                        print('Setting gbar_na to %.6g in %s'
                              % (gbar_na, sec.name()))
                    seg_mec = getattr(seg, 'na_2F')
                    setattr(seg_mec, 'gbar', gbar_na)
                    sec.insert('kad')
                    gbar_kad = (150 + 0.7 * d) * (1 / 300 * d) / 1e4  # change from pS/um2 to mho/cm^2
                    if gbar_kad <= 0:
                        gbar_kad = 0
                    elif gbar_kad >= 2000:
                        gbar_kad = 2000
                    seg_mec = getattr(seg, 'kad')
                    setattr(seg_mec, 'gkabar', gbar_kad)
                    sec.insert('kap')
                    gbar_kap = (150 + 0.7 * d) * (1 - 1 / 300 * d) / 1e4  # change from pS/um2 to mho/cm^2
                    if gbar_kap <= 0:
                        gbar_kap = 0
                    elif gbar_kap >= 2000:
                        gbar_kap = 2000
                    seg_mec = getattr(seg, 'kap')
                    setattr(seg_mec, 'gkabar', gbar_kap)
                    if verbool:
                        print('Adding mechanism kad to %s' % (sec.name()))
                        print('Setting gbar_kad to %.6g in %s' % (gbar_kad, sec.name()))
                        print('Adding mechanism kap to %s' % (sec.name()))
                        print('Setting gbar_kap to %.6g in %s' % (gbar_kap, sec.name()))
                    if d >= 30:  #distal
                        seg_mec = getattr(seg, 'ca')
                        setattr(seg_mec, 'gbar', 0.4)
                        seg_mec = getattr(seg, 'it')
                        setattr(seg_mec, 'gbar', 1.6/1e4)   # change from pS/um2 to mho/cm^2
                        if verbool:
                            print('Setting gbar_ca to 0.4 in %s' % (sec.name()))
                            print('Setting gbar_it to 1.6e-4 in %s' %(sec.name()))
                    else:
                        seg_mec = getattr(seg, 'ca')
                        setattr(seg_mec, 'gbar', 2)
                        seg_mec = getattr(seg, 'it')
                        setattr(seg_mec, 'gbar', 2/1e4)   # change from pS/um2 to mho/cm^2
                        if verbool:
                            print('Setting gbar_ca to 0.4 in %s' % (sec.name()))
                            print('Setting gbar_it to 2e-4 in %s' %(sec.name()))
                    if d >= 50:  # "spine factor", *2 for gpas and cm
                        seg.g_pas = 3e-5 * 2
                        seg.cm = 2
                    else:
                        seg.g_pas = 3e-5
                        seg.cm = 1

    def set_strong_basal_dendrite(self, sec_name = 'dend[5]'):
        """
        set BSP to a certain basal branch (blocking Ia and make gnabar = 200)
        :param sec_name:
        :return:
        """
        gna_strong = 800
        for sec in h.allsec():
            if sec.name() == sec_name:
                for seg in sec:
                    try:
                        seg.na.gbar = gna_strong
                    except AttributeError:
                        seg.na_2F.gbar = gna_strong
                    seg.kad.gkabar = 0
                    seg.kap.gkabar = 0
                    if self.verbool:
                        print('Setting gbar_na to %.6g in %s' % (gna_strong, sec.name()))
                        print('Setting gbar_kad to %.6g in %s' % (seg.kad.gkabar, sec.name()))
                        print('Setting gbar_kap to %.6g in %s' % (seg.kap.gkabar, sec.name()))



    def connect_spines(self):
        """
        insert passive channels to the spines, and connect them to the parent sections

        :param spine_list: list of dictionaries
        {
            "section": name of the target section
            "type": 'cluster', 'rand'
            "num": number of spines (rand dist), or [number of clusters, [number of spine in each cluster]] (cluster)
            "shead_len"
            "shead_diam"
            "sneck_len"
            "sneck_diam"
            "head_g_pas"
            "head_e_pas"
            "neck_g_pas"
            "neck_e_pas"
            "head_cm"
            "head_Ra"
            "neck_cm"
            "neck_Ra"
        }

        :return:
        """
        if self.verbool:
            print("adding spines")
        self.spines = []  # This attribute saves dictionaries of information of each spine head, include:
        # name / list of names within a cluster
        # parent section
        # parent segment
        # clustered or not
        spines = self.data['spines']
        shead_sec = []
        sneck_sec = []
        for sec in h.allsec():
            if 'shead' in sec.name():
                shead_sec.append(sec)
            elif 'sneck' in sec.name():
                sneck_sec.append(sec)

        spine_idx = 0
        for spine in spines:
            sec_name = spine["section"]

            if spine["type"] == 'rand':
                for p_sec in h.allsec():
                    if p_sec.name() in sec_name:
                        n = spine["num"]
                        segs = self.cell.get_rand_idx_area_norm(section=p_sec.name(), nidx=n)
                        for seg_idx in segs:
                            head = shead_sec[spine_idx]
                            head.L = spine["head_len"]
                            head.diam = spine["head_diam"]
                            head.pt3dclear()
                            s_x = self.cell.xmid[seg_idx]+self.cell.diam[seg_idx]/2
                            s_z = self.cell.zmid[seg_idx] + self.cell.diam[seg_idx] / 2
                            xvec = list(np.linspace(s_x+spine["neck_len"], s_x + spine["neck_len"] + spine["head_len"], 4))
                            yvec = list(np.repeat(self.cell.ymid[seg_idx], 4))
                            zvec = list(np.repeat(s_z, 4))
                            dvec = list(np.repeat(spine["head_diam"], 4))
                            for x, y, z, d in zip(xvec, yvec, zvec, dvec):
                                head.pt3dadd(x, y, z, d)
                            head.insert('pas')
                            head.g_pas = spine["head_g_pas"]
                            head.e_pas = spine["head_e_pas"]
                            head.cm = spine["head_cm"]
                            head.Ra = spine["head_Ra"]
                            neck = sneck_sec[spine_idx]
                            neck.L = spine["neck_len"]
                            neck.diam = spine["neck_diam"]
                            neck.pt3dclear()
                            xvec = list(np.linspace(s_x, s_x + spine["neck_len"], 4))
                            yvec = list(np.repeat(self.cell.ymid[seg_idx], 4))
                            zvec = list(np.repeat(s_z, 4))
                            dvec = list(np.repeat(spine["neck_diam"], 4))
                            for x,y,z,d in zip(xvec, yvec, zvec, dvec):
                                neck.pt3dadd(x,y,z,d)
                            neck.insert('pas')
                            neck.g_pas = spine["neck_g_pas"]
                            neck.e_pas = spine["neck_e_pas"]
                            neck.cm = spine["neck_cm"]
                            neck.Ra = spine["neck_Ra"]
                            head.nseg = 1
                            neck.nseg = 1
                            head.connect(neck, 1, 0)
                            for seg in p_sec:
                                if seg.area() == self.cell.area[seg_idx]:
                                    neck.connect(p_sec, seg.x, 0)
                                    # print("%s connected to %s on index %d" % (neck.name(), p_sec.name(), int(seg_idx)))
                                    break
                            if self.verbool:
                                print('Adding spine %s on parent branch %s' % (neck.name(), p_sec.name() + '[' + str(seg.x) + ']'))
                            spine_idx = spine_idx + 1
                            self.spines.append(
                                {
                                    "name": head.name(),
                                    "parent": p_sec.name(),
                                    "parent_seg": seg_idx,
                                    "if_clustered": False,
                                }
                            )
            elif spine["type"] == 'cluster':
                for p_sec in h.allsec():
                    if p_sec.name() in sec_name:
                        nclust = spine["num"][0]
                        ns = spine["num"][1:]
                        if nclust > self.cell.get_idx(p_sec.name()).size:
                            print('Note: nclust (%d) larger than nseg (%d), please decrease number of clusters'
                                             % (nclust, self.cell.get_idx(p_sec.name()).size))
                            nclust = self.cell.get_idx(p_sec.name()).size
                            ns = ns[0:nclust]
                        for ii in range(10):
                            segs = self.cell.get_rand_idx_area_norm(section=p_sec.name(), nidx=nclust)
                            if np.unique(segs).size == segs.size:
                                break
                        for seg_idx, n in zip(segs, ns):
                            names = []
                            for i in range(n):
                                head = shead_sec[spine_idx]
                                head.L = spine["head_len"]
                                head.diam = spine["head_diam"]
                                head.pt3dclear()
                                s_x = self.cell.xmid[seg_idx]+self.cell.diam[seg_idx]/2
                                s_z = self.cell.zmid[seg_idx] + self.cell.diam[seg_idx] / 2
                                xvec = list(np.linspace(s_x+spine["neck_len"], s_x + spine["neck_len"] + spine["head_len"], 4))
                                yvec = list(np.repeat(self.cell.ymid[seg_idx], 4))
                                zvec = list(np.repeat(s_z, 4))
                                dvec = list(np.repeat(spine["head_diam"], 4))
                                for x, y, z, d in zip(xvec, yvec, zvec, dvec):
                                    head.pt3dadd(x, y, z, d)
                                head.insert('pas')
                                head.g_pas = spine["head_g_pas"]
                                head.e_pas = spine["head_e_pas"]
                                head.cm = spine["head_cm"]
                                head.Ra = spine["head_Ra"]
                                neck = sneck_sec[spine_idx]
                                neck.L = spine["neck_len"]
                                neck.diam = spine["neck_diam"]
                                neck.pt3dclear()
                                xvec = list(np.linspace(s_x, s_x + spine["neck_len"], 4))
                                yvec = list(np.repeat(self.cell.ymid[seg_idx], 4))
                                zvec = list(np.repeat(s_z, 4))
                                dvec = list(np.repeat(spine["neck_diam"], 4))
                                for x,y,z,d in zip(xvec, yvec, zvec, dvec):
                                    neck.pt3dadd(x,y,z,d)
                                neck.insert('pas')
                                neck.g_pas = spine["neck_g_pas"]
                                neck.e_pas = spine["neck_e_pas"]
                                neck.cm = spine["neck_cm"]
                                neck.Ra = spine["neck_Ra"]
                                head.nseg = 1
                                neck.nseg = 1
                                head.connect(neck, 1, 0)
                                for seg in p_sec:
                                    if seg.area() == self.cell.area[seg_idx]:
                                        neck.connect(p_sec, seg.x, 0)
                                        # print("%s connected to %s on index %d" % (neck.name(), p_sec.name(), int(seg_idx)))
                                        break
                                if self.verbool:
                                    print('Adding clustered spine %s on parent branch %s' % (neck.name(), p_sec.name() + '[' + str(seg.x) + ']'))
                                spine_idx = spine_idx + 1
                                names.append(head.name())
                                self.spines.append(
                                    {
                                        "name": head.name(),
                                        "parent": p_sec.name(),
                                        "parent_seg": seg_idx,
                                        "if_clustered": True,
                                    }
                                )

        if self.cell.pt3d:
            self.cell.x3d, self.cell.y3d, self.cell.z3d, self.cell.diam3d = self.cell._collect_pt3d()
            self.cell._update_pt3d()
        else:
            self.cell._collect_geometry()

    def insert_synapses(self, synapse_parameters, syn_dict, synapse_dict = {}, mode = 'rand'):
        """
        insert synpase to the cell model
    
        Parameters
        ----------
        synapse_parameters : dict
            dictionary contains the physiological properties of a synapse
        section : list of str
            list of postsynaptic sections where synapse will be inserted
        n : TYPE
            number of synapse to insert
        name: str
            name of the synapse ('AMPA', 'NMDA' or 'GABA_A')
        synapse_dict: dict
            the information in the dictionary will be updated to the synapse.kwarg
    
        Returns
        -------
        None.
    
        """
        section = syn_dict['section']
        n = syn_dict['num']
        name = syn_dict['name']
        for sec in section:
            if mode == 'rand':
                idx = self.cell.get_rand_idx_area_norm(section=sec, nidx=n)   # insert n synapses in random location on the section
            elif mode == 'fixed':
                seg_list = self.cell.get_idx(section = sec)   # insert n synapse in middle of section
                idx = [seg_list[int(np.floor(len(seg_list)/2))]]*n
            else:
                idx = [mode]*n
            synapse_parameters_arg = syn_dict



            for i in idx:
                if 'Alpha' in name:
                    for section in h.allsec(): #self.cell.allseclist:
                        for seg in section:
                            if seg.area() == self.cell.area[i]:
                                syn_Alpha = h.AlphaSynapse(seg)
                                if self.verbool:
                                    print('Adding synapse %s to %s at the index %d' % (name, sec, int(i)))
                                for param in list(synapse_parameters_arg.keys()):
                                    try:
                                        if sys.version >= "3.4":
                                            exec('syn_Alpha.' + param + '=' + str(synapse_parameters_arg[param]),
                                                 locals(), globals())
                                        else:
                                            exec('syn_Alpha.' + param + '=' + str(synapse_parameters_arg[param]))
                                    except:
                                        pass
                                self.PNT_synapses.append(syn_Alpha)

                else:
                    synapse_parameters.update({'idx': int(i)})
                    synapse_parameters.update(synapse_parameters_arg)
                    synapse_parameters.update(synapse_dict)
                    synapse_parameters.update({"section": section[0]})
                    synapse_parameters.update({'record_current': True})

                    # Create synapse(s) and setting times using the Synapse class in LFPy
                    synapse = LFPy.Synapse(self.cell, **synapse_parameters)
                    # synapse.set_spike_times(np.array(150.))

                    self.synapses.append(synapse)
                    if self.verbool:
                        print('Adding synapse %s to %s at the index %d' % (name, sec, int(i)))

    def insert_synapses_NMDA_Mg(self, synapse_parameters, syn_dict, synapse_dict={}, mode = 'rand'):
        """
        insert NMDA synapses onto the cell (Branco et al 2010)

        Parameters
        ----------
        synapse_parameters : dict
            dictionary contains the physiological properties of a synapse

        section : list of str
            list of postsynaptic sections where synapse will be inserted
        n : TYPE
            number of synapse to insert
        name: str
            name of the synapse ('NMDA_Mg')
        synapse_dict: dict
            the information in the dictionary will be updated to the synapse.kwarg

        Returns
        -------
        None.

        """
        section = syn_dict['section']
        n = syn_dict['num']
        name = syn_dict['name']
        for sec in section:
            if (("shead" not in sec) and ("sneck" not in sec)):
                if mode == 'rand':
                    idx = self.cell.get_rand_idx_area_norm(section=sec,
                                                           nidx=n)  # insert n synapses in random location on the section
                elif mode == 'fixed':
                    seg_list = self.cell.get_idx(section=sec)  # insert n synapse in middle of section
                    idx = [seg_list[int(np.floor(len(seg_list) / 2))]] * n
                else:
                    idx = [mode] * n

                for i in idx:
                    for sect in h.allsec():#self.cell.allseclist:
                        for seg in sect:
                            if seg.area() == self.cell.area[i]:
                                syn_NMDA = h.NMDA_Mg_T(seg)
                                # pre = h.Section(name = 'pre[' +str(self.n_pre_bouton) + ']')
                                pre = self.pre[self.n_pre_bouton]
                                pre.L = 1
                                pre.diam = 1
                                # self.pre.append(pre)
                                self.n_pre_bouton = self.n_pre_bouton + 1
                                pre.insert('rel')
                                pre.dur_rel = 1
                                pre.amp_rel = 2
                                h.Erev_NMDA_Mg_T = 5
                                h.mg_NMDA_Mg_T = 1
                                for param in list(synapse_parameters.keys()):
                                    try:
                                        if sys.version >= "3.4":
                                            exec('syn_NMDA.' + param + '=' + str(synapse_parameters[param]),
                                                 locals(), globals())
                                        else:
                                            exec('syn_NMDA.' + param + '=' + str(synapse_parameters[param]))
                                    except:
                                        pass
                                h.setpointer(pre(0.5)._ref_T_rel, 'C', syn_NMDA)
                                # synapse_dict.update(syn_dict)
                                syn_dict.update({"section": section[0]})
                                self.ex_NMDA.append(syn_dict)
                                self.PNT_synapses.append(syn_NMDA)
                                g_vec = h.Vector()
                                glu_vec = h.Vector()
                                g_vec.record(syn_NMDA._ref_g)
                                glu_vec.record(pre(0.5)._ref_T_rel)
                                self.g_vec.append(g_vec)
                                self.glu_vec.append(glu_vec)
                                if self.verbool:
                                    print('Adding synapse %s to %s at the index %d' % (name, sec, int(i)))
                                break
            else:
                for sect in h.allsec():#self.cell.allseclist:
                    if sect.name() == sec:
                        seg = sect(0.5)
                        syn_NMDA = h.NMDA_Mg_T(seg)
                        # pre = h.Section(name='pre[' + str(self.n_pre_bouton) + ']')
                        pre = self.pre[self.n_pre_bouton]
                        pre.L = 1
                        pre.diam = 1
                        # self.pre.append(pre)
                        self.n_pre_bouton = self.n_pre_bouton + 1
                        pre.insert('rel')
                        pre.dur_rel = 0.5
                        pre.amp_rel = 2
                        h.Erev_NMDA_Mg_T = 5
                        h.mg_NMDA_Mg_T = 1
                        for param in list(synapse_parameters.keys()):
                            try:
                                if sys.version >= "3.4":
                                    exec('syn_NMDA.' + param + '=' + str(synapse_parameters[param]),
                                         locals(), globals())
                                else:
                                    exec('syn_NMDA.' + param + '=' + str(synapse_parameters[param]))
                            except:
                                pass
                        h.setpointer(pre(0.5)._ref_T_rel, 'C', syn_NMDA)
                        # synapse_dict.update(syn_dict)
                        syn_dict.update({"section": section[0]})
                        self.PNT_synapses.append(syn_NMDA)
                        g_vec = h.Vector()
                        g_vec.record(syn_NMDA._ref_g)
                        self.g_vec.append(g_vec)
                        if self.verbool:
                            print('Adding synapse %s to %s' % (name, sec))


    def set_spiketrain_to_bouton(self,spike_train,pre):
        """
        set the pointer for the pre synaptic bouton
        :param spike_train: np.ndarray, a train of spike time in ms
        :param pre: nrn.Section
        :return:
        """
        dur = 1
        amp = 2
        vec_length = int(self.cell.tstop/self.cell.dt)+1
        vec = np.zeros(vec_length)
        for time in spike_train:
            if int(time/self.cell.dt)<=vec_length:
                if int((time+dur)/self.cell.dt)<=vec_length:
                    a = vec[int(time/self.cell.dt)+1:int((time+dur)/self.cell.dt)]
                    vec[int(time / self.cell.dt) + 1:int((time + dur) / self.cell.dt)] = np.repeat(amp, len(a))
                else:
                    a = vec[int(time / self.cell.dt) + 1:vec_length]
                    vec[int(time / self.cell.dt) + 1:vec_length] = np.repeat(amp, len(a))
        vec_point = h.Vector(vec)
        vec_point.play(pre(0.5)._ref_T_rel, self.cell.dt)


    def custom_cabel_LFP(self, beta_bar=1, g_j=0.3, c_EL=1, if_plot=False):
        """
        use the cabel theory: g_j*V_j + C_EL*dV_j/d_t = I_m to calculate the LFP, the value returned as a attribute in kwargs in the LFPy.electrode object

        Returns
        -------
        self.electrode.kwargs['cabel_LFP']: np.array
            the V_j arranged in row vectors

        """
        cell = self.cell
        electrode = self.electrode
        cabel_LFP = []
        for i in range(electrode.x.size):
            position = [electrode.x[i], electrode.y[i], electrode.z[i]]
            v_j = junctional_V_calc(cell, position, beta_bar=beta_bar, g_j=g_j, c_EL=c_EL, if_plot=False)
            cabel_LFP.append(v_j)

        cabel_LFP = np.asarray(cabel_LFP)
        LFP_dict = {
            'custom_cabelLFP': cabel_LFP,
        }

        if if_plot:
            time_range = [115, 125]
            plt.figure()
            for i in range(electrode.x.size):
                plt.subplot(4, 8, i + 1)
                plt.plot(cell.tvec, cabel_LFP[i, :])
                plt.xlim(time_range[0], time_range[1])

        return cabel_LFP
        # electrode.kwargs.update(LFP_dict)

