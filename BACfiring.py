# %%
"""
unit of NEURON (unless otherwise noticed):
    time: ms
    voltage: mV
    current: mA/cm^2 (all the ionic current recorded is density), nA
    concentration: mM
    capacitance: uf/cm^2
    length: um
    conducrance: S/cm^2, uS
    axis resistivity (Ra): ohm*cm
    resistance (SEClamp.rs): 10^6*ohm
    extracellular conductivity (self-defined): S/m

see the unit with command: h.units()
"""

# %% Import modules
import sys
wd = 'C:\\work\\Code\\neuron-l5pn-model' # working directory
sys.path.insert(1, wd)

from neuron import h
from neuron import gui2
import neuron
import numpy as np
import matplotlib.pyplot as plt
import glob
import os
from pprint import pprint
import LFPy
from scipy.stats import norm
from numpy.random import RandomState

import LFP
import visualization
from LFP import LFPclass
import scipy.io as sio

neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model")
neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model\\mod_test")
neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model\\mod_Gao2020")
neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model\\NMDA")

def currentinjection(sections, amp):
    Vm_all = []
    pathname = 'C:\\work\\BACsimulation\\timewindow\\'
    current_files = glob.glob(os.path.join(pathname, '*.mat'))
    for i in range(len(amp)):
        h('forall delete_section()')
        LFPclass = LFP.create_LFPclass(wd, verbool=False)  # change to verbool = False if no print out is wanted
        LFPclass.create_cell(tstop=300, dt=0.05)  # create a default cell
        cell = LFPclass.cell
        LFPclass.set_active_basal_dendrites()
        LFPclass.set_strong_basal_dendrite(sec_name='dend[5]')

        stim_param = {
            'sec': sections,
            'electrode_type': list(np.repeat('IClamp', len(sections))),
            'stim_delay': list(np.repeat(200, len(sections))),
            'stim_amp': amp[i],
            'stim_dur': [5, 5], #list(np.repeat(10, len(sections))),
        }
        LFPclass.create_int_stim(**stim_param)
        LFPclass.set_simulation(spike_times=[], trg_syns=[])
        Vm = visualization.plot_Vm_traces(cell, ['soma[0]', 'apic[36]', 'apic[38]'], time_range=[190, 300], if_plot=True)
        Vm_all.append(Vm[:,2000:-1])
    data = {}
    data["amp"] = amp
    data["delay"] = stim_param["stim_delay"]
    data["Vm"] = Vm_all
    data["dur"] = stim_param["stim_dur"]
    data["sections"] = stim_param["sec"]
    file_name = os.path.join(pathname, 'result' +str(len(current_files)+1) +'.mat')
    sio.savemat(file_name, data)

def currentinjection_duration(sections, amp, dur):
    Vm_all = []
    stim_dur = []
    stim_amp = []
    spike_time_all = []
    spike_freq_all = []
    spike_num = []
    peak_trunk = []
    pathname = 'C:\\work\\BACsimulation\\timewindow\\'
    current_files = glob.glob(os.path.join(pathname, '*.mat'))
    dt = 0.05
    for i in range(len(dur)):
        for ii in range(len(amp)):
            h('forall delete_section()')
            LFPclass = LFP.create_LFPclass(wd, verbool=False)  # change to verbool = False if no print out is wanted
            LFPclass.create_cell(tstop=300, dt=0.05)  # create a default cell
            cell = LFPclass.cell
            LFPclass.set_active_basal_dendrites()
            LFPclass.set_strong_basal_dendrite(sec_name='dend[5]')

            stim_param = {
                'sec': sections,
                'electrode_type': ['IClamp', 'IClamp'], #list(np.repeat('IClamp', len(sections))),
                'stim_delay': list(np.repeat(200, len(sections))),
                'stim_amp': [2, amp[ii]],
                'stim_dur': [dur[i], 5], #list(np.repeat(10, len(sections))),
            }
            LFPclass.create_int_stim(**stim_param)
            LFPclass.set_simulation(spike_times=[], trg_syns=[])
            Vm = visualization.plot_Vm_traces(cell, ['soma[0]', 'apic[36]', 'apic[38]'], time_range=[190, 300], if_plot=False)
            Vm_all.append(Vm[:,2000:-1])
            stim_dur.append(stim_param["stim_dur"])
            stim_amp.append(stim_param["stim_amp"])
            peak_trunk.append(np.max(Vm[1,2000:-1]))
            Vm_soma = Vm[0,2000:-1]
            spike_time_idx= []
            for iii in range(len(Vm_soma)-1):
                if Vm_soma[iii+1]>=-10 and Vm_soma[iii]<-10:
                    spike_time_idx.append(iii+1)
            if len(spike_time_idx)<=1:
                spike_freq_all.append(33.34)
            else:
                spike_freq_all.append(1/(np.mean(np.diff(spike_time_idx)*dt))*1e3) # Hz
            spike_time_all.append(np.asarray(spike_time_idx)*dt - 100)
            spike_num.append(len(spike_time_idx))
    data = {}
    data["amp"] = stim_amp
    data["delay"] = stim_param["stim_delay"]
    data["Vm"] = Vm_all
    data["dur"] = stim_dur
    data["sections"] = stim_param["sec"]
    data["spike_freq"] = spike_freq_all
    data["spike_time"] = spike_time_all
    data["peak_trunk"] = peak_trunk
    data["spike_num"] = spike_num
    file_name = os.path.join(pathname, 'result' +str(len(current_files)+1) +'.mat')
    sio.savemat(file_name, data)
    spike_freq_all = np.reshape(spike_freq_all, [len(dur), len(amp)])
    spike_num = np.reshape(spike_num, [len(dur), len(amp)])
    plt.figure()
    plt.plot(amp, np.transpose(spike_freq_all))
    plt.figure()
    plt.plot(amp, np.transpose(spike_num))

def currentinjection_duration_waveform(sections, amp, dur):
    Vm_all = []
    stim_dur = []
    stim_amp = []
    spike_time_all = []
    spike_freq_all = []
    spike_num = []
    peak_trunk = []
    pathname = 'C:\\work\\BACsimulation\\timewindow\\'
    current_files = glob.glob(os.path.join(pathname, '*.mat'))
    dt = 0.05
    for i in range(len(dur)):
        for ii in range(len(amp)):
            h('forall delete_section()')
            LFPclass = LFP.create_LFPclass(wd, verbool=False)  # change to verbool = False if no print out is wanted
            LFPclass.create_cell(tstop=300, dt=0.05)  # create a default cell
            cell = LFPclass.cell
            LFPclass.set_active_basal_dendrites()
            LFPclass.set_strong_basal_dendrite(sec_name='dend[5]')
            square = np.concatenate((np.repeat(0, int(200/dt)), np.repeat(2, int(dur[i]/dt)), np.repeat(0, int(300/dt)-int(200/dt)-int(dur[i]/dt)) ), axis = None)
            tau1 = 1
            tau2 = 4
            t_decay = np.arange(0,95,dt)
            decay_wave = (1-np.exp(-t_decay/tau1))*np.exp(-t_decay/tau2)
            decay = np.concatenate((np.repeat(0, int(205/dt)), decay_wave/max(decay_wave)*amp[ii]), axis = None)

            stim_param = {
                'sec': sections,
                'electrode_type': ['IClamp', 'IClamp'],
                'stim_wave':[square, decay],
            }
            LFPclass.create_int_stim_dynamic(**stim_param)
            LFPclass.set_simulation(spike_times=[], trg_syns=[])
            Vm = visualization.plot_Vm_traces(cell, ['soma[0]', 'apic[36]', 'apic[38]'], time_range=[190, 300], if_plot=False)
            Vm_all.append(Vm[:,2000:-1])
            stim_dur.append(dur[i])
            stim_amp.append(amp[ii])
            peak_trunk.append(np.max(Vm[1,2000:-1]))
            Vm_soma = Vm[0,2000:-1]
            spike_time_idx= []
            for iii in range(len(Vm_soma)-1):
                if Vm_soma[iii+1]>=-10 and Vm_soma[iii]<-10:
                    spike_time_idx.append(iii+1)
            if len(spike_time_idx)<=1:
                spike_freq_all.append(33.34)
            else:
                spike_freq_all.append(1/(np.mean(np.diff(spike_time_idx)*dt))*1e3) # Hz
            spike_time_all.append(np.asarray(spike_time_idx)*dt - 100)
            spike_num.append(len(spike_time_idx))
    data = {}
    data["amp"] = stim_amp
    data["Vm"] = Vm_all
    data["dur"] = stim_dur
    data["sections"] = stim_param["sec"]
    data["spike_freq"] = spike_freq_all
    data["spike_time"] = spike_time_all
    data["peak_trunk"] = peak_trunk
    data["spike_num"] = spike_num
    file_name = os.path.join(pathname, 'result' +str(len(current_files)+1) +'.mat')
    sio.savemat(file_name, data)
    spike_freq_all = np.reshape(spike_freq_all, [len(dur), len(amp)])
    spike_num = np.reshape(spike_num, [len(dur), len(amp)])
    plt.figure()
    plt.plot(amp, np.transpose(spike_freq_all))
    plt.figure()
    plt.plot(amp, np.transpose(spike_num))

def currentinjection_onesection(section, amp, dur):
    pathname = 'C:\\work\\BACsimulation\\timewindow\\'
    current_files = glob.glob(os.path.join(pathname, '*.mat'))
    Vm_all = []
    for i in range(len(amp)):
        h('forall delete_section()')
        LFPclass = LFP.create_LFPclass(wd, verbool=False)  # change to verbool = False if no print out is wanted
        LFPclass.create_cell(tstop=400, dt=0.05)  # create a default cell
        cell = LFPclass.cell
        LFPclass.set_active_basal_dendrites()
        LFPclass.set_strong_basal_dendrite(sec_name='dend[5]')

        stim_param = {
            'sec': section,
            'electrode_type': ['IClamp'],
            'stim_delay': [200],
            'stim_amp': [amp[i]],
            'stim_dur': [dur[i]], #list(np.repeat(10, len(sections))),
        }
        LFPclass.create_int_stim(**stim_param)
        LFPclass.set_simulation(spike_times=[], trg_syns=[])
        Vm = visualization.plot_Vm_traces(cell, ['soma[0]', 'apic[36]', 'apic[38]'], time_range=[190, 300], if_plot=True)
        Vm_all.append(Vm[:, 2000:-1])
    data = {}
    data["amp"] = amp
    data["delay"] = stim_param["stim_delay"]
    data["Vm"] = Vm_all
    data["dur"] = dur
    data["sections"] = stim_param["sec"]
    file_name = os.path.join(pathname, 'result_soma' +str(len(current_files)+1) +'.mat')
    sio.savemat(file_name, data)

def currentinjection_timewindow(sections, delay, amp = [1,1.3]):
    pathname = 'C:\\work\\BACsimulation\\timewindow\\'
    current_files = glob.glob(os.path.join(pathname, '*.mat'))
    Vm_all = []
    stim_delay = []
    for i in range(len(delay)):
        h('forall delete_section()')
        LFPclass = LFP.create_LFPclass(wd, verbool=False)  # change to verbool = False if no print out is wanted
        LFPclass.create_cell(tstop=400, dt=0.05)  # create a default cell
        cell = LFPclass.cell
        LFPclass.set_active_basal_dendrites()
        LFPclass.set_strong_basal_dendrite(sec_name='dend[5]')

        stim_param = {
            'sec': sections,
            'electrode_type': list(np.repeat('IClamp', len(sections))),
            'stim_delay': [200, 200 + delay[i]],
            'stim_amp': amp,
            'stim_dur': [5, 5], #list(np.repeat(10, len(sections))),
        }
        LFPclass.create_int_stim(**stim_param)
        LFPclass.set_simulation(spike_times=[], trg_syns=[])
        Vm = visualization.plot_Vm_traces(cell, ['soma[0]', 'apic[36]', 'apic[38]'], time_range=[100, 400], if_plot=True)
        Vm_all.append(Vm[:,2000:-1])
        stim_delay.append(stim_param["stim_delay"])
    data = {}
    data["amp"] = amp
    data["isi"] = delay
    data["delay"] = stim_delay
    data["sections_saved"] = ['soma[0]', 'apic[36]', 'apic[38]']
    data["Vm"] = Vm_all
    data["dur"] = stim_param["stim_dur"]
    data["sections"] = stim_param["sec"]
    file_name = os.path.join(pathname, 'result' +str(len(current_files)+1) +'.mat')
    sio.savemat(file_name, data)
#%%
# soma_amp = [1,1.5,2]
# apic_amp = [0.8,1, 1.3]
# x, y = np.meshgrid(soma_amp, apic_amp)
# N = len(soma_amp)*len(apic_amp)
# amp = np.transpose([np.reshape(x,N), np.reshape(y,N)])
amp = [
    [2, 1.3]
#    [2, 1],
 #   [2, 1.3]
]
currentinjection(['soma[0]','apic[36]'], amp)
#%%
amp = [2]
dur = [9]
currentinjection_onesection(['soma[0]'], amp, dur)
#%%
delay = [0,5,10,15,20]
currentinjection_timewindow(['soma[0]','apic[36]'], delay, amp = [2,1.3])
#%%
dur = [5,12,20]
amp = np.arange(0.1,0.75, 0.05)
currentinjection_duration(['soma[0]','apic[36]'], amp, dur)
#%%
dur = [9]
amp = np.arange(0.05, 1.25, 0.1)#np.arange(0.55)
currentinjection_duration_waveform(['soma[0]','apic[36]'], amp, dur)