"""
Created on Sat Sep 14 16:18:23 2019

@author: xiao208

single cell analysis to fit the ion channel parameters

"""
import allensdk.model.biophysical.utils as utils
import allensdk.model.biophysical.runner as runner

import numpy as np
import glob
import os
import sys
import statistics
import math
import json
import itertools

import LFPy
from neuron import h
from neuron import gui2
import neuron

import LFP
import visualization
from LFP import LFPclass

import matplotlib.pyplot as plt


class ParamCalc(object):
    # the functions in this class is currently combined to the ParamFit class, this class is
    # claimed but unused.
    """
    At a given site (defined by section name and relative seg_idx (starts from 1)), find out the:

    amplitude of first AP/bAP/Ca spike (mV)
    width (half width) of first AP/bAP/Ca spike (ms)
    AHP depth (only for soma) (mV)
    mean ISI (somatic) (ms)
    spike frequency (Hz)
    first event latency (ms)

    """
    def __init__(self, cell, sec_name = 'soma[0]', seg_idx = []):
        self.cell = cell
        self.sec_name = sec_name
        if seg_idx == []:
            self.seg_idx = self.cell.get_idx(self.sec_name)
        else:
            self.seg_idx = [self.cell.get_idx(self.sec_name)[0] + seg_idx - 1]
        self.vsec = np.mean(self.cell.vmem[self.seg_idx], axis = 0)

    # def spike_detection(self, threshold = -10):
    #     """
    #     detect the spikes based on somatic Vm, spike defined as when the Vm cross the threshold
    #     :return:
    #         time point (idx) of the local max (spike peak)
    #     """
    #
    #     somav = self.cell.somav
    #     idx_cross = [idx for idx in range(len(somav)-1) if (somav[idx]-threshold)*(somav[idx+1]-threshold) < 0]
    #     self.tidx_peak = []
    #     if len(idx_cross)%2:
    #         # the last spike didn't end, discard it
    #         idx_cross = idx_cross[:-1]
    #
    #     for i in range(len(idx_cross)/2):
    #         self.tidx_peak.append(idx_cross[2*i] + np.argmax(somav[idx_cross[2*i]:idx_cross[2*i+1]]))
    #
    # def calc_amplitude(self):
    #     """
    #     amplitude of the first spike, the peak is searched within +/- 1ms (100 time point) of the somatic peak
    #     :return:
    #         amplitude (mV) of the first spike
    #     """
    #     if not hasattr(self, tidx_peak):
    #         self.spike_detection()
    #
    #     win = int(1/self.cell.dt)
    #     self.amplitude  = np.max(self.vsec[self.tidx_peak[0]-win: self.tidx_peak[0]+win])
    #
    # def calc_half_width(self):
    #     """
    #     half width of the first spike, searched within +/- 1ms of the somatic peak (for Ca spike, search in the range of +/- 30ms)
    #     :return:
    #         half width of the first spike
    #     """
    #     if not hasattr(self, amplitude):
    #         self.calc_amplitude()
    #
    #     win = int(1/self.cell.dt)
    #     half_amp = self.amplitude/2
    #     somav = self.cell.somav
    #     t_cross = [idx for idx in range(self.tidx_peak[0]-win, self.tidx_peak[0]+win) if
    #                (somav[idx]-half_amp)*(somav[idx+1]-half_amp) < 0]
    #     if (len(t_cross) != 2):
    #         # the event is longer than 2ms, probably not AP but Ca spike
    #         win = int(30 / self.cell.dt)
    #         t_cross = [idx for idx in range(self.tidx_peak[0] - win, self.tidx_peak[0] + win) if
    #                    (somav[idx] - half_amp) * (somav[idx + 1] - half_amp) < 0]
    #         if (len(t_cross) != 2):
    #             print("check the Ca spike width")
    #             return
    #     self.half_width = self.cell.dt*(t_cross[1]-t_cross[0])
    #
    # def calc_AHP(self):
    #     """
    #     afterhyperpolarization (AHP) depth, search within 0 to +1ms after somatic peak
    #     :return:
    #         afterhyperpolarization (AHP) depth and width (time difference to spike peak, ms), return 0 if there's no afterhyperpolarization
    #     """
    #     if not hasattr(self, tidx_peak):
    #         self.spike_detection()
    #
    #     if 'soma' not in self.sec_name:
    #         print('AHP only calculate for somatic section')
    #
    #     win = int(1 / self.cell.dt)
    #     self.AHP_width = (np.argmin(self.cell.somav[self.tidx_peak[0]: self.tidx_peak[0]+win]) - self.tidx_peak[0])*self.cell.dt
    #     self.AHP_depth = self.cell.somav[int(self.tidx_peak[0] + self.AHP_width/self.cell.dt)]
    #
    #     if self.AHP_depth > -68:
    #         # TODO: a more precise detection of resting potential
    #         print('no AHP, try to vary stimulation duration')
    #         self.AHP_depth = 0
    #         self.AHP_width = 0
    #
    # def calc_ISI(self):
    #     """
    #         ISI between somatic spikes, only calculated for soma, disregard of the section
    #     :return:
    #         mean ISI
    #     """
    #     if not hasattr(self, tidx_peak):
    #         self.spike_detection()
    #
    #     isi = []
    #     for i in range(len(tidx_peak)-1):
    #         isi.append(self.cell.dt*(tidx_peak[i+1]-tidx_peak[i]))
    #
    #     self.mean_ISI = np.mean(isi)
    #
    # def calc_FR(self, stim_delay = 0, stim_dur = 0):
    #     """
    #     spike rate, (number of total spike - 1) / (time of last spike - time of first spike)
    #     :return:
    #     """
    #     if not hasattr(self, stim_delay):
    #         if stim_delay == 0:
    #             print("please specify stim_delay (stimulation start time)")
    #         else:
    #             self.stim_delay = stim_delay
    #
    #     if not hasattr(self, stim_dur):
    #         if stim_dur == 0:
    #             print("please specify stim_dur (stimulation duration)")
    #         else:
    #             self.stim_dur = stim_dur
    #
    #     if not hasattr(self, tidx_peak):
    #         self.spike_detection()
    #
    #     self.spike_rate = (len(tidx_peak)-1)/self.stim_dur
    #
    #
    # def first_event_latency(self, stim_delay = 0, stim_dur = 0):
    #     """
    #     delay of the first spike (peak) to the start of stimulation (in ms)
    #     """
    #     if not hasattr(self, stim_delay):
    #         if stim_delay == 0:
    #             print("please specify stim_delay (stimulation start time)")
    #         else:
    #             self.stim_delay = stim_delay
    #
    #     if not hasattr(self, stim_dur):
    #         if stim_dur == 0:
    #             print("please specify stim_dur (stimulation duration)")
    #         else:
    #             self.stim_dur = stim_dur
    #
    #     if not hasattr(self, tidx_peak):
    #         self.spike_detection()
    #
    #     self.first_event_latency = self.tidx*self.cell.dt - self.stim_delay

def create_ParamFit_class(wd, verbool = False):
    return ParamFit(wd, verbool)

class ParamFit(object):
    """
    fit the physiological parameters, the params will be looped in a coarse-to-fine manner (3 steps)

    """
    def __init__(self, wd, verbool = False):
        """
            param_fit.json: json file with all the parameters
                "stim_param": list of dict
                    "name": name of the stim ('somatic_stp_inj', 'dendritic_stp_inj', 'paired_stp_inj', 'somatic_syn',
                        'demdritic_syn', 'paired_syn')
                    "delay":
                    "amp": (no amp and dur for synaptic stim, for alphasynapse, gmax is also in amp)
                    "dur": (for alphasynapse, tau is also in dur)
                    "section": list of stimulated sections, soma before dendrite if
                    "syn_num": (no syn_num and syn_type for current injection)
                    "syn_type"

                "trg_param": list of dict
                    "section": name of the section ('soma', 'dend', 'axon', or 'apic')
                    "name": name of the target physiological parameter (eg. 'gNap_Et2bar')
                    "value": list, [start of the range, end of the range]
                    "mechanism": name of the mechanism

                "aim_param": list of dict
                    "section": name of the section
                    "feature": name of the aimed feature
                        rest:
                            "soma_rest"
                            "apic_rest"
                        soma:
                            "AP_amplitude"
                            "AP_half_width"
                            "AP_AHP"
                            "AP_ISI"
                            "AP_latency"
                            "bAP_amplitude"
                        paired:
                            "Ca_amplitude"
                            "Ca_half_width"
                            "AP_count"
                            "AP_ISI"
                            "AP_AHP"
                            "AP_amplitude"
                            "AP_half_width"
                    "stim": name of the stim ('somatic_stp_inj', 'dendritic_stp_inj', 'paired_stp_inj', 'somatic_syn',
                        'demdritic_syn', 'paired_syn')
                    "value": target value of the aimed feature
                    "std": value, to calculate the tolerance range

                "run_param": dict
                    "dur": duration of the entire simulation
                    "dt": dt of simulation
        """
        self.wd = wd
        self.verbool = verbool
        self.step_num = 4  # built-in value, number of steps in each coarse grid search
        with open(os.path.join(wd, 'param_fit_biophys4.json')) as file:
            data = json.load(file)

        self.stim_param = data["stim_param"]
        self.trg_param = data["trg_param"]
        self.aim_param = data["aim_param"]
        self.run_param = data["run_param"]
        h.load_file('stdrun.hoc')

    def create_master_cell(self, if_add_spine = False):
        self.cell_class = LFP.create_LFPclass(self.wd, verbool = self.verbool)
        self.cell_class.create_cell(if_add_spines = if_add_spine)
        self.cell = self.cell_class.cell
        self.cell_class.create_synapse()
        self.synapses = self.cell.synapses
        self.next_phy_param = []  # the physiological parameters that will be applied
        self.fitted_phy_param = [] # self.fitted_phy_param only updated when the final result comes,
        # otherwise refer to self.best_idx for the current best value in each loop
        self.phy_param_range = []
        self.performance = []  # total number of achieved aim
        self.mse = [] # normalized mean square error, used to decide the direction of optimization
        self.best_performance = 0
        self.best_mse = 1
        self.best_idx = 0
        self.loop_count = -1
        self.perm = list(itertools.permutations(range(self.step_num), len(self.trg_param)))
        for p in self.trg_param:
            self.fitted_phy_param.append(p)
            self.phy_param_range.append(list(np.linspace(p["value"][0], p["value"][1], self.step_num)))
            p["value"] = p["value"][0]  # initiate to the start value
            self.next_phy_param.append(p)
        self.group_stimulus()
        self.update_loop()


    def update_physiological_parameter(self):
        """
        loop the parameter in 1/3 step size
        :return:
        """
        self.rest_metric = []
        self.soma_stp_metric = []
        self.paired_stp_metric = []
        while self.best_performance < len(self.aim_param) or self.loop_count <= 5:
            if self.loop_idx >= len(self.perm):
                self.update_loop()
                flag = False
                if self.soma_stp_metric != []:
                    a = np.asarray(self.soma_stp_metric)
                    for i in range(len(self.soma_stp_group)):
                        if np.unique(a[:,i]).size == 1:
                            print("change parameter range, the metric %s is always %.2f"
                                  % (self.soma_stp_group[i]["feature"], np.unique(a[:,i])[0]))
                            flag = True
                            break
                if self.paired_stp_metric != []:
                    a = np.asarray(self.paired_stp_metric)
                    for i in range(len(self.paired_stp_group)):
                        if np.unique(a[:,i]).size == 1:
                            print("change parameter range, the metric %s is always %.2f"
                                  % (self.paired_stp_group[i]["feature"], np.unique(a[:, i])[0]))
                            flag = True
                            break
                if self.rest_metric != []:
                    a = np.asarray(self.rest_metric)
                    for i in range(len(self.rest_group)):
                        if np.unique(a[:,i]).size == 1:
                            print("change parameter range, the metric %s is always %.2f"
                                  % (self.rest_group[i]["feature"], np.unique(a[:, i])[0]))
                            flag = True
                            break
                self.soma_stp_metric = []
                self.rest_metric = []
                self.paired_stp_metric = []
                if flag:
                    break
            if self.best_performance == len(self.aim_param):
                print("the fitting is done!")
                for p, i in zip(self.trg_param, range(len(self.trg_param))):
                    p["value"] = self.phy_param_range[i][self.best_idx[i]]  # initiate to the start value
                    self.fitted_phy_param.append(p)
                break
            elif self.loop_count > 5:
                print("not converging")
                break
            idx = self.perm[self.loop_idx]
            if self.verbool:
                print("testing the No. %d combination in this loop" % self.loop_idx)
            for i in range(len(self.trg_param)):
                self.next_phy_param[i]["value"] = self.phy_param_range[i][idx[i]]
            self.set_physiological_parameter()
            self.run()
            self.loop_idx = self.loop_idx + 1



    def update_loop(self):
        """
        start a new loop after evaluating the result
        :return:
        """
        # TODO: use z score instead of binary to evaluate the performance
        if self.performance != []:
            idx = self.perm[np.argmin(self.mse)]
            for i in range(len(self.phy_param_range)):
                if idx[i] == 0:
                    self.phy_param_range[i] = list(
                        np.linspace(self.phy_param_range[i][0], self.phy_param_range[i][1], self.step_num))
                elif idx[i] == self.step_num - 1:
                    self.phy_param_range[i] = list(
                        np.linspace(self.phy_param_range[i][-2], self.phy_param_range[i][-1], self.step_num))
                else:
                    self.phy_param_range[i] = list(
                        np.linspace(self.phy_param_range[i][idx[i]-1], self.phy_param_range[i][idx[i]+1], self.step_num))
            self.best_performance = np.max(self.performance)
            self.best_mse = np.min(self.mse)
            self.best_idx = idx
            sorted_mse = np.sort(self.mse)
            if np.array_equal(np.asarray(self.mse), sorted_mse):
                if self.verboll:
                    print("Note: the mse is in ascending order, may need to change the range")
            if np.array_equal(np.asarray(self.mse), sorted_mse[::-1]):
                if self.verbool:
                    print("Note: the mse is in descending order, may need to change the range")
            if self.verbool:
                print("The loop No. %d yields a best mse of %.3f and %d targets are fitted, start loop No. %d"
                      % (self.loop_count, self.best_mse, self.best_performance, self.loop_count + 1))
        else:
            if self.verbool:
                print("start loop No. %d" % self.loop_count + 1)
        self.loop_idx = 0
        self.loop_count = self.loop_count + 1
        self.performance = []
        self.mse = []


    def set_physiological_parameter(self):
        """
        update the physiolocal parameters based on self.next_phy_param
        :return:
        """
        for phy_param in self.next_phy_param:
            section_array = phy_param["section"]
            mechanism = phy_param["mechanism"]
            param_name = phy_param["name"]
            param_value = float(phy_param["value"])
            for sec in self.cell.allseclist:
                if sec.name()[0:4] in section_array:
                    if mechanism != "":
                        if h.ismembrane(mechanism, sec=sec) != 1:
                            sec.insert(str(mechanism))
                        setattr(sec, param_name + "_" + mechanism, param_value)
                    else:
                        setattr(sec, param_name, param_value)

    def group_stimulus(self):
        """
        group aims which required different stimulus
        :return:
        """
        self.soma_stp_group = []
        self.paired_stp_group = []
        self.soma_syn_group = []
        self.paired_syn_group = []
        self.rest_group = []
        for aim_param in self.aim_param:
            if aim_param["stim"] == "somatic_stp_inj":
                self.soma_stp_group.append(aim_param)
            elif aim_param["stim"] == "paired_stp_inj":
                self.paired_stp_group.append(aim_param)
            elif aim_param["stim"] == "somatic_syn":
                self.soma_syn_group.append(aim_param)
            elif aim_param["stim"] == "paired_syn":
                self.paired_syn_group.append(aim_param)
            elif aim_param["stim"] == "rest":
                self.rest_group.append(aim_param)
            else:
                print("The stimulus is not in the category, please check")

    def set_somatic_stp_inj(self):
        if self.soma_stp_group != []:
            stim_param = [param for param in self.stim_param if param["name"] == 'somatic_stp_inj']
            stim_param = stim_param[0]
            self.stim_soma = h.IClamp(self.soma_seg)
            self.stim_soma.dur = stim_param["dur"]
            self.stim_soma.delay = stim_param["delay"]
            self.stim_soma.amp = stim_param["amp"]

    def sef_paired_stp_inj(self):
        """
        paired_stp_inj is actually somatic step current + apical Alphasynapse
        :return:
        """
        # TODO: update the paired loop, add both somatic input only and dendritic input only, decrease simulation time
        if self.paired_stp_group != []:
            stim_param = [param for param in self.stim_param if param["name"] == 'paired_stp_inj']
            stim_param = stim_param[0]
            self.stim_paired_soma = h.IClamp(self.soma_seg)
            self.stim_paired_soma.dur = stim_param["dur"][0]
            self.stim_paired_soma.delay = stim_param["delay"][0]
            self.stim_paired_soma.amp = stim_param["amp"][0]
            self.stim_paired_apic = []
            self.apic_seg = []
            for section, i in zip(stim_param["sections"][1:], range(len(stim_param["sections"])-1)):
                apic = [sec for sec in self.cell.allseclist if sec.name() == section]
                self.apic_seg.append(apic[0](0.5))
                asyn = h.AlphaSynapse(apic[0](0.5))
                asyn.gmax = stim_param["amp"][i]
                asyn.onset = stim_param["delay"][i]
                asyn.tau = stim_param["dur"][i]
                self.stim_paired_apic.append(asyn)

    def set_rest(self):
        if self.soma_stp_group != []:
            stim_param = [param for param in self.stim_param if param["name"] == 'rest']
            stim_param = stim_param[0]
            self.stim_soma = h.IClamp(self.soma_seg)
            self.stim_soma.dur = stim_param["dur"]
            self.stim_soma.delay = 0
            self.stim_soma.amp = 0

    def set_somatic_syn(self):
        if self.soma_syn_group != []:
            print('the synaptic stimulation is not supported yet')

    def set_paired_syn(self):
        if self.paired_syn_group != []:
            print('the synaptic stimulation is not supported yet')

    def run(self):
        """
        start the simulation and return the performance
        :return:
        """
        # TODO: improve the optimization
        aim_sec = []
        for aim in self.aim_param:
            if aim["section"] not in aim_sec:
                aim_sec.append(aim["section"])
        soma = [sec for sec in self.cell.allseclist if sec.name() == 'soma[0]']
        self.soma_seg = soma[0](0.5)
        somav = h.Vector()
        somav.record(self.soma_seg._ref_v)
        apicv = []
        for sec in self.cell.allseclist:
            if (sec.name() in aim_sec and sec.name() != 'soma[0]'):
                a = h.Vector()
                a.record(sec(0.5)._ref_v)
                apicv.append(a)

        tvec = h.Vector()
        tvec.record(h._ref_t)
        mse = []

        if self.rest_group != []:
            self.set_rest()
            h.dt = self.run_param["dt"]
            h.tstop = 100

            h.run()
            metric = []
            for aim in self.rest_group:
                if aim["feature"] == "soma_rest":
                    value = self.calc_rest_potential(np.asarray(somav))
                elif aim["feature"] == "apic_rest":
                    idx = aim_sec.index(aim["section"])
                    value = self.calc_rest_potential(np.array(apicv[idx]))

                mse.append(((value - aim["value"])/aim["value"])**2)
                if np.abs(value - aim["value"]) <= aim["std"]:
                    performance = performance + 1

                if self.verbool:
                    print("under resting stimulus the %s metric is %.3f, mse is %.3f, performance considered as %d"
                          % (aim["feature"], value, ((value - aim["value"])/aim["value"])**2, (np.abs(value - aim["value"]) <= aim["std"])))

                metric.append(value)
            self.rest_metric.append(metric)

        if self.soma_stp_group != []:
            self.set_somatic_stp_inj()
            h.dt = self.run_param["dt"]
            h.tstop = self.run_param["dur"]

            h.run()

            self.somav = np.asarray(somav)
            self.spike_detection(threshold = -10)
            performance = 0
            metric = []
            for aim in self.soma_stp_group:
                if aim["feature"] == "AP_amplitude":
                    value = self.calc_amplitude(self.somav)
                elif aim["feature"] == "AP_half_width":
                    value = self.calc_half_width(self.somav)
                elif aim["feature"] == "AP_AHP":
                    value = self.calc_AHP()
                elif aim["feature"] == "AP_count":
                    value = self.calc_spike_count()
                elif aim["feature"] == "AP_latency":
                    value = self.calc_first_event_latency(stim_delay = self.stim_soma.delay, stim_dur = self.stim_soma.dur)
                elif aim["feature"] == "bAP_amplitude":
                    idx = aim_sec.index(aim["section"])
                    value = self.calc_amplitude(np.array(apicv[idx]))

                mse.append(((value - aim["value"]) / aim["value"]) ** 2)
                if np.abs(value - aim["value"]) <= aim["std"]:
                    performance = performance + 1

                if self.verbool:
                    print("under somatic_stp_inj stimulus the %s metric is %.3f, mse is %.3f,  performance considered as %d"
                          % (aim["feature"], value, ((value - aim["value"])/aim["value"])**2, (np.abs(value - aim["value"]) <= aim["std"])))
                metric.append(value)
            self.soma_stp_metric.append(metric)

        if self.paired_stp_group != []:
            self.set_paired_stp_inj()
            h.dt = self.run_param["dt"]
            h.tstop = 200
            h.run()

            self.somav = np.asarray(somav)
            self.spike_detection(threshold=-10)
            performance = 0
            metric = []
            for aim in self.paired_stp_group:
                if aim["feature"] == "rest":
                    value = self.calc_rest_potential()
                if aim["feature"] == "AP_amplitude":
                    value = self.calc_amplitude(self.somav)
                elif aim["feature"] == "AP_half_width":
                    value = self.calc_half_width(self.somav)
                elif aim["feature"] == "AP_AHP":
                    value = self.calc_AHP()
                elif aim["feature"] == "AP_ISI":
                    value = self.calc_ISI()
                elif aim["feature"] == "Ca_amplitude":
                    value = self.calc_amplitude(np.array(apicv[0]))
                elif aim["feature"] == "Ca_half_width":
                    value = self.calc_half_width(np.array(apicv[0]))

                mse.append(((value - aim["value"]) / aim["value"]) ** 2)
                if np.abs(value - aim["value"]) <= aim["std"]:
                    performance = performance + 1

                if self.verbool:
                    print("under paired_stp_inj stimulus the %s metric is %.3f, mse is %.3f, performance considered as %d"
                          % (aim["feature"], value, ((value - aim["value"])/aim["value"])**2, (np.abs(value - aim["value"]) <= aim["std"])))
                metric.append(value)
            self.paired_stp_metric.append(metric)

        self.mse.append(np.mean(np.asarray(mse)))
        self.performance.append(performance)
        print("For trial No %d in loop %d, the overall mse is %.3f, the performance is %d"
              % (self.loop_idx, self.loop_count, np.mean(np.asarray(mse)), performance))

    def spike_detection(self, threshold = -10):
        """
        detect the spikes based on somatic Vm, spike defined as when the Vm cross the threshold
        :return:
            time point (idx) of the local max (spike peak)
        """

        somav = self.somav
        idx_cross = [idx for idx in range(len(somav)-1) if (somav[idx]-threshold)*(somav[idx+1]-threshold) < 0]
        self.tidx_peak = []
        if len(idx_cross)%2:
            # the last spike didn't end, discard it
            idx_cross = idx_cross[:-1]

        for i in range(int(len(idx_cross)/2)):
            self.tidx_peak.append(idx_cross[2*i] + np.argmax(somav[idx_cross[2*i]:idx_cross[2*i+1]]))

    def calc_rest_potential(self, v):
        return np.mean(v[-10:])

    def calc_amplitude(self, v):
        """
        amplitude of the first spike, the peak is searched within +/- 1ms (100 time point) of the somatic peak
        :return:
            amplitude (mV) of the first spike
        """
        if not hasattr(self, 'tidx_peak'):
            self.spike_detection()

        if self.tidx_peak == []:
            print('no AP')
            return -70

        win = int(1/self.cell.dt)
        amplitude = np.max(v[self.tidx_peak[0]-win: self.tidx_peak[0]+win])
        return amplitude

    def calc_half_width(self, v):
        """
        half width of the first spike, searched within +/- 1ms of the somatic peak (for Ca spike, search in the range of +/- 30ms)
        :return:
            half width of the first spike
        """
        if not hasattr(self, 'amplitude'):
            self.calc_amplitude()

        if self.tidx_peak == []:
            print('no AP')
            return 0

        win = int(1/self.run_param["dt"])
        half_amp = self.amplitude/2
        somav = self.somav
        t_cross = [idx for idx in range(self.tidx_peak[0]-win, self.tidx_peak[0]+win) if
                   (somav[idx]-half_amp)*(somav[idx+1]-half_amp) < 0]
        if (len(t_cross) != 2):
            # the event is longer than 2ms, probably not AP but Ca spike
            win = int(30 / self.run_param["dt"])
            t_cross = [idx for idx in range(self.tidx_peak[0] - win, self.tidx_peak[0] + win) if
                       (somav[idx] - half_amp) * (somav[idx + 1] - half_amp) < 0]
            if (len(t_cross) != 2):
                print("check the Ca spike width")
                return
        half_width = self.run_param["dt"]*(t_cross[1]-t_cross[0])
        return half_width


    def calc_AHP(self):
        """
        afterhyperpolarization (AHP) depth, search within 0 to +1ms after somatic peak
        :return:
            afterhyperpolarization (AHP) depth and width (time difference to spike peak, ms), return 0 if there's no afterhyperpolarization
        """
        if not hasattr(self, 'tidx_peak'):
            self.spike_detection()

        if self.tidx_peak == []:
            print('no AP')
            return -70

        win = int(1 / self.run_param["dt"])
        AHP_width = (np.argmin(self.somav[self.tidx_peak[0]: self.tidx_peak[0]+win]) - self.tidx_peak[0])*self.run_param["dt"]
        AHP_depth = self.somav[int(self.tidx_peak[0] + self.AHP_width/self.run_param["dt"])]

        if AHP_depth > -68:
            # TODO: a more precise detection of resting potential
            print('no AHP, try to vary stimulation duration')
            AHP_depth = 0
            AHP_width = 0

        return AHP_depth


    def calc_ISI(self):
        """
            ISI between somatic spikes, only calculated for soma, disregard of the section
        :return:
            mean ISI
        """
        if not hasattr(self, 'tidx_peak'):
            self.spike_detection()

        if self.tidx_peak == []:
            print('no AP')
            return 1000

        isi = []
        for i in range(len(self.tidx_peak)-1):
            isi.append(self.run_param["dt"]*(self.tidx_peak[i+1]-self.tidx_peak[i]))

        mean_ISI = np.mean(isi)
        return mean_ISI

    def calc_spike_count(self):
        """
        spike count
        :return:
        """
        if not hasattr(self, 'tidx_peak'):
            self.spike_detection()

        if self.tidx_peak == []:
            print('no AP')
            return 0

        spike_count = len(self.tidx_peak)
        return spike_count


    def calc_first_event_latency(self, stim_delay = 0, stim_dur = 0):
        """
        delay of the first spike (peak) to the start of stimulation (in ms)
        """
        if stim_delay == 0:
            print("please specify stim_delay (stimulation start time)")

        if stim_dur == 0:
            print("please specify stim_dur (stimulation duration)")

        if not hasattr(self, 'tidx_peak'):
            self.spike_detection()

        if self.tidx_peak == []:
            print('no AP')
            return 1000

        first_event_latency = self.tidx_peak*self.run_param["dt"] - stim_delay
        return first_event_latency


if __name__ == "__main__":
    wd = 'C:\\work\\Code\\neuron-l5pn-model'
    # neuron.load_mechanisms("C:\\work\\Code")
    sys.path.insert(1, wd)
    param_fit_class = create_ParamFit_class(wd, verbool = True)
    param_fit_class.create_master_cell(if_add_spine = False)
    param_fit_class.update_physiological_parameter()









