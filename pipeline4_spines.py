# %%
'''
This is a pipeline for implementing the L5 PN model (Hay et al 2011)

Before running the pipeline, please set up the environment according to readme.txt, activate the
environment and cd to the directory in cmd
'''

# %%
"""
unit of NEURON (unless otherwise noticed):
    time: ms
    voltage: mV
    current: mA/cm^2 (all the ionic current recorded is density), nA
    concentration: mM
    capacitance: uf/cm^2
    length: um
    conducrance: S/cm^2, uS
    axis resistivity (Ra): ohm*cm
    resistance (SEClamp.rs): 10^6*ohm
    extracellular conductivity (self-defined): S/m 

see the unit with command: h.units()
"""

# %% Import modules
import sys
wd = 'C:\\work\\Code\\neuron-l5pn-model' # working directory
sys.path.insert(1, wd)

from neuron import h
from neuron import gui2
import neuron
import numpy as np
import matplotlib.pyplot as plt
import glob
import os
from pprint import pprint
import LFPy

import LFP
import visualization
from LFP import LFPclass

# %% load mechanisms
# This only need to be run once unless any changes are made in nrnmech.dll
neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model")
# Note: before creating synapses, load the NMDA_Mg_T mechainsm in Branco et al 2010 paper
neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model\\Branco_2010")
# %%
'''
Before running the following code, please change the directory in manifest.json to your working directory

Important! Please change the file in manifest.json from biophys4.json to biophys4-spine.json 
'''
# %% Create the cell and insert synapse

LFPclass = LFP.create_LFPclass(wd, verbool=True)  # change to verbool = False if no print out is wanted
LFPclass.create_cell(if_add_spines = True)  # important! Set if_add_spine to True so that spines will be created
# Note: to specify the number, location, clustering etc. of spines, see biophys file and the LFPclass.insert_spines
# function
cell = LFPclass.cell
LFPclass.create_synapse()  # insert synapse onto the cell
# Note: One AMPA synapse will be inserted to each spine head as default, no need to specify
synapses = cell.synapses

# %% create electrodes
# Create 10 * 10 2d electrode grid (200um * 200um) that centered at the center of a section
LFPclass.create_electrode_section(sigma = 0.26, sec = 'soma[0]', r = 200)
electrode = LFPclass.electrode

# %% set presynaptic spikes and start simulation
# Note: for each simulation, a new cell need to be created
LFPclass.set_simulation(spike_times=[np.asarray([50])], trg_syns=['shead'])
# LFPclass.set_simulation(spike_times=[np.asarray([50]), np.asarray([55]), np.asarray([60]),
#     np.asarray([65]), np.asarray([70]), np.asarray([75]),
#     np.asarray([80]), np.asarray([85]), np.asarray([90]),
#     np.asarray([95])],
#                         trg_syns=['shead[0]', 'shead[1]', 'shead[2]',
#                                   'shead[3]', 'shead[4]', 'shead[5]',
#                                   'shead[6]', 'shead[7]', 'shead[8]',
#                                   'shead[9]'])
# %% Visualze Vm and morphology
Vm = visualization.plot_Vm_traces(cell, ['soma[0]', 'dend[0]', 'sneck[0]', 'shead[0]'], time_range=[40, 100],
                                  if_plot=1)
Cm = visualization.plot_Cai_traces(cell, ['soma[0]', 'dend[0]'], time_range=[40, 100],
                                  if_plot=1)
visualization.plot_morphology(LFPclass, if_show_synapses=1, if_show_electrode=1,
                              sections=['soma[0]', 'dend[0]', 'sneck[0]', 'shead[0]'])
# this will show the location of synapses and electrode on the morphology plot

# %% read out the LFP traces
lfp = electrode.LFP  # each row correspondes to LFP recorded from one electrode
# visualize the electrode grid and LFP traces
visualization.plot_electrode_LFP(LFPclass, time_range = [45, 75], if_plot_morphology = 1)

# %% Estimate CSD

csd = LFP.calc_csd(electrode)
# Note: the unit of CSD is A/(cm)^2
visualization.plot_csd(LFPclass, csd, col = 5, interp_ratio=10, time_range=[50, 100])
