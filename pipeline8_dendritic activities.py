# %%
'''
This is a pipeline for implementing the L5 PN model (Hay et al 2011)

Before running the pipeline, please set up the environment according to readme.txt, activate the
environment and cd to the directory in cmd
'''

# %%
"""
unit of NEURON (unless otherwise noticed):
    time: ms
    voltage: mV
    current: mA/cm^2 (all the ionic current recorded is density), nA
    concentration: mM
    capacitance: uf/cm^2
    length: um
    conducrance: S/cm^2, uS
    axis resistivity (Ra): ohm*cm
    resistance (SEClamp.rs): 10^6*ohm
    extracellular conductivity (self-defined): S/m 

see the unit with command: h.units()
"""

# %% Import modules
import sys
wd = 'C:\\work\\Code\\neuron-l5pn-model' # working directory
sys.path.insert(1, wd)

from neuron import h
from neuron import gui2
import neuron
import numpy as np
import matplotlib.pyplot as plt
import glob
import os
from pprint import pprint
import LFPy

import LFP
import visualization
from LFP import LFPclass

# %% load mechanisms
# This only need to be run once unless any changes are made in nrnmech.dll
neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model")
# Note: before creating synapses, load the NMDA_Mg_T mechainsm in Branco et al 2010 paper
neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model\\Branco_2010")
# %%
'''
Before running the following code, please change the directory in manifest.json to your working directory

Important! Please change the file in manifest.json from biophys4.json to biophys4-spine.json 
'''
# %% Create the cell and insert synapse

LFPclass = LFP.create_LFPclass(wd, verbool=True)  # change to verbool = False if no print out is wanted
LFPclass.create_cell(if_add_spines = True)
cell = LFPclass.cell
LFPclass.create_synapse()  # insert synapse onto the cell
# Note: One AMPA and one NMDA synapse will be inserted on each spine as default
synapses = cell.synapses

# %% create electrodes
# Create 10 * 10 2d electrode grid (200um * 200um) that centered at the center of a section
LFPclass.create_electrode_section(sigma = 0.26, sec = 'soma[0]', r = 200)
electrode = LFPclass.electrode

# %% NMDA spikes, evoked by simultaneous synaptic inputs onto 10 spines on a distal apical (apic[67] as example)
# Note: for each simulation, a new cell need to be created
LFPclass.set_simulation(spike_times=[np.asarray([100])], trg_syns=['shead'])

# %% Ca spikes, evoked as BAC (paired current injection into soma and proximal apical dendrites)
# Note: for each simulation, a new cell need to be created
LFPclass.set_simulation(spike_times = [np.asarray([100]), np.asarray([100])], trg_syns = ['apic[61]', 'soma[0]'])

# %% dendritic Na spikes, evoked by synaptic input onto proximal apical dendrites (with AMPA receptor only)
# Note: for each simulation, a new cell need to be created

LFPclass.set_simulation(spike_times = [np.asarray([100])], trg_syns = ['apic[5]'])
# %% Visualze Vm, intracellular Ca concentration and morphology
Vm = visualization.plot_Vm_traces(cell, ['soma[0]', 'apic[67]', 'apic[61]', 'shead[0]'], time_range=[90, 150],
                                  if_plot=1)
Cm = visualization.plot_Cai_traces(cell, ['soma[0]', 'apic[67]'], time_range=[90, 150],
                                  if_plot=1)
visualization.plot_morphology(LFPclass, if_show_synapses=1, if_show_electrode=1,
                              sections=['soma[0]', 'apic[67]', 'apic[61]', 'shead[0]'])
# this will show the location of synapses and electrode on the morphology plot

