#%%
"""
This is an example of a fully connected cell model, woth 8000 excitatory synapses and 2000 inhibitory synapses randomly
distributed on the dendrites (both basal and apical). To avoid the abnormally high excitability, active Na and K channels
are removed from the basal dendrites
"""
#%%
import sys
wd = 'C:\\work\\Code\\neuron-l5pn-model'  # working directory
# wd = 'E:\\Code\\neuron-l5pn-model'
sys.path.insert(1, wd)

from neuron import h
from neuron import gui2
import neuron
import numpy as np
import matplotlib.pyplot as plt
# import NEURON_function
import glob
import os
from pprint import pprint
import LFPy
import scipy.io as sio

import LFP
import visualization
from LFP import LFPclass
#%%
neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model")
# Note: before creating synapses, load the NMDA_Mg_T mechainsm in Branco et al 2010 paper
neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model\\NMDA")
neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model\\mod_Gao2020")
neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model\\mod_test")
#%%
h('forall delete_section()')
# Note: before running the mdoel, make sure to change the biophys file in manifest to biophys4_stochastic_full_cluster1.json
LFPclass = LFP.create_LFPclass(wd, verbool = False)
LFPclass.create_cell(if_add_spines = True, tstop = 1000, dt = 0.05)
# Note: the model is computational heavy, don't go crazy with simulation duration
cell = LFPclass.cell
LFPclass.set_active_basal_dendrites()
# inactivate all fast Na channels to block action potential
LFP.remove_channel(cell, 'NaTa_t')
LFP.remove_channel(cell, 'NaTs2_t')
LFP.remove_channel(cell, 'NaTa_t_2F')
LFP.remove_channel(cell, 'NaTs2_t_2F')
# stim_param = {
#    'sec': ['soma[0]'],
#    'electrode_type': ['IClamp'],
#    'stim_delay': [50],
#    'stim_amp': [1],
#    'stim_dur': [200],
# }
#
# LFPclass.create_int_stim(**stim_param)
#%
LFPclass.create_synapse()
synapses = cell.synapses
# LFPclass.create_electrode_array(sigma=0.3, x_off=0, y_off=0, z_off=0)
# electrode = LFPclass.electrode

# visualization.plot_morphology(LFPclass, if_show_spine = 1)
#%  set random presynaptic spike time and run simulation
LFPclass.set_full_connect_simulation(e_freq = 3, i_freq = 5, freq_nonclust = 0, freq_clust = 4)
# LFPclass.set_full_connect_simulation_w_inputs()
cell.simulate(**LFPclass.simulationParameters)
# h('forall delete_section()')


# Vm = visualization.plot_Vm_traces(cell, ['soma[0]'],
#                                   time_range = [0, 300], if_plot = 1)
# save_d = 'C:\\work\\high codc state simulation'
# sio.savemat(save_d + '\\full.mat', {'t': cell.tvec, 'Vm': cell.vmem})
# Vm = visualization.plot_Vm_traces(cell, ['soma[0]', 'apic[36]', 'apic[37]', 'apic[38]'],
#                                   time_range = [0, 500], if_plot = 1)
# csd = LFP.calc_csd(electrode)
# visualization.plot_csd(LFPclass, csd, interp_ratio = 10, time_range = [0, 100])
#%% view calcium signal
Vm = visualization.plot_Vm_traces(cell, ['soma[0]', 'apic[36]', 'apic[37]', 'apic[38]'],
                                  time_range = [0, 1000], if_plot = 1)
# visualization.plot_Cai_traces(cell, ['apic[14]', 'apic[20]', 'apic[26]', 'apic[36]'],
#                              time_range = [0, 500], if_plot = 1)
# visualization.plot_morphology(LFPclass, if_show_electrode = 0, if_show_synapses = 0,
#                               sections = ['apic[14]', 'apic[20]', 'apic[26]', 'apic[36]'])
# visualization.Vm_dynamic(cell, wd, time_range = [150, 400], if_save=True)
#%% synaptic current & presynaptic spike time
syn = {}
syns = []
for synapse, i in zip(synapses, range(len(synapses))):
    syn = {
        'section': synapse.kwargs["section"],
        'name': synapse.kwargs["name"],
        # 'i': synapse.i,
        # "spike_time": synapse.spike_time,
    }
    try:
        syn.update({
            "parent":synapse.kwargs["parent"],
            "if_clustered":synapse.kwargs["if_clustered"],
        })
    except KeyError:
        pass
    syns.append(syn)

#%%  save the simulation result
data = {}
# data["has_spines"] = LFPclass.has_spines
data["Vm"] = cell.vmem
data['t'] = cell.tvec
# data["LFP"] = electrode.LFP
# data["csd"] = csd
# data["cai"] = cell.rec_variables["cai"]
# data["syns"] = syns
file_name = os.path.join(wd, 'result_noNa.mat')
sio.savemat(file_name, data)
#%% view dynamics
visualization.Vm_dynamic(cell, wd, time_range = [50, 150], if_save=True)