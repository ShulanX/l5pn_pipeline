# %%
'''
This is a pipeline for implementing the L5 PN model (Hay et al 2011)

Before running the pipeline, please set up the environment according to readme.txt, activate the
environment and cd to the directory in cmd
'''

# %%
"""
unit of NEURON (unless otherwise noticed):
    time: ms
    voltage: mV
    current: mA/cm^2 (all the ionic current recorded is density), nA
    concentration: mM
    capacitance: uf/cm^2
    length: um
    conducrance: S/cm^2, uS
    axis resistivity (Ra): ohm*cm
    resistance (SEClamp.rs): 10^6*ohm
    extracellular conductivity (self-defined): S/m 

see the unit with command: h.units()
"""

# %% Import modules
import sys
wd = 'C:\\work\\Code\\neuron-l5pn-model' # working directory
sys.path.insert(1, wd)

from neuron import h
# from neuron import gui2
import neuron
import numpy as np
import matplotlib.pyplot as plt
import glob
import os
from pprint import pprint
import LFPy
import scipy.io as sio

import LFP
import visualization
from LFP import LFPclass
#%%
def NMDAspike(LFPclass, t, trg_branch, num_spine):
    """
    initiate an NMDA spikes on the cell by simultanouse input onto the target synapses
    :param t: 2D list (ms), time when simultaneouns input will arrive to evoke NMDA spike
    :param LFPclass: LFPclass object
    :param trg_branch: list of str, names of the target branch
    :param num_spine: 2D list, number of spines thats inserted onto each branch
    :return:
    """
    synapse_dict = []
    for branch, num in zip(trg_branch, num_spine):
        synapse_dict.append({
            "section": [branch],
            "num": num,
            "name": "AMPA"
        })
        synapse_dict.append({
            "section": [branch],
            "num": num,
            "name": "NMDA_Mg"
        })
    spike_t = []
    for time, num, branch in zip(t, num_spine, trg_branch):
        if len(time) == 1:
            spike_t.append(time)
        elif len(time) == num:
            spike_t.append(time*2)  # to address for both NMDA and AMPA
        else:
            raise ValueError('The number of presynaptic spike time and synapses doesn\'t match on branch ' + branch)
    LFPclass.create_synapse(if_from_file = False, syn_dict = synapse_dict)
    LFPclass.set_simulation(spike_times = spike_t, trg_syns = trg_branch)
# %% load mechanisms
# This only need to be run once unless any changes are made in nrnmech.dll

neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model")
# Note: before creating synapses, load the NMDA_Mg_T mechainsm in Branco et al 2010 paper
neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model\\NMDA")
neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model\\mod_Gao2020")
neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model\\mod_test")
# %%
'''
Before running the following code, please change the directory in manifest.json to your working directory

Important! Please change the file in manifest.json from biophys4.json to biophys4-NMDAintegration.json 
'''
# %% Create the cell

LFPclass = LFP.create_LFPclass(wd, verbool=True)  # change to verbool = False if no print out is wanted
LFPclass.create_cell(if_add_spines = False, tstop = 250)
LFPclass.set_active_basal_dendrites()
LFPclass.set_strong_basal_dendrite(sec_name = 'dend[5]')
#%% single NMDA spike
NMDAspike(LFPclass,t =  [np.asarray([50])], trg_branch = ['dend[5]'], num_spine = [20])
cell = LFPclass.cell
# visualization.plot_morphology(LFPclass, sections = ['soma[0]', 'dend[0]', 'dend[5]'])
Vm = visualization.plot_Vm_traces(cell, ['soma[0]', 'dend[0]', 'dend[5]'], time_range = [0, 200], if_plot = True)
#%% multiple NMDA spike
NMDAspike(LFPclass,t =  [np.asarray([50]), np.asarray([53])], trg_branch = ['dend[61]', 'dend[59]'], num_spine = [7, 7])

#%% increment number of synapses
n_tot = 10
save_d = 'X:\\data\\shulan\\simulation\\no spine'

def increment_synapses(i):
    LFPclass = LFP.create_LFPclass(wd, verbool=True)  # change to verbool = False if no print out is wanted
    LFPclass.create_cell(if_add_spines=False, tstop=250)
    NMDAspike(LFPclass, t=[np.asarray([50])], trg_branch=['dend[61]'], num_spine=[i])
    plot_branch = ['soma[0]', 'dend[61]']
    Vm = visualization.plot_Vm_traces(LFPclass.cell, plot_branch, time_range=[40, 250],
                                      if_plot=0)
    sio.savemat(save_d + '\\branch_61_AMPA_%dspine.mat' % i, {'t': LFPclass.cell.tvec, 'Vm': Vm, 'branch': plot_branch})
    for a in h.Vector:
        a.resize(0)
        a.play_remove()

for i in np.arange(1, n_tot+1):
    increment_synapses(i)

# %% NMDA spikes, evoked by simultaneous synaptic inputs onto 10 spines on a distal basal (dend[61])
# Note: for each simulation, a new cell need to be created

# LFPclass.set_simulation(spike_times=[np.asarray([50.0])], trg_syns=['dend[61]'])
# a = list(np.arange(50, 150, 10))  # isi = 10
# spiketime = a*2
#
# LFPclass.set_simulation(spike_times=[np.asarray(spiketime)], trg_syns=['dend[61]'])

#%%
# plt.figure()
# plt.plot(LFPclass.cell.tvec, LFPclass.synapses[0].i)
# plt.figure()
# plt.plot(LFPclass.cell.tvec, LFPclass.g_vec[0])
#%%
# plot_branch = ['soma[0]', 'dend[61]', 'dned[59]']
# Vm = visualization.plot_Vm_traces(LFPclass.cell, plot_branch, time_range=[40, 250],
#                                   if_plot=1)
# Cm = visualization.plot_Cai_traces(LFPclass.cell, plot_branch, time_range=[40, 200],
#                                   if_plot=1)
# # visualization.plot_morphology(LFPclass, if_show_synapses=1, if_show_electrode=0,
# #                                sections=['soma[0]', 'dend[61]', 'dend[59]'])
# #%%
# save_d = 'X:\\data\\shulan\\simulation\\no spine'
# sio.savemat(save_d + '\\branch_61_7spine.mat',  {'t':LFPclass.cell.tvec, 'Vm':Vm, 'branch': plot_branch})

#%%


