#%%
"""
This pipeline is an example of how to fit biophysical parameters (ion channel conductance, etc)

The fit setting can be specified in a .json file (see param_fit_biophys4.json as an example). The supported
stimulation and evaluation metrics are listed in documentation of single_cell.py

Apart from this pipeline, the single_cell.py file can be run (either from python IDE or cmd window)
to perform the same function

Note:
    The optimization is performed in a binary search manner (step size and maximum number of loops can
    be changed in the single_cell.py file). Therefore, if multiple parameters are going to be fitted,
    the complexity of the optimization will easily go crazy, which will take a long time and a lot of
    computational power.
"""
#%%
import sys
wd = 'C:\\work\\Code\\neuron-l5pn-model' # working directory
sys.path.insert(1, wd)

import single_cell
from single_cell import ParamFit

# neuron.load_mechanisms("C:\\work\\Code") # load the mechanisms if they are not already loaded
#%%
PFclass = single_cell.create_ParamFit_class(wd, verbool = True)
PFclass.create_master_cell(if_add_spine = False)
PFclass.update_physiological_parameter()

#%%
# read out the optimal parameter
params = PFclass.fitted_phy_param
if params == PFclass.trg_param:  # will happens if not all aims are met
    params = []
    for p, i in zip(PFclass.trg_param, range(len(PFclass.trg_param))):
        p["value"] = PFclass.phy_param_range[i][PFclass.best_idx[i]]  # read out the temporal best value
        params.append(p)
