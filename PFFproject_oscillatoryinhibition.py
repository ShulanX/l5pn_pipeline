# %%
"""
unit of NEURON (unless otherwise noticed):
    time: ms
    voltage: mV
    current: mA/cm^2 (all the ionic current recorded is density), nA
    concentration: mM
    capacitance: uf/cm^2
    length: um
    conducrance: S/cm^2, uS
    axis resistivity (Ra): ohm*cm
    resistance (SEClamp.rs): 10^6*ohm
    extracellular conductivity (self-defined): S/m

see the unit with command: h.units()
"""

#  Import modules
import sys
wd = 'C:\\work\\Code\\neuron-l5pn-model' # working directory
sys.path.insert(1, wd)

from neuron import h
from neuron import gui2
import neuron
import numpy as np
import matplotlib.pyplot as plt
import glob
import os
from pprint import pprint
import LFPy

import LFP
import visualization
from LFP import LFPclass
import scipy.io as sio
#  load mechanisms
# This only need to be run once unless any changes are made in nrnmech.dll

neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model")
neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model\\mod_test")
neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model\\mod_Gao2020")
neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model\\NMDA")
#
'''
Before running the following code, please change the directory in manifest.json to your working directory

Change the files specified in manifest.json to 'biophys4-PFFproject'
'''
#
def apical_inhibition(freq_mod_i_apic = 0):

    # freq_mod_i_apic = 0

    h('forall delete_section()')
    LFPclass = LFP.create_LFPclass(wd, verbool=False)  # change to verbool = False if no print out is wanted
    LFPclass.create_cell(if_add_spines=False, tstop = 3000, dt = 0.05)  # create a default cell
    cell = LFPclass.cell
    LFPclass.set_active_basal_dendrites()
    LFPclass.set_strong_basal_dendrite(sec_name='dend[5]')
    LFPclass.create_synapse()  # insert synapse onto the cell
    synapses = cell.synapses
    LFPclass.create_electrode_array(sigma=0.3, x_off=-20, nChannel=64)
    electrode = LFPclass.electrode
    LFPclass.set_full_connect_simulation(e_freq = [10,15], i_freq = [10,10], freqmod_i_apic = freq_mod_i_apic, freqmod_i_base = 0, freqmod_nonclust = 0, freqmod_clust = 0, phase = 0.0, phase_i = 0.0, t_win = 10)
    cell.simulate(**LFPclass.simulationParameters)
    Vm = visualization.plot_Vm_traces(cell, ['soma[0]', 'apic[36]','apic[37]', 'apic[38]'], time_range=[0, 3000],
                                      if_plot=0)

    GABA_i = []
    GABA_g = []
    for synapse in synapses:
        if "GABA" in synapse.kwargs["name"]:
            GABA_i.append(synapse.i)
            v = np.mean(cell.vmem[cell.get_idx(synapse.kwargs["section"])][:])
            g = synapse.i/(v-synapse.kwargs["e"])
            GABA_g.append(g)

    syns = []
    for synapse, i in zip(synapses, range(len(synapses))):
        syn = {
            'section': synapse.kwargs["section"],
            'name': synapse.kwargs["name"],
            "if_clustered": synapse.kwargs["if_clustered"],
        }
        try:
            syn.update({
                "parent": synapse.kwargs["parent"],
                "if_clustered": synapse.kwargs["if_clustered"],
            })
        except KeyError:
            pass
        try:
            syn.update({'spike_time': synapse.spike_time})
        except AttributeError:
            pass
        syns.append(syn)
    for synapse, i in zip(LFPclass.ex_NMDA, range(len(LFPclass.ex_NMDA))):
        syn = {
            'section': synapse["section"],
            'name': synapse["name"],
            'spike_time': synapse["spike_time"],
            "if_clustered": synapse["if_clustered"]
        }
        syns.append(syn)
    data = {}
    data["lfp"] = electrode.LFP
    data["Vm"] = Vm
    data["syns"] = syns
    data["GABA_i"] = GABA_i
    data["spike_train"] = LFPclass.spike_trains
    file_name = os.path.join('C:\\work\\PFFsimulation','apicimod%d' %(freq_mod_i_apic) +'result_wLFP' + '.mat')
    sio.savemat(file_name, data)

#%%
for i in [40]:
    apical_inhibition(freq_mod_i_apic = i)