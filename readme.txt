﻿This is a cortical layer 5 pyramidal cell model addaped from
Hay E, Hill S, Schürmann F, Markram H, Segev I (2011) Models of neocortical layer 5b pyramidal cells capturing a wide range of dendritic and perisomatic active properties. PLoS Comput Biol 7:e1002107

Create a new anaconda environment with the following commands:
conda create --name name_of_the_env
conda activate name_of_the_env
cd the\path\of\the\requirements
conda install -c anaconda pip
pip install LFPy
conda install -c anaconda numpy
conda install -c anaconda scipy
conda install -c conda-forge matplotlib
conda install spyder=4.1.1
pip install allensdk

Download the .mod files from the website
https://senselab.med.yale.edu/ModelDB/showModel.cshtml?model=139653#tabs-1
and compile in cmd with the following commands:
- cd the\path\of\the\mod\folder
- nrnivmodl .\mod
Then the nrnmech.dll should show up in the path

The module is able to 
1. Create a cell with 3d shape given its morphology (reconstructed morphologies of different cell types 
can be found on mouselight or modeldb)
2. Insert biophysilogical ion channels (need to be specified in a separate .json file, 
see biophys4.json as an example),
3. Insert synapses/spines
4. Perform intracellular or extracellular stimulation (see pipeline1 as an example)
5. Run the cabel model simulation to calculate membrane voltage (vmem) as well as transmembrane current (imem), 
if other features such as intracellular Ca concentration (cai), ion channel currents (ik, ina, ihcn, etc.) also need
to be monitored, specify them in stimulationParameters['rec_variables'] in LFPclass.simulation function (see the 
commented codes there as an example). To read out these recordings, use command like: ik = cell.rec_variables['ik']
5. Perform extracellular recording (see pipeline2 and 3), electrode location, size... etc can be specified by creating 
custom function or modifying the LFPclass.create_electrode functions
6. Some quick visualization (see visualization.py as an example)
7. biophysical parameter fitting is not yet fully developed and tested (some functions are in single_cell.py),
please see the coming updates  


LFP.py contains the functions to create the cell, set up stimulations, caculate field potential, etc
visualization.py contains the functions for visualization of cell morphology/ dynamic, etc
test.py contains the poorly organized pipeline

cell1.asc contains the morphology, adapted from Hay et al 2011
the biophys.json files contain the ion channel settings, adapted from Hay et al 2011
manifest.json specify the files that will be used to create the cell model




