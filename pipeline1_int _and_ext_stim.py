# %%
'''
This is a pipeline for implemeting the L5 PN model (Hay et al 2011)

Before running the pipeline, please set up the environment according to readme.txt, activate the
environment and cd to the directory in cmd
'''

# %%
"""
unit of NEURON (unless otherwise noticed):
    time: ms
    voltage: mV
    current: mA/cm^2 (all the ionic current recorded is density), nA
    concentration: mM
    capacitance: uf/cm^2
    length: um
    conducrance: S/cm^2, uS
    axis resistivity (Ra): ohm*cm
    resistance (SEClamp.rs): 10^6*ohm
    extracellular conductivity (self-defined): S/m 

see the unit with command: h.units()
"""

# %% Import modules
import sys
wd = 'C:\\work\\Code\\neuron-l5pn-model' # working directory
sys.path.insert(1, wd)

from neuron import h
from neuron import gui2
import neuron
import numpy as np
import matplotlib.pyplot as plt
import glob
import os
from pprint import pprint
import LFPy
from scipy.stats import norm
from numpy.random import RandomState

import LFP
import visualization
from LFP import LFPclass

# %% load mechanisms
# This only need to be run once unless any changes are made in nrnmech.dll

neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model")
neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model\\mod_test")
neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model\\mod_Gao2020")
neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model\\NMDA")
# %%
'''
Before running the following code, please change the directory in manifest.json to your working directory

Change the files specified in manifest.json if other morphology or biophysical channels will be used in the model
'''
# %% Create the cell and insert synapse
h('forall delete_section()')
LFPclass = LFP.create_LFPclass(wd, verbool=True)  # change to verbool = False if no print out is wanted
LFPclass.create_cell(tstop = 300, dt = 0.05)  # create a default cell
cell = LFPclass.cell
LFPclass.set_active_basal_dendrites()
LFPclass.create_electrode_array(sigma=0.3, x_off=50, nChannel=64)
LFPclass.set_strong_basal_dendrite(sec_name='dend[5]')
# LFPclass.set_deficite_channels(mec_name = 'NaTs2_t', prop_name = 'gNaTs2_tbar', sec_name='all', percentage=0.3)
# LFPclass.set_deficite_channels(mec_name='NaTa_t', prop_name = 'gNaTa_tbar', sec_name='all', percentage=0.3)
# LFPclass.set_deficite_channels(mec_name='na', prop_name = 'gbar', sec_name='all', percentage=0.3)
electrode = LFPclass.electrode
# %% Set intracellular stimulation
# The default intracellular stimulation is 100ms, 0.8nA step current injection into cell body
stim_param = {
    'sec': ['soma[0]','apic[36]'],
    'electrode_type': ['IClamp', 'IClamp'],
    'stim_delay': [200, 200],
    'stim_amp': [2, 0.7],
    'stim_dur': [5, 5],
}
# Note: even if only one section is stimulated, all perameters need to be lists, for example:
# stim_param = {
#    'sec': ['apic[36]'],
#    'electrode_type': ['IClamp'],
#    'stim_delay': [200],
#    'stim_amp': [2],
#    'stim_dur': [5],
# }

LFPclass.create_int_stim(**stim_param)

#%% Set extracellular stimulation
# The default extracellular stimulation is 200ms, 5 Hz, 20nA (amplitude) sinusoidal wave 50 um (x axis offset)
# away from cell body

# LFPclass.create_ext_stim(x = 50, stim_amp = 200)
mu = 0
sigma = 150
tau = 3

rand_norm = norm
rand_norm.randomstate = RandomState(seed = None)
wn = rand_norm.rvs(mu, sigma, size = int(10e4))
t = np.arange(0, 10e4*1e-5, 1e-5)
conv_f = t*np.exp(-t/(tau/1e3))
conv_f = conv_f/np.sum(conv_f)
wn_conv = np.convolve(wn, conv_f)
wn_new = mu+(wn_conv[int(5e4):int(10e4)]-np.mean(wn_conv[int(5e4):int(10e4)]))*sigma/np.std(wn_conv[int(5e4):int(10e4)])

stim_param = {
    'x':50,
    'stim_type': "defined",
    'stim_wave': wn_new,
}

LFPclass.create_ext_stim(**stim_param)
#%% Set simulation
LFPclass.set_simulation(spike_times = [], trg_syns = [])

# ihcn = cell.rec_variables['ihcn_Ih']
# ina = cell.rec_variables['ina']
# ipas = cell.rec_variables['i_pas']
# ik = cell.rec_variables['ik']
# ica = cell.rec_variables['ica']

# #%% View Morphology and Vm traces
# visualization.plot_morphology(LFPclass, sections = ['soma[0]','dend[0]','dend[5]'])
# the color code on morphology will be corresponding to Vm traces if sections are specified
# Vm = visualization.plot_Vm_traces(cell, ['soma[0]', 'apic[36]','apic[38]'], time_range = [0, 300], if_plot = True)
# Rin = (np.mean(cell.vmem[-1019,4500:6000])-np.mean(cell.vmem[-1019,8500:10000]))/(-0.1)
lfp = electrode.LFP  # each row correspondes to LFP recorded from one electrode

# visualize the electrode grid and LFP traces
visualization.plot_electrode_LFP(LFPclass, time_range = [200, 300], if_plot_morphology = 1)
#%% View Vm dynamic as animation
visualization.Vm_dynamic(cell, wd, time_range = [40, 140], if_save=True)
