#%%
"""
This is a L2/3 PN model adapted from
Branco T, Clark BA, Häusser M (2010) Dendritic discrimination of temporal input sequences in cortical neurons

The morphology file is rc19.hoc and the biophys file is biophys_l23pn.json, please change the working directory
in manifest_l23pn.json
"""
#%%
import sys
wd = 'C:\\work\\Code\\neuron-l5pn-model' # working directory
sys.path.insert(1, wd)

from neuron import h
from neuron import gui2
import neuron
import numpy as np
import matplotlib.pyplot as plt
import glob
import os
from pprint import pprint
import LFPy

import LFP
import visualization
from LFP import LFPclass

#%% create model
# Note: unlike the l5pn model, the soma and axon section is named as 'soma' and 'axon' in l2/3pn model
LFPclass = LFP.create_LFPclass(wd, verbool = True)
LFPclass.create_cell(if_add_spines = False, cell_type = 'l23pn')
cell = LFPclass.cell
LFPclass.create_synapse()
synapses = cell.synapses
LFPclass.create_electrode_section(sigma = 0.26, sec = 'soma', r = 200)
#%% simulate
LFPclass.set_simulation(spike_times = [np.array([50])], trg_syns  = ['soma'])
#%% visualize
Vm = visualization.plot_Vm_traces(cell, ['soma', 'dend[0]', 'apic[30]', 'axon'], time_range = [0, 200], if_plot = 1)
visualization.plot_morphology(LFPclass, sections = ['soma', 'dend[0]', 'apic[30]', 'axon'])
visualization.plot_electrode_LFP(LFPclass, time_range = [45, 70], if_plot_morphology = 1)
visualization.Vm_dynamic(cell, wd, time_range = [40, 90], if_save=False)