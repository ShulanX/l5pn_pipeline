# %%
'''
This is a pipeline for implemeting the L5 PN model (Hay et al 2011)

Before running the pipeline, please set up the environment according to readme.txt, activate the
environment and cd to the directory in cmd
'''

# %%
"""
unit of NEURON (unless otherwise noticed):
    time: ms
    voltage: mV
    current: mA/cm^2 (all the ionic current recorded is density), nA
    concentration: mM
    capacitance: uf/cm^2
    length: um
    conducrance: S/cm^2, uS
    axis resistivity (Ra): ohm*cm
    resistance (SEClamp.rs): 10^6*ohm
    extracellular conductivity (self-defined): S/m 

see the unit with command: h.units()
"""

# %% Import modules
import sys
wd = 'C:\\work\\Code\\neuron-l5pn-model' # working directory
sys.path.insert(1, wd)

from neuron import h
from neuron import gui2
import neuron
import numpy as np
import matplotlib.pyplot as plt
import glob
import os
from pprint import pprint
import LFPy

import LFP
import visualization
from LFP import LFPclass

# %% load mechanisms
# This only need to be run once unless any changes are made in nrnmech.dll

neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model")
neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model\\mod_test")
neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model\\mod_Gao2020")
neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model\\NMDA")
# %%
'''
Before running the following code, please change the directory in manifest.json to your working directory

Change the files specified in manifest.json if other morphology or biophysical channels will be used in the model
'''
# %% Create the cell and insert synapse

h('forall delete_section()')
LFPclass = LFP.create_LFPclass(wd, verbool=True)  # change to verbool = False if no print out is wanted
LFPclass.create_cell(if_add_spines = False,tstop = 500, dt = 0.05)  # create a default cell
cell = LFPclass.cell
LFPclass.set_active_basal_dendrites()
LFPclass.create_synapse()  # insert synapse onto the cell
# If synapses other than the ones specified in the biophys.json file will be inserted, used following command instead:
# syn_dict = [{
# 'section':['soma[0]'],
# 'num': 5,
# 'name': 'GABA_A',
# }]
# LFPclass.create_synapse(if_from_file = False, syn_dict)
synapses = cell.synapses

# create electrodes
# Create 1d electrode array with 50 um x offset from the center of cell body that spread the entire y-axis range of the cell
LFPclass.create_electrode_array(sigma=0.3, x_off=-20, nChannel=64)
electrode = LFPclass.electrode

# %% set presynaptic spikes and start simulation
# Note: for each simulation, a new cell need to be created

LFPclass.set_simulation(spike_times=[np.asarray([350]), np.asarray([350])], trg_syns=['apic', 'soma'])
# spike_times and trg_syn should be list even if only one section is targeted, for example:
# LFPclass.set_simulation(spike_times = [np.array([50])], trg_syns  = ['soma'])
# LFPclass.set_simulation(spike_times = [], trg_syns = [])

# %% Visualze Vm and morphology
Vm = visualization.plot_Vm_traces(cell, ['soma[0]', 'apic[36]', 'apic[37]', 'apic[38]'], time_range=[300, 500],
                                  if_plot=1)
# visualization.plot_morphology(LFPclass, if_show_synapses=1, if_show_electrode=1,
#                               sections=['soma[0]', 'apic[36]', 'apic[37]', 'apic[38]'])
# this will show the location of synapses and electrode on the morphology plot

# %% read out the LFP traces

lfp = electrode.LFP  # each row correspondes to LFP recorded from one electrode
visualization.plot_electrode_LFP_1D(LFPclass, time_range = [340, 400], if_plot_morphology = 1)
# %% Estimate CSD

csd = LFP.calc_csd(electrode)
# Note: the unit of CSD is A/(cm)^2
visualization.plot_csd(LFPclass, csd, interp_ratio=10, time_range=[50, 100])
