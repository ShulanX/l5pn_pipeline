# %%
'''
This is a pipeline for implemeting the L5 PN model (Hay et al 2011)

Before running the pipeline, please set up the environment according to readme.txt, activate the
environment and cd to the directory in cmd
'''

# %%
"""
unit of NEURON (unless otherwise noticed):
    time: ms
    voltage: mV
    current: mA/cm^2 (all the ionic current recorded is density), nA
    concentration: mM
    capacitance: uf/cm^2
    length: um
    conducrance: S/cm^2, uS
    axis resistivity (Ra): ohm*cm
    resistance (SEClamp.rs): 10^6*ohm
    extracellular conductivity (self-defined): S/m 

see the unit with command: h.units()
"""

# %% Import modules
import sys
wd = 'C:\\work\\Code\\neuron-l5pn-model' # working directory
sys.path.insert(1, wd)

from neuron import h
from neuron import gui2
import neuron
import numpy as np
import matplotlib.pyplot as plt
import glob
import os
from pprint import pprint
import LFPy
from scipy.stats import norm
from numpy.random import RandomState

import LFP
import visualization
from LFP import LFPclass

# %% load mechanisms
# This only need to be run once unless any changes are made in nrnmech.dll

neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model")
neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model\\mod_test")
neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model\\mod_Gao2020")
neuron.load_mechanisms("C:\\work\\Code\\neuron-l5pn-model\\NMDA")
# %%
'''
Before running the following code, please change the directory in manifest.json to your working directory

Change the files specified in manifest.json if other morphology or biophysical channels will be used in the model
'''
# %% Create the cell and insert synapse

LFPclass = LFP.create_LFPclass(wd, verbool=True)  # change to verbool = False if no print out is wanted
LFPclass.create_cell(tstop = 200, cell_type = 'l5pn', dt = 0.01)  # create a default cell
cell = LFPclass.cell
LFPclass.create_electrode(x = 20, y = 0, z = 0)
electrode = LFPclass.electrode
#%% Set dynamic stim
mu = 0.6
sigma = 0.4
tau = 3

rand_norm = norm
rand_norm.randomstate = RandomState(seed = None)
wn = rand_norm.rvs(mu, sigma, size = int(10e3))
t = np.arange(0, 10e3*1e-4, 1e-4)
conv_f = t*np.exp(-t/(tau/1e3))
conv_f = conv_f/np.sum(conv_f)
wn_conv = np.convolve(wn, conv_f)
wn_new = mu+(wn_conv[int(5e3):int(10e3)]-np.mean(wn_conv[int(5e3):int(10e3)]))*sigma/np.std(wn_conv[int(5e3):int(10e3)])

#%%
stim_param = {
    'sec':['soma[0]'],
    'electrode_type': ['IClamp'],
    'stim_wave': [wn_new],
}
LFPclass.create_int_stim_dynamic(**stim_param)
# %% Set intracellular stimulation
# The default intracellular stimulation is 100ms, 0.8nA step current injection into cell body
stim_param = {
    'sec': ['soma[0]','apic[36]'],
    'electrode_type': ['IClamp', 'IClamp'],
    'stim_delay': [50, 50],
    'stim_amp': [1.8, 0.5],
    'stim_dur': [5, 5],
}
# Note: even if only one section is stimulated, all perameters need to be lists, for example:
# stim_param = {
#    'sec': ['soma[0]'],
#    'electrode_type': ['IClamp'],
#    'stim_delay': [50],
#    'stim_amp': [0.5],
#    'stim_dur': [400],
# }

LFPclass.create_int_stim(**stim_param)

#%% Set extracellular stimulation
# The default extracellular stimulation is 200ms, 5 Hz, 20nA (amplitude) sinusoidal wave 50 um (x axis offset)
# away from cell body

LFPclass.create_ext_stim(x = 50, stim_amp = 200)

#%% Set simulation
LFPclass.set_simulation(spike_times = [], trg_syns = [])

#%% View Morphology and Vm traces
# visualization.plot_morphology(LFPclass, sections = ['soma[0]', 'apic[36]', 'apic[37]', 'apic[38]'])
# the color code on morphology will be corresponding to Vm traces if sections are specified
Vm = visualization.plot_Vm_traces(cell, ['soma[0]', 'apic[36]', 'apic[37]', 'apic[38]'], time_range = [0, 200], if_plot = True)

#%% View Vm dynamic as animation
visualization.Vm_dynamic(cell, wd, time_range = [40, 140], if_save=True)
