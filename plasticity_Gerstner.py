"""
postsynaptic voltaged based plasticity model implemented from

@misc{meissnerbernard2020paradoxical,
      title={Paradoxical Results of Long-Term Potentiation explained by Voltage-based Plasticity Rule},
      author={Claire Meissner-Bernard and Matthias Tsai and Laureline Logiaco and Wulfram Gerstner},
      year={2020},
      eprint={2001.03614},
      archivePrefix={arXiv},
      primaryClass={q-bio.NC}
}


"""
import numpy as np
from scipy.optimize import least_squares

def generate_time_iterate(stop, dt = 0.025, start = 0):
    return np.arange(start, stop, dt)

def generate_spike_train(tvec, spike_time):


    """
    generate Dirac pulse train X(t) with spike time list

    :param tvec: np array, time in ms
    :param spike_time: list, time of spiking in ms
    :return:
    """
    X = np.zeros(tvec.size())
    start = tvec[0]
    dt = tvec[1] - tvec[0]
    for st in spike_time:
        X[np.floor((st-start)/dt)] = 1
    return X

def calc_weight(params, tvec, data, w0 = 0.5):

    """
    calculate weight trace at each timepoint given the fitting parameters

    :param tvec: np array, time in ms
    :param data: list of array [X, u]
        X: presynaptic Dirac pulse train
        u: postsynaptic voltage (mV), RMP -> 0
    :param w0: scalar, initial value for weight
    :param params:
        tau_x
        tau_plus
        theta_plus
        theta_0
        A_LTP
        A_LTD
        tau_minus
        b_theta
        tau_theta
    :return:
    """

    data_dict = {
        'X':data[0],
        'u':data[1],
        'tau_x' : params[0],
        'tau_plus' : params[1],
        'theta_plus' : params[2],
        'theta_0' : params[3],
        'A_LTP' : params[4],
        'A_LTD' : params[5],
        'tau_minus' : params[6],
        'b_theta' : params[7],
        'tau_theta' : params[8]
    }
    dt = tvec[1] = tvec[0]
    LP_trace_initieal = [w0, 0, data[1][0], data[1][0], w0, w0, 0]
    # LP filtered traces [w, x_bar, u_minus_bar, u_plus_bar, w_LTP, w_LTD, theta]
    LP_trace = euler([ode_w, ode_x_bar, ode_u_minus_bar, ode_u_plus_bar, ode_w_LTP, ode_w_LTD, ode_Theta],
                  tvec, dt, LP_trace_initieal, data_dict)
    return LP_trace[0]

def fit_func(params, data, tvec, w_exp):
    """
    re-formated function for LSE fitting f(params, t, [X, u])

    data will be a list of lists [[X, u]]
    w_exp will be a list [w_exp]
    :return:
    """
    w_f = []
    for d in data:
        w_trace = calc_weight(params, d, tvec, w0 = 0.5)
        w_f.append(w_trace[-1])
    return np.asarray(w_f) - np.asarray(w_exp)

def run_fitting(tvec, data_train, w_train):
    """

    :param tvec:
    :param data_train: list of lists [[X, u]]
    :param w_train: list [w_exp]
    :return: params
    """
    param_0 = np.asarray([22.4, 2, 27.1, 6.2, 4.28, 16.4, 60., 1., 29.1])
    upper_bound = np.asarray([30, 60, 30, 15, 1e-2, 1e-1, 60, 5e4, 100])
    lower_bound = np.asarray([2, 2, 5, 2.5, 1e-5, 1e-5, 2, 0, 2])
    data_train_with_t = []
    for data in data_train:
        data_train_with_t.append([data, tvec])
    res = least_squares(fit_func, x0 = param_0, bounds = (lower_bound, upper_bound), args = (data_train, tvec, w_train))
    return res.x

def ode_w(t_c, y, A_LTP, A_LTD, theta_plus):
    """
    dw/dt = dw_LTP/dt - dw_LTD/dt
    :param t_c:
    :param y:
    :return:
    """
    f = ode_w_LTP(t_c, y, A_LTP, theta_plus) - ode_w_LTD(t_c, y, A_LTD, theta_0)
    return f

def ode_x_bar(t_c, y, tau_x, X):
    """
    dx_bar/dt = (-x_bar+X)/tau_x
    :param t_c:
    :param y:
    :param X:
    :return:
    """
    f = (-y[1]+X[int(t_c)])/tau_x
    return f

def ode_u_minus_bar(t_c, y, tau_minus, u):
    """
    du_minus_bar/dt = (-u_minus_bar+u)/tau_minus
    :param t_c:
    :param y:
    :param tau_minus:
    :param X:
    :param theta_0:
    :return:
    """
    f = (-y[2]+u[int(t_c)])/tau_minus
    return f

def ode_u_plus_bar(t_c, y, tau_plus, u):
    """
    du_plus_bar/dt = (-u_plus_bar+u)/tau_plus
    :param t_c:
    :param y:
    :param tau_plut:
    :param u:
    :return:
    """
    f = (-y[3]+u[int(t_c)])/tau_plus
    return f

def ode_w_LTP(t_c, y, A_LTP, theta_plus):
    """
    dw_LTP/dt = A_LTP*x_bar[u_bar_plus - theta_plus]
    :param t_c:
    :param y:
    :param A_LTP:
    :param theta_0:
    :return:
    """
    f = A_LTP*y[1]*(y[3]-theta_plus)
    return f

def ode_w_LTD(t_c, y, A_LTD, theta_0):
    """
    dw_LTD/dt = A_LTD*x_bar*(u_minus_bar-theta_0-theta)
    :param t_c:
    :param y:
    :param A_LTD:
    :param theta_0:
    :return:
    """
    f = A_LTD*y[1]*(y[2]-theta_0-y[6])
    return f

def ode_theta(t_c, y, tau_theta, b_theta):
    """
    dtheta/dt = (-theta + b_theta*dwLTP/dt)/tau_theta
    :param t_c:
    :param y:
    :param tau_theta:
    :param b_theta:
    :return:
    """
    f = (-y[6] + b_theta*ode_w_LTP(t_c, y, A_LTP, theta_plus))/tau_theta
    return f

def euler(odes, interval, step, initial_values, kwargs):
    """
    solve a 1 order ODE with euler method.
    For dy/dt = f(y, t), solve with 4 order RK method

    Parameters
    ----------
    odes : list of python functions
        ODE(interval, value) = f(t, y), return f
    interval: np array
        time points (in ms)
    step : numerical
        interval size (in ms)
    initial_values : list
        the initial value of y (y_0)

    Returns: y list of ndarray
    -------
    """
    y = [initial_values]
    N = len(initial_values)
    if len(odes) != N:
        raise ValueError('The %d ODEs doesn\'t match with %d initial values' % (len(odes), len(initial_values)))
    dt = step
    for t_c in np.arange(1, len(interval), 1):
        y_c = np.asarray(y[-1])  # current value of y
        y_n = []
        k = dt*ode(t_c, y_c, **kwargs) # t_c is actually index here
        y_n.append(y_c + k)
        y.append(y_n)
    u = []
    for i in range(N):
        u.append(np.asarray([x[i] for x in y]))
    return u
